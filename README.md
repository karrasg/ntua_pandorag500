# README #

Assuming Final Version of Girona500 Simulator for ROS Hydro is installed.

1. Download:

**roscd catkin_ws/src**

**git clone https://bitbucket.org/karrasg/ntua_pandorag500**

2. Install:

**catkin_make**

If the simulator has been installed correctly , no additional dependencies are needed

3. Initialize the framework:

**roslaunch ntua_pandorag500 motion_control.launch**

This is actually the same launch file provided by Girona, but it calls the g500_sim_ntua.launch where the following nodes have been disabled:
-pose_controller
-velocity_controller
-safe_depth_altitude
-set_zero_velocity
-logitech_fx10_joystick

4. Bring up controller:

**rosrun ntua_controlg500 ntua_g500_controller.py.**

This is a 5DoF pose controller for the g500 vehicle.
By default it positions the vehicle at North:0 East:0 Down:1.5 Pitch:0 Yaw:0

5. Bring up the planner:

**rosrun ntua_controlg500 ntua_wp_planner.py North East Depth Pitch Yaw**

Position is in m and orientation in degrees.

Example:

**rosrun ntua_controlg500 ntua_wp_planner.py 2.0 1.0 1.5 0.0 90.0**
This will send the command to the controller to move the vehicle at the desired pose: 2 m North, 1 m East, 1.5 m Down, 0 deg pitch, 90 deg yaw


**NOTE:** It is more likely that you will need to tune the gains of the controller. 
After step 4 has been executed you can open a new terminal and type:
rosrun rqt_reconfigure rqt_reconfigure.
From there you can on line change all the gains of the controller. When you finalize the gains you must edit the control_parameters.cfg file located in the cfg folder and then rebuild the package again using catkin_make.

**CAUTION!!!** The pitch controller is designed to stabilize the vehicle along zero pitch. **DO NOT** try to achieve other angles, because the navigation filter will diverge and the vehicle will be uncontrollable.**DANGER!**