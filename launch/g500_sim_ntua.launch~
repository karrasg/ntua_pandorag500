<launch>

    <!-- Define args to be provided by the includer ("parent") launch file -->
    <arg name="enableJoystick" default="false"/>
    <arg name="joystickDevice" default="/dev/input/js0"/>
    <arg name="enableTeleopArm" default="false"/>
    <arg name="enableNtuaControl" default="false" />


    <!--
        <arg name="ROSMaster" value="http://narcis-mbp:11311/" />
    -->
    <arg name="simulatedClock" value="false" />

    <!-- LOAD PARAMETERS -->
    <rosparam command="load" file="$(find cola2_safety)/config/safety_g500.yaml" />
    <rosparam command="load" file="$(find cola2_sim)/config/fake_main_board_g500.yaml" />
    <rosparam command="load" file="$(find cola2_sim)/config/dynamics_odin.yaml" />
    <rosparam command="load" file="$(find cola2_sim)/config/sim_nav_sensors_g500.yaml" />
    <rosparam command="load" file="$(find cola2_sim)/config/sim_actuators_g500.yaml" />
    <rosparam command="load" file="$(find cola2_control)/config/thruster_allocator_g500_sim.yaml" />
    <rosparam command="load" file="$(find cola2_control)/config/pose_controller_g500_sim.yaml" />
    <rosparam command="load" file="$(find cola2_control)/config/velocity_controller_g500_sim.yaml" />
    <rosparam command="load" file="$(find cola2_control)/config/pilot.yaml"/>
    <rosparam command="load" file="$(find cola2_navigation)/config/navigator_g500.yaml" />
    <rosparam command="load" file="$(find cola2_launch)/config/mission.yaml" />
    <rosparam command="load" file="$(find cola2_control)/config/map_ack_g500.yaml" />
    <rosparam command="load" file="$(find cola2_control)/config/teleoperation_g500.yaml" />
    <rosparam command="load" file="$(find cola2_control)/config/merge_g500.yaml"/>
    <!-- <rosparam command="load" file="$(find cola2_control)/config/joyArm.yaml"/> -->
    <group if="$(arg enableTeleopArm)">
        <rosparam command="load" file="$(find cola2_control)/config/joy_arm.yaml"/>
        <rosparam command="load" file="$(find cola2_control)/config/arm_controller.yaml" />
        <rosparam command="load" file="$(find cola2_sim)/config/fake_csip_e5_arm.yaml" />
    </group>
    <param name="robot_description" command="cat $(find cola2_launch)/config/model.xml" />
    <rosparam param="urdf_root"> </rosparam>

    <!-- OVERRIDE BASIC MISSION PARAMETERS -->
    <rosparam command="load" file="$(find cola2_launch)/config/basic_mission_parameters.yaml" />

    <group if="$(arg simulatedClock)">
        <param name="/time_scale" value="2" />
        <param name="/clock_publish_rate" value="1000" />
        <node name="sim_clock" pkg="cola2_lib" type="sim_clock"/>
    </group>

    <!-- ################## AVOID TO TOUCH ANYTHING BELOW THIS LINE ##################### -->

    <!-- Extra frame for rotate the Z in RViz -->
    <node pkg="tf" type="static_transform_publisher" name="rviz_to_world" args="0.0 0.0 0.0 0.0 0.0 3.14159 /rviz /world 100" />

    <!-- LOAD NODES -->

    <!-- Safety nodes -->
    <node name="arduino_helper" pkg="cola2_safety" type="arduino_helper_g500.py" output="screen"/>
   <!-- <node name="safe_depth_altitude" pkg="cola2_safety" type="safe_depth_altitude.py" output="screen"/> -->
   <!-- <node name="set_zero_velocity" pkg="cola2_safety" type="set_zero_velocity.py" output="screen"/> -->
    <node name="diagnostics_status" pkg="cola2_safety" type="diagnostics_status_g500.py" output="screen"/>
    <node name="recovery_actions" pkg="cola2_safety" type="recovery_actions.py" output="screen"/>
    <node name="trap_avoidance" pkg="cola2_safety" type="trap_avoidance.py" output="screen"/>
    <include file="$(find cola2_safety)/launch/aggregator_sim.launch" />

    <!-- Run dynamics -->
    <node name="dynamics" pkg="cola2_sim" type="dynamics.py" respawn="false" output="screen"/>
    <node name="sim_nav_sensors" pkg="cola2_sim" type="sim_nav_sensors_g500.py" respawn="false"/>
    <node name="sim_actuators_g500" pkg="cola2_sim" type="sim_actuators_g500.py" respawn="false" output="screen"/>

    <!-- Enable fake main control board -->
    <node name="fake_main_board_g500" pkg="cola2_sim" type="fake_main_board_g500.py" />

    <!-- Run Control Stack -->
    <node name="teleoperation" pkg="cola2_control" type="teleoperation.py" output = "screen"/>
    <node name="map_ack" pkg="cola2_control" type="map_ack.py" output="screen"/>
    <node name="thruster_allocator" pkg="cola2_control" type="thruster_allocator.py"/>
    <!-- <node name="pose_controller" pkg="cola2_control" type="pose_controller_g500.py"/> -->
   <!-- <node name="velocity_controller" pkg="cola2_control" type="velocity_controller_g500.py"/> -->
    <node name="merge_world_waypoint_req" pkg="cola2_control" type="merge_world_waypoint_req.py"/>
    <node name="merge_body_velocity_req" pkg="cola2_control" type="merge_body_velocity_req.py"/>
    <node name="merge_body_force_req" pkg="cola2_control" type="merge_body_force_req.py" />
    <node name="pilot" pkg="cola2_control" type="pilot.py" output="screen"/>
    <node name="captain" pkg="cola2_control" type="captain.py" output="screen"/>

    <!-- Run Navigation Stack -->
    <node name="navigator" pkg="cola2_navigation" type="navigator_g500.py" />
    <node name="ekf_slam" pkg="pose_ekf_slam" type="ekf_slam.py" output="screen">
        <param name="world_frame_name" type="string" value="world" />
        <param name="robot_frame_name" type="string" value="girona500" />
    </node>

    <!-- <node name="arm_manipulator" pkg="cola2_control" type="arm_manipulator.py" /> -->
    <group if="$(arg enableTeleopArm)">
        <node name="arm_controller" pkg="cola2_control" type="arm_controller.py"/>
        <node name="fake_csip_e5_arm" pkg="cola2_sim" type="fake_csip_e5_arm.py"/>
        <group if="$(arg enableJoystick)">
           <!-- <node respawn="true" name="logitech_fx10_joystick" pkg="cola2_control" type="logitech_fx10_joystick_arnau.py"/> -->
            <node respawn="true" pkg="joy" type="joy_node" name="joystick">
                <param name="deadzone" value="0.1"/>
                <param name="dev" value="$(arg joystickDevice)"/>
            </node>
        </group>
    </group>

    <!-- Teleoperation controllers -->
    <!-- Run Joystick or Keyboard -->
    <group unless="$(arg enableTeleopArm)">
        <group if="$(arg enableJoystick)">
            <rosparam command="load" file="$(find cola2_control)/config/map_ack_joy_g500.yaml" />
            <node respawn="true" pkg="joy" type="joy_node" name="joystick">
                <param name="deadzone" value="0.1"/>
                <param name="dev" value="$(arg joystickDevice)"/>
            </node>
        </group>
    </group>
    <group unless="$(arg enableJoystick)">
        <node name="keyboard" pkg="cola2_control" type="keyboard" />
    </group>

</launch>
