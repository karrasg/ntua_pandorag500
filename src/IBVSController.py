#!/usr/bin/env python

# ROS imports
import roslib 
roslib.load_manifest('ntua_pandora')
roslib.load_manifest('udg_pandora')
import rospy
import tf

# Msgs imports
from sensor_msgs.msg import JointState
from geometry_msgs.msg import PoseStamped, Twist
from udg_pandora.msg import Point2D, Point2DMatched, BoxCorners

# Python imports
from numpy import *
from numpy.linalg import *

# Custom imports
import cola2_lib
import cola2_ros_lib


class IBVSController:
    def __init__(self, name):
        
	# Create publishers --> Publish Arm EE Velocities
	self.pub_eeVel = rospy.Publisher("/visual_servo_uvms/eeVelCmds", Twist)
        
        # Create Subscribers
        rospy.Subscriber("/visual_detector_ee/box_corners", BoxCorners, self.updateImageFeatures) #Get Image Features
        
	#Get Initial Time
        self.t0 = rospy.Time.now().to_sec()  
	
	self.numFeatures = 4
	self.featuresArray = empty((self.numFeatures, 2), dtype=float)

	#Get Camera Parameters
	self.cu = rospy.get_param("/visual_detector_ee/image_x_size")/2.0 #Image x Center (width/2)
	self.cv = rospy.get_param("/visual_detector_ee/image_y_size")/2.0 #Image y xenter (height/2)
	self.f = rospy.get_param("/visual_detector_ee/f") #focal length in mm
	self.ax = rospy.get_param("/visual_detector_ee/pixels_mm_x") #focal length in pixels/mm --> x
	self.ay = rospy.get_param("/visual_detector_ee/pixels_mm_y") #focal length in pixels/mm --> y

	self.fax = self.f*self.ax
	self.fay = self.f*self.ay
        
        #Dt
	self.dt = 0.2 #Why?

	#Transformation EndEffector (Target Frame) --> SubPanel Frame (Reference Frame)
	#Initilalize Transformation States
	self.x_eespf = 0.
	self.y_eespf = 0.
	self.z_eespf = 0.48 #To avoid divide by zero
	self.roll_eespf = 0.
	self.pitch_eespf = 0.
	self.yaw_eespf = 0.

	self.ee2sp = lambda : 0
        self.ee2sp.rot = None
        self.ee2sp.trans = None
        # TF listener to compute the transforms
        self.ee2sp.tflistener = tf.TransformListener()

	#Transformation EndEffector (Target Frame) --> World Frame (Reference Frame)
	#Initilalize Transformation States
	self.x_eewf = 0.
	self.y_eewf = 0.
	self.z_eewf = 0.
	self.roll_eewf = 0.
	self.pitch_eewf = 0.
	self.yaw_eewf = 0.

	self.ee2w = lambda : 0
        self.ee2w.rot = None
        self.ee2w.trans = None
        # TF listener to compute the transforms
        self.ee2w.tflistener = tf.TransformListener()


 
    def getEEtoSubPanelFrame(self):
	ee2sp = self.ee2sp
	try:
           ee2sp.trans, ee2sp.rot = ee2sp.tflistener.lookupTransform("/panel_centre", "end_effector", rospy.Time(0))
        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
            print "No End Effector to Panel Transform available"
	    self.z_eespf = 0.48 #To avoid divide by zero
	else:
           self.x_eespf = ee2sp.trans[0]
	   self.y_eespf = ee2sp.trans[1]
	   self.z_eespf = ee2sp.trans[2]

	   angles = tf.transformations.euler_from_quaternion(ee2sp.rot)
	   self.roll_eespf = angles[0]
	   self.pitch_eespf = angles[1]
	   self.yaw_eespf = angles[2]
           print "Translation:", ee2sp.trans, "Rotation:", angles

    def getEEtoWorldPose(self):
	ee2w = self.ee2w
	try:
           ee2w.trans, ee2w.rot = ee2w.tflistener.lookupTransform("world", "end_effector", rospy.Time(0))
        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
            print "No End Effector to World transform available"
	else:
           self.x_eewf = ee2w.trans[0]
	   self.y_eewf = ee2w.trans[1]
	   self.z_eewf = ee2w.trans[2]

	   angles = tf.transformations.euler_from_quaternion(ee2w.rot)
	   self.roll_eewf = angles[0]
	   self.pitch_eewf = angles[1]
	   self.yaw_eewf = angles[2]
           print "Translation:", ee2w.trans, "Rotation:", angles


    def updateImageFeatures(self, featurePoints):
        
 	self.featuresArray[0][0] = featurePoints.corners[0].x
	self.featuresArray[0][1] = featurePoints.corners[0].y
	self.featuresArray[1][0] = featurePoints.corners[1].x
	self.featuresArray[1][1] = featurePoints.corners[1].y
	self.featuresArray[2][0] = featurePoints.corners[2].x
	self.featuresArray[2][1] = featurePoints.corners[2].y
	self.featuresArray[3][0] = featurePoints.corners[3].x
	self.featuresArray[3][1] = featurePoints.corners[3].y
	        

    
    def control(self, eeVelCmds):
        
	#Image Depth
	Z = self.z_eespf
	Zd = 0.48
	f = self.f

	s = zeros((self.numFeatures, 2))

	#Current Features wrt image center
	s[0][0] = -(self.featuresArray[0][0] - self.cu) 
	s[0][1] = -(self.featuresArray[0][1] - self.cv)
	s[1][0] = -(self.featuresArray[1][0] - self.cu)
	s[1][1] = -(self.featuresArray[1][1] - self.cv)
	s[2][0] = -(self.featuresArray[2][0] - self.cu)
	s[2][1] = -(self.featuresArray[2][1] - self.cv)
	s[3][0] = -(self.featuresArray[3][0] - self.cu)
	s[3][1] = -(self.featuresArray[3][1] - self.cv)
	#Desired Features wrt image center
	sd = zeros((self.numFeatures, 2))
	sd[0][0] = -(341 - self.cu)
	sd[0][1] = -(237 - self.cv)
	sd[1][0] = -(504 - self.cu)
	sd[1][1] = -(236 - self.cv)
	sd[2][0] = -(343 - self.cu)
	sd[2][1] = -(389 - self.cv)
	sd[3][0] = -(497 - self.cu)
	sd[3][1] = -(391 - self.cv)

	es = zeros((self.numFeatures, 2))
	es[0][0] = s[0][0] - sd[0][0]
	es[0][1] = s[0][1] - sd[0][1]
	es[1][0] = s[1][0] - sd[1][0]
	es[1][1] = s[1][1] - sd[1][1]
	es[2][0] = s[2][0] - sd[2][0]
	es[2][1] = s[2][1] - sd[2][1]
	es[3][0] = s[3][0] - sd[3][0]
	es[3][1] = s[3][1] - sd[3][1]

	

	#es = self.featuresArray - sd
	es = array(es).reshape(8,1)

	print"es:",es
	
	fax = self.fax
	fay = self.fay
	
	Lx=array([ -fax/Z,     0.0,         s[0][0]/Z,          (s[0][0]*s[0][1])/fax,       -(fax**2+s[0][0]**2)/fax,     s[0][1],
                    0.0,    -fay/Z,         s[0][1]/Z,          (fay**2+s[0][1]**2)/fay,       -(s[0][0]*s[0][1])/fay,    -s[0][0],
     
                   -fax/Z,     0.0,         s[1][0]/Z,          (s[1][0]*s[1][1])/fax,       -(fax**2+s[1][0]**2)/fax,     s[1][1],
                    0.0,     -fay/Z,        s[1][1]/Z,          (fay**2+s[1][1]**2)/fay,       -(s[1][0]*s[1][1])/fay,    -s[1][0],

                   -fax/Z,     0.0,         s[2][0]/Z,          (s[2][0]*s[2][1])/fax,       -(fax**2+s[2][0]**2)/fax,     s[2][1],
                    0.0,    -fay/Z,         s[2][1]/Z,          (fay**2+s[2][1]**2)/fay,       -(s[2][0]*s[2][1])/fay,    -s[2][0],
     
     
                   -fax/Z,     0.0,         s[3][0]/Z,          (s[3][0]*s[3][1])/fax,       -(fax**2+s[3][0]**2)/fax,     s[3][1],
                    0.0,    -fay/Z,         s[3][1]/Z,          (f**2+s[3][1]**2)/fay,       -(s[3][0]*s[3][1])/fay,    -s[3][0]])

	
     

	Lx = array(Lx).reshape(8, 6)
	
	K = 50.0
	
	#Control Output in CAMERA FRAME
        Ucf = -K*dot(pinv(Lx),es)

	Re=array([ 0.0, 1.0,  0.0,  0.0,  0.0,  0.0,
     	    	  -1.0, 0.0,  0.0,  0.0,  0.0,  0.0,
                   0.0, 0.0,  1.0,  0.0,  0.0,  0.0,
                   0.0, 0.0,  0.0,  0.0,  1.0,  0.0,
                   0.0, 0.0,  0.0, -1.0,  0.0,  0.0,
                   0.0, 0.0,  0.0,  0.0,  0.0,  1.0])

	"""Re=array([ 0.0, 1.0,  0.0,  0.0,  0.0,  0.0,
     	    	  -1.0, 0.0,  0.0,  0.0,  0.0,  0.0,
                   0.0, 0.0,  1.0,  0.0,  0.0,  0.0,
                   0.0, 0.0,  0.0,  1.0,  0.0,  0.0,
                   0.0, 0.0,  0.0,  0.0,  1.0,  0.0,
                   0.0, 0.0,  0.0,  0.0,  0.0,  1.0])"""

	"""R_cam_inet = array([0.0,   0.0,   -1.0,     0.0,   0.0,   0.0, 
            	            1.0,   0.0,    0.0,     0.0,   0.0,   0.0, 
            	            0.0,  -1.0,    0.0,     0.0,   0.0,   0.0, 
                            0.0,   0.0,    0.0,     0.0,   0.0,  -1.0, 
                            0.0,   0.0,    0.0,     1.0,   0.0,   0.0, 
                            0.0,   0.0,    0.0,     0.0,  -1.0,   0.0])"""


	"""R_init=array([  0.0,   0.0,    1.0,    0.0,  0.0,   0.0, 
       		       -1.0,   0.0,    0.0,    0.0,  0.0,   0.0,
        		0.0,  -1.0,    0.0,    0.0,  0.0,   0.0,
        		0.0,   0.0,    0.0,    0.0,  0.0,   1.0, 
        		0.0,   0.0,    0.0,   -1.0,  0.0,   0.0, 
        		0.0,   0.0,    0.0,    0.0, -1.0,   0.0 ])"""
	


	R_ini_pseud =[ -1.0,    0.0,    0.0,   0.0,  0.0,   0.0, 
                        0.0,   -1.0,    0.0,   0.0,  0.0,   0.0, 
                        0.0,    0.0,    1.0,   0.0,  0.0,   0.0, 
                        0.0,    0.0,    0.0,  -1.0,  0.0,   0.0,
			0.0,    0.0,    0.0,   0.0, -1.0,   0.0, 
	                0.0,    0.0,    0.0,   0.0,  0.0,   1.0]

	Re = array(Re).reshape(6,6)
	R_ini_pseud = array(R_ini_pseud).reshape(6,6)

	#R_cam_inet = array(R_cam_inet).reshape(6,6)
	#R_init=array(R_init).reshape(6,6)
	#Control in EE Frame
	Uee = dot(Re,Ucf) 
	
	#U = dot(R_cam_inet,Ucf)
	#U = dot(R_init,Ucf)


	#Transform Camera Frame Velocities to World Frame Velocities
	phi = -self.roll_eewf 
	theta = -self.pitch_eewf 
	psi =   self.yaw_eewf 

	J1 = [cos(theta)*cos(psi),  cos(psi)*sin(theta)*sin(phi)-sin(psi)*cos(phi),  sin(psi)*sin(phi)+cos(psi)*cos(phi)*sin(theta),
      	      cos(theta)*sin(psi),  cos(psi)*cos(phi)+sin(phi)*sin(theta)*sin(psi),  sin(psi)*sin(theta)*cos(phi)-cos(psi)*sin(phi),
              -sin(theta),          cos(theta)*sin(phi),                             cos(theta)*cos(phi)]

	J2 = [1.0,  sin(phi)*tan(theta),  cos(phi)*tan(theta),
              0.0,  cos(phi),             -sin(phi),
              0.0,  sin(phi)/cos(theta),  cos(phi)/cos(theta)]

	J1 = array(J1).reshape(3,3)
	J2 = array(J2).reshape(3,3)

	JJ = zeros((6,6),float)
	JJ[0:3,0:3] = J1
        JJ[3:6,3:6] = J2

	Uwf = dot(JJ,Uee)
	U = Uwf
	#U = dot(R_ini_pseud,Uwf)
	
	eeVelCmds.linear.x = U[0]
	eeVelCmds.linear.y = U[1]
	eeVelCmds.linear.z = U[2]
	eeVelCmds.angular.x = U[3]
	eeVelCmds.angular.y = U[4]
	eeVelCmds.angular.z = U[5]

	print "U:", U
	self.pub_eeVel.publish(eeVelCmds)
	
	
      
if __name__ == '__main__':
    try:
        rospy.init_node('IBVSController')

        IBVSControllerObj = IBVSController(rospy.get_name())

        velCmds = Twist()
	while not rospy.is_shutdown():
                rospy.sleep(IBVSControllerObj.dt/2.0)
		IBVSControllerObj.getEEtoSubPanelFrame()
		IBVSControllerObj.getEEtoWorldPose()
		IBVSControllerObj.control(velCmds)
		rospy.sleep(IBVSControllerObj.dt/2.0)     
        rospy.spin()

    except rospy.ROSInterruptException: pass
