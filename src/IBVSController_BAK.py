#!/usr/bin/env python

# ROS imports
import roslib 
roslib.load_manifest('ntua_pandora')
roslib.load_manifest('udg_pandora')
import rospy
import tf

# Msgs imports
from sensor_msgs.msg import JointState
from geometry_msgs.msg import PoseStamped, Twist
from udg_pandora.msg import Point2D, Point2DMatched, BoxCorners

# Python imports
from numpy import *
from numpy.linalg import *

# Custom imports
import cola2_lib
import cola2_ros_lib


class IBVSController:
    def __init__(self, name):
        
	# Create publishers --> Publish Arm EE Velocities
	self.pub_eeVel = rospy.Publisher("/visual_servo_uvms/eeVelCmds", Twist)
        
        # Create Subscribers
        rospy.Subscriber("/visual_detector_ee/box_corners", BoxCorners, self.updateImageFeatures) #Get Image Features
        
	#Get Initial Time
        self.t0 = rospy.Time.now().to_sec()  
	
	self.numFeatures = 4
	self.featuresArray = empty((self.numFeatures, 2), dtype=float)

	#Get Camera Parameters
	self.cu = rospy.get_param("/visual_detector_ee/image_x_size")/2.0 #Image x Center (width/2)
	self.cv = rospy.get_param("/visual_detector_ee/image_y_size")/2.0 #Image y xenter (height/2)
	self.f = rospy.get_param("/visual_detector_ee/f") #focal length in mm
	self.ax = rospy.get_param("/visual_detector_ee/pixels_mm_x") #focal length in pixels/mm --> x
	self.ay = rospy.get_param("/visual_detector_ee/pixels_mm_y") #focal length in pixels/mm --> y

	self.f = self.f/1000.0 #focal length in m
	self.ax = self.ax/1000.0 #focal length in pixels/m --> x
	self.ay = self.ay/1000.0 #focal length in pixels/m --> y

        
        #Dt
	self.dt = 0.2 #Why?

	#Transformation EndEffector (Target Frame) --> SubPanel Frame (Reference Frame)
	#Initilalize Transformation States
	self.x_eespf = 0.
	self.y_eespf = 0.
	self.z_eespf = 0.48 #To avoid divide by zero
	self.roll_eespf = 0.
	self.pitch_eespf = 0.
	self.yaw_eespf = 0.

	self.ee2sp = lambda : 0
        self.ee2sp.rot = None
        self.ee2sp.trans = None
        # TF listener to compute the transforms
        self.ee2sp.tflistener = tf.TransformListener()

 
    def getEEtoSubPanelFrame(self):
	ee2sp = self.ee2sp
	try:
           ee2sp.trans, ee2sp.rot = ee2sp.tflistener.lookupTransform("/panel_centre", "end_effector", rospy.Time(0))
        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
            print "No End Effector to Panel Transform available"
	    self.z_eespf = 0.48 #To avoid divide by zero
	else:
           self.x_eespf = ee2sp.trans[0]
	   self.y_eespf = ee2sp.trans[1]
	   self.z_eespf = ee2sp.trans[2]

	   angles = tf.transformations.euler_from_quaternion(ee2sp.rot)
	   self.roll_eespf = angles[0]
	   self.pitch_eespf = angles[1]
	   self.yaw_eespf = angles[2]
           print "Translation:", ee2sp.trans, "Rotation:", angles


    def updateImageFeatures(self, featurePoints):
        
 	self.featuresArray[0][0] = featurePoints.corners[0].x
	self.featuresArray[0][1] = featurePoints.corners[0].y
	self.featuresArray[1][0] = featurePoints.corners[1].x
	self.featuresArray[1][1] = featurePoints.corners[1].y
	self.featuresArray[2][0] = featurePoints.corners[2].x
	self.featuresArray[2][1] = featurePoints.corners[2].y
	self.featuresArray[3][0] = featurePoints.corners[3].x
	self.featuresArray[3][1] = featurePoints.corners[3].y
	        

    
    def control(self, eeVelCmds):
        
	#Image Depth
	Z = self.z_eespf
	Zd = 0.48
	f = self.f

	s = zeros((self.numFeatures, 2))

	#Current Features wrt image center
	s[0][0] = -(self.featuresArray[0][0] - self.cu) 
	s[0][1] = -(self.featuresArray[0][1] - self.cv)
	s[1][0] = -(self.featuresArray[1][0] - self.cu)
	s[1][1] = -(self.featuresArray[1][1] - self.cv)
	s[2][0] = -(self.featuresArray[2][0] - self.cu)
	s[2][1] = -(self.featuresArray[2][1] - self.cv)
	s[3][0] = -(self.featuresArray[3][0] - self.cu)
	s[3][1] = -(self.featuresArray[3][1] - self.cv)
	#Desired Features wrt image center
	sd = zeros((self.numFeatures, 2))
	sd[0][0] = -(341 - self.cu)
	sd[0][1] = -(237 - self.cv)
	sd[1][0] = -(504 - self.cu)
	sd[1][1] = -(236 - self.cv)
	sd[2][0] = -(343 - self.cu)
	sd[2][1] = -(389 - self.cv)
	sd[3][0] = -(497 - self.cu)
	sd[3][1] = -(391 - self.cv)

	es = zeros((self.numFeatures, 2))
	es[0][0] = s[0][0] - sd[0][0]
	es[0][1] = s[0][1] - sd[0][1]
	es[1][0] = s[1][0] - sd[1][0]
	es[1][1] = s[1][1] - sd[1][1]
	es[2][0] = s[2][0] - sd[2][0]
	es[2][1] = s[2][1] - sd[2][1]
	es[3][0] = s[3][0] - sd[3][0]
	es[3][1] = s[3][1] - sd[3][1]

	#es = self.featuresArray - sd
	es = array(es).reshape(8,1)

	print"es:",es
	
	#Features in the form for Image Jacobian
	"""x0 = (self.featuresArray[0][0] - self.cu)/self.ax
	gamma0 = (self.featuresArray[0][1] - self.cv)/self.ay	

	x1 = (self.featuresArray[1][0] - self.cu)/self.ax
	gamma1 = (self.featuresArray[1][1] - self.cv)/self.ay
	
        x2 = (self.featuresArray[2][0] - self.cu)/self.ax
	gamma2 = (self.featuresArray[2][1] - self.cv)/self.ay	

        x3 = (self.featuresArray[3][0] - self.cu)/self.ax
	gamma3 = (self.featuresArray[3][1] - self.cv)/self.ay"""	
	

	#Create Interaction Matrix (Image Jacobian)
	""""Lx = [-1/Z,   0.0,  x0/Z,       x0*gamma0,      -(1.0+x0**2),   gamma0, 
               0.0,  -1/Z,  gamma0/Z,   1+gamma0**2,    -x0*gamma0,      -x0,
	      -1/Z,   0.0,  x1/Z,       x1*gamma1,      -(1.0+x1**2),   gamma1, 
               0.0,  -1/Z,  gamma1/Z,   1+gamma1**2,    -x1*gamma1,      -x1,
              -1/Z,   0.0,  x2/Z,       x2*gamma2,      -(1.0+x2**2),   gamma2, 
               0.0,  -1/Z,  gamma2/Z,   1+gamma2*2,    -x2*gamma2,      -x2,
	      -1/Z,   0.0,  x3/Z,       x3*gamma3,      -(1.0+x3**2),   gamma3, 
               0.0,  -1/Z,  gamma3/Z,   1+gamma3**2,    -x3*gamma3,      -x3]"""

	#NOT CORRECT YET
	"""LxH = [-1/Zd,   0.0,  x0/Zd,       x0*gamma0,      -(1.0+x0**2),   gamma0, 
               0.0,  -1/Zd,  gamma0/Zd,   1+gamma0**2,    -x0*gamma0,      -x0,
	      -1/Zd,   0.0,  x1/Zd,       x1*gamma1,      -(1.0+x1**2),   gamma1, 
               0.0,  -1/Zd,  gamma1/Zd,   1+gamma1**2,    -x1*gamma1,      -x1,
              -1/Zd,   0.0,  x2/Zd,       x2*gamma2,      -(1.0+x2**2),   gamma2, 
               0.0,  -1/Zd,  gamma2/Zd,   1+gamma2*2,    -x2*gamma2,      -x2,
	      -1/Zd,   0.0,  x3/Zd,       x3*gamma3,      -(1.0+x3**2),   gamma3, 
               0.0,  -1/Zd,  gamma3/Zd,   1+gamma3**2,    -x3*gamma3,      -x3]"""

	Lx=array([ -f/Z,     0.0,         s[0][0]/Z,          (s[0][0]*s[0][1])/f,       -(f**2+s[0][0]**2)/f,     s[0][1],
                    0.0,    -f/Z,         s[0][1]/Z,          (f**2+s[0][1]**2)/f,       -(s[0][0]*s[0][1])/f,    -s[0][0],
     
                   -f/Z,     0.0,         s[1][0]/Z,          (s[1][0]*s[1][1])/f,       -(f**2+s[1][0]**2)/f,     s[1][1],
                    0.0,     -f/Z,        s[1][1]/Z,          (f**2+s[1][1]**2)/f,       -(s[1][0]*s[1][1])/f,    -s[1][0],

                   -f/Z,     0.0,         s[2][0]/Z,          (s[2][0]*s[2][1])/f,       -(f**2+s[2][0]**2)/f,     s[2][1],
                    0.0,    -f/Z,         s[2][1]/Z,          (f**2+s[2][1]**2)/f,       -(s[2][0]*s[2][1])/f,    -s[2][0],
     
     
                   -f/Z,     0.0,         s[3][0]/Z,          (s[3][0]*s[3][1])/f,       -(f**2+s[3][0]**2)/f,     s[3][1],
                    0.0,    -f/Z,         s[3][1]/Z,          (f**2+s[3][1]**2)/f,       -(s[3][0]*s[3][1])/f,    -s[3][0]])

	Lx = -1.0*Lx #Because I think Shahab ommited this
     

	Lx = array(Lx).reshape(8, 6)
	#LxH = array(Lx).reshape(8, 6)


        #L = 0.5*(Lx+LxH)	   
  	#L = Lx

	

	#K = 1.0*eye(6, dtype=float)
	K = 1.0
	
	#Control Output in CAMERA FRAME
        Ucf = -dot(dot(K,pinv(Lx)),es)

	Re=array([ 0.0, 1.0,  0.0,  0.0,  0.0,  0.0,
     	    	  -1.0, 0.0,  0.0,  0.0,  0.0,  0.0,
                   0.0, 0.0,  1.0,  0.0,  0.0,  0.0,
                   0.0, 0.0,  0.0,  0.0,  1.0,  0.0,
                   0.0, 0.0,  0.0, -1.0,  0.0,  0.0,
                   0.0, 0.0,  0.0,  0.0,  0.0,  1.0])

	"""Re=array([ 0.0, 1.0,  0.0,  0.0,  0.0,  0.0,
     	    	  -1.0, 0.0,  0.0,  0.0,  0.0,  0.0,
                   0.0, 0.0,  1.0,  0.0,  0.0,  0.0,
                   0.0, 0.0,  0.0,  1.0,  0.0,  0.0,
                   0.0, 0.0,  0.0,  0.0,  1.0,  0.0,
                   0.0, 0.0,  0.0,  0.0,  0.0,  1.0])"""

	"""R_cam_inet = array([0.0,   0.0,   -1.0,     0.0,   0.0,   0.0, 
            	            1.0,   0.0,    0.0,     0.0,   0.0,   0.0, 
            	            0.0,  -1.0,    0.0,     0.0,   0.0,   0.0, 
                            0.0,   0.0,    0.0,     0.0,   0.0,  -1.0, 
                            0.0,   0.0,    0.0,     1.0,   0.0,   0.0, 
                            0.0,   0.0,    0.0,     0.0,  -1.0,   0.0])"""


	"""R_init=array([  0.0,   0.0,    1.0,    0.0,  0.0,   0.0, 
       		       -1.0,   0.0,    0.0,    0.0,  0.0,   0.0,
        		0.0,  -1.0,    0.0,    0.0,  0.0,   0.0,
        		0.0,   0.0,    0.0,    0.0,  0.0,   1.0, 
        		0.0,   0.0,    0.0,   -1.0,  0.0,   0.0, 
        		0.0,   0.0,    0.0,    0.0, -1.0,   0.0 ])"""
	
	Re = array(Re).reshape(6,6)

	#R_cam_inet = array(R_cam_inet).reshape(6,6)
	#R_init=array(R_init).reshape(6,6)
	#Control in EE Frame
	U = dot(Re,Ucf) 
	#U = dot(R_cam_inet,Ucf)
	#U = dot(R_init,Ucf)

	eeVelCmds.linear.x = U[0]
	eeVelCmds.linear.y = U[1]
	eeVelCmds.linear.z = U[2]
	eeVelCmds.angular.x = U[3]
	eeVelCmds.angular.y = U[4]
	eeVelCmds.angular.z = U[5]

	print "U:", U
	self.pub_eeVel.publish(eeVelCmds)
	
	
      
if __name__ == '__main__':
    try:
        rospy.init_node('IBVSController')

        IBVSControllerObj = IBVSController(rospy.get_name())

        velCmds = Twist()
	while not rospy.is_shutdown():
                rospy.sleep(IBVSControllerObj.dt/2.0)
		IBVSControllerObj.getEEtoSubPanelFrame()
		IBVSControllerObj.control(velCmds)
		rospy.sleep(IBVSControllerObj.dt/2.0)     
        rospy.spin()

    except rospy.ROSInterruptException: pass
