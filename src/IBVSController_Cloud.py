#!/usr/bin/env python

# ROS imports
import roslib 
roslib.load_manifest('ntua_pandora')
import rospy

# Msgs imports
from sensor_msgs.msg import JointState
from geometry_msgs.msg import PoseStamped, Twist
from udg_pandora.msg import Point2D, Point2DMatched, BoxCorners

# Python imports
from numpy import *
from numpy.linalg import *

# Custom imports
import cola2_lib
import cola2_ros_lib


class IBVSController:
    def __init__(self, name):
        
	# Create publishers --> Publish Arm EE Velocities
	self.pub_eeVel = rospy.Publisher("/visual_servo_uvms/eeVelCmds", Twist)
        
        # Create Subscribers
        rospy.Subscriber("/visual_detector_ee/pixel_matches", BoxCorners, self.updateImageFeatures) #Get Image Features
        
	#Get Initial Time
        self.t0 = rospy.Time.now().to_sec()  
	
	self.numFeatures = 4
	self.featuresArray = empty(2*(self.numFeatures, 2), dtype=float)

	#Get Camera Parameters
	self.cu = rospy.get_param("/visual_detector_ee/image_x_size")/2.0 #Image x Center (width/2)
	self.cv = rospy.get_param("/visual_detector_ee/image_y_size")/2.0 #Image y xenter (height/2)
	self.f = rospy.get_param("/visual_detector_ee/f") #focal length in mm
	self.ax = rospy.get_param("/visual_detector_ee/pixels_mm_x") #focal length in pixels/mm --> x
	self.ay = rospy.get_param("/visual_detector_ee/pixels_mm_y") #focal length in pixels/mm --> y
        
        #Dt
	self.dt = 0.2 #Why?

	#Transformation EndEffector (Target Frame) --> SubPanel Frame (Reference Frame)
	#Initilalize Transformation States
	self.x_eespf = 0.
	self.y_eespf = 0.
	self.z_eespf = 0.
	self.roll_eespf = 0.
	self.pitch_eespf = 0.
	self.yaw_eespf = 0.

	self.ee2sp = lambda : 0
        self.ee2sp.rot = None
        self.ee2sp.trans = None
        # TF listener to compute the transforms
        self.ee2sp.tflistener = tf.TransformListener()

 
    def getEEtoSubPanelFrame(self):
	ee2sp = self.ee2sp
	try:
           ee2sp.trans, ee2sp.rot = ee2sp.tflistener.lookupTransform("/subpanel_centre", "end_effector", rospy.Time(0))
        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
            print "No End Effector to SubPanel Transform available"
	else:
           self.x_eespf = ee2sp.trans[0]
	   self.y_eespf = ee2sp.trans[1]
	   self.z_eespf = ee2sp.trans[2]

	   angles = tf.transformations.euler_from_quaternion(ee2sp.rot)
	   self.roll_eespf = angles[0]
	   self.pitch_eespf = angles[1]
	   self.yaw_eespf = angles[2]
           print "Translation:", ee2sp.trans, "Rotation:", angles


    def updateImageFeatures(self, featurePoints):
        if featurePoints.header.seq > self.msg_id:
	  
           self.numberFeatures = len(featurePoints.img0) #Have to figure out img0 + img1...
           print "Feature Number:", self.numberFeatures
           self.msg_id = self.msg_id + 1
 
	   #self.flag = False
           #self.tfinal = 0.0
           #self.endCounter = 0
   
           self.featuresArray = empty((2*self.numFeatures, 2), dtype=float)  
               
           for i in range (0,len(self.featuresArray)):
                self.featuresArray[i][0] = featurePoints.img0[i].x
	        self.featuresArray[i][1] = featurePoints.img0[i].y
	        

    
    def control(self, eeVelCmds):
        
	#Create Interaction Matrix (Image Jacobian)
	imJac = empty((len(self.featuresArray), 6), dtype=float)

	Z = self.z_eespf
	
	for i in range (0,len(self.featuresArray))

            x = (self.featuresArray[i][0] - self.cu)/self.ax
	    gamma = (self.featuresArray[i][1] - self.cv)/self.ay	
	    #Z = self.z_eespf IN OR OUT OF THE LOOP???

	    Lx = [-1/Z,   0.0,  x/Z,       x*gamma,      -(1.0+x**2),   gamma, 
                   0.0,  -1/Z,  gamma/Z,   1+gamma**2,    -x*gamma,      -x]

	    Lx = array(Lx).reshape(2, 6)

	    imJac[2*i:2*i+1,:] = Lx

	sd = zeros((2*self.numFeatures, 2))
	
	es = self.featuresArray - sd

	K = eye(6, dtype=float)
	
        U = -K*pinv(imJac)*es
	
	eeVelCmds.linear.x = U[0]
	eeVelCmds.linear.y = U[1]
	eeVelCmds.linear.z = U[2]
	eeVelCmds.angular.x = U[3]
	eeVelCmds.angular.y = U[4]
	eeVelCmds.angular.z = U[5]

	self.pub_eeVel.publish(eeVelCmds)
	
	
      
if __name__ == '__main__':
    try:
        rospy.init_node('IBVSController')

        IBVSControllerObj = IBVSController(rospy.get_name())

        velCmds = Twist()
	while not rospy.is_shutdown():
                rospy.sleep(armEFControllerObj.dt/2.0)
		IBVSControllerObj.control()
		rospy.sleep(armEFControllerObj.dt/2.0)     
        rospy.spin()

    except rospy.ROSInterruptException: pass
