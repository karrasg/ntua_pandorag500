#!/usr/bin/env python

# ROS imports
import roslib 
roslib.load_manifest('ntua_pandora')
roslib.load_manifest('udg_pandora')
import rospy
import tf

# Msgs imports
from sensor_msgs.msg import JointState
from geometry_msgs.msg import PoseStamped, Twist
from udg_pandora.msg import Point2D, Point2DMatched, BoxCorners

# Python imports
from numpy import *
from numpy.linalg import *

# Custom imports
import cola2_lib
import cola2_ros_lib


class IBVSController:
    def __init__(self, name):
        
	# Create publishers --> Publish Arm EE Velocities
	self.pub_eeVel = rospy.Publisher("/visual_servo_uvms/eeVelCmds", Twist)
        
        # Create Subscribers
        rospy.Subscriber("/visual_detector_ee/box_corners", BoxCorners, self.updateImageFeatures) #Get Image Features
        
	#Get Initial Time
        self.t0 = rospy.Time.now().to_sec()  
	
	self.numFeatures = 4
	self.featuresArray = empty((self.numFeatures, 2), dtype=float)

	#Get Camera Parameters
	self.cu = rospy.get_param("/visual_detector_ee/image_x_size")/2.0 #Image x Center (width/2)
	self.cv = rospy.get_param("/visual_detector_ee/image_y_size")/2.0 #Image y xenter (height/2)
	self.f = rospy.get_param("/visual_detector_ee/f") #focal length in mm
	self.ax = rospy.get_param("/visual_detector_ee/pixels_mm_x") #focal length in pixels/mm --> x
	self.ay = rospy.get_param("/visual_detector_ee/pixels_mm_y") #focal length in pixels/mm --> y

	self.fax = self.f*self.ax
	self.fay = self.f*self.ay
        
        #Dt
	self.dt = 0.2 #Why?

	#Transformation EndEffector (Target Frame) --> SubPanel Frame (Reference Frame)
	#Initilalize Transformation States
	self.x_eespf = 0.
	self.y_eespf = 0.
	self.z_eespf = 0.48 #To avoid divide by zero
	self.roll_eespf = 0.
	self.pitch_eespf = 0.
	self.yaw_eespf = 0.

	self.ee2sp = lambda : 0
        self.ee2sp.rot = None
        self.ee2sp.trans = None
        # TF listener to compute the transforms
        self.ee2sp.tflistener = tf.TransformListener()

	#Transformation EndEffector (Target Frame) --> World Frame (Reference Frame)
	#Initilalize Transformation States
	self.x_eewf = 0.
	self.y_eewf = 0.
	self.z_eewf = 0.
	self.roll_eewf = 0.
	self.pitch_eewf = 0.
	self.yaw_eewf = 0.

	self.ee2w = lambda : 0
        self.ee2w.rot = None
        self.ee2w.trans = None
        # TF listener to compute the transforms
        self.ee2w.tflistener = tf.TransformListener()

	#Transformation EndEffector (Target Frame) --> Base Frame (Reference Frame)
	#Initilalize Transformation States
	self.x_eebf = 0.
	self.y_eebf = 0.
	self.z_eebf = 0.
	self.roll_eebf = 0.
	self.pitch_eebf = 0.
	self.yaw_eebf = 0.

	self.ee2bf = lambda : 0
        self.ee2bf.rot = None
        self.ee2bf.trans = None
        # TF listener to compute the transforms
        self.ee2bf.tflistener = tf.TransformListener()

	#Transformation Camera (Target Frame) --> World Frame (Reference Frame)
	#Initilalize Transformation States
	self.x_camee = 0.
	self.y_camee = 0.
	self.z_camee = 0.
	self.roll_camee = 0.
	self.pitch_camee = 0.
	self.yaw_camee = 0.

	self.cam2ee = lambda : 0
        self.cam2ee.rot = None
        self.cam2ee.trans = None
        # TF listener to compute the transforms
        self.cam2ee.tflistener = tf.TransformListener()

 
    def getEEtoSubPanelFrame(self):
	ee2sp = self.ee2sp
	try:
           ee2sp.trans, ee2sp.rot = ee2sp.tflistener.lookupTransform("/panel_centre", "end_effector", rospy.Time(0))
        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
            print "No End Effector to Panel Transform available"
	    self.z_eespf = 0.48 #To avoid divide by zero
	else:
           self.x_eespf = ee2sp.trans[0]
	   self.y_eespf = ee2sp.trans[1]
	   self.z_eespf = ee2sp.trans[2]

	   angles = tf.transformations.euler_from_quaternion(ee2sp.rot)
	   self.roll_eespf = angles[0]
	   self.pitch_eespf = angles[1]
	   self.yaw_eespf = angles[2]
           print "EE TO PANELTranslation:", ee2sp.trans, " EE TO PANEL Rotation:", angles

    def getEEtoWorldPose(self):
	ee2w = self.ee2w
	try:
           ee2w.trans, ee2w.rot = ee2w.tflistener.lookupTransform("world", "end_effector", rospy.Time(0))
        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
            print "No End Effector to World transform available"
	else:
           self.x_eewf = ee2w.trans[0]
	   self.y_eewf = ee2w.trans[1]
	   self.z_eewf = ee2w.trans[2]

	   angles = tf.transformations.euler_from_quaternion(ee2w.rot, axes='sxyz')
	   self.roll_eewf = angles[0]
	   self.pitch_eewf = angles[1]
	   self.yaw_eewf = angles[2]
           print "EE TO WORLD Translation:", ee2w.trans, "EE TO WORLD Rotation:", angles

    def getCameratoEE(self):
	cam2ee = self.cam2ee
	try:
           cam2ee.trans, cam2ee.rot = cam2ee.tflistener.lookupTransform("end_effector", "g500/hand_camera", rospy.Time(0))
        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
            print "No Camera to EE transform available"
	else:
           self.x_camee = cam2ee.trans[0]
	   self.y_camee = cam2ee.trans[1]
	   self.z_camee = cam2ee.trans[2]

	   angles = tf.transformations.euler_from_quaternion(cam2ee.rot)
	   self.roll_camee = angles[0]
	   self.pitch_camee = angles[1]
	   self.yaw_camee = angles[2]
           print "HAND CAM TO EE Translation:", cam2ee.trans, "HAND CAM TO EE Rotation:", angles

    
    def getEEtoBase(self):
	ee2bf = self.ee2bf
	try:
           ee2bf.trans, ee2bf.rot = ee2bf.tflistener.lookupTransform("base_arm", "end_effector", rospy.Time(0))
        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
            print "No End Effector to Base transform available"
	else:
           self.x_eebf = ee2bf.trans[0]
	   self.y_eebf = ee2bf.trans[1]
	   self.z_eebf = ee2bf.trans[2]

	   angles = tf.transformations.euler_from_quaternion(ee2bf.rot)
	   self.roll_eebf = angles[0]
	   self.pitch_eebf = angles[1]
	   self.yaw_eebf = angles[2]
           print "EE TO BASE Translation:", ee2bf.trans, "EE TO BASE Rotation:", angles

    def updateImageFeatures(self, featurePoints):
        
 	self.featuresArray[0][0] = featurePoints.corners[0].x
	self.featuresArray[0][1] = featurePoints.corners[0].y
	self.featuresArray[1][0] = featurePoints.corners[1].x
	self.featuresArray[1][1] = featurePoints.corners[1].y
	self.featuresArray[2][0] = featurePoints.corners[2].x
	self.featuresArray[2][1] = featurePoints.corners[2].y
	self.featuresArray[3][0] = featurePoints.corners[3].x
	self.featuresArray[3][1] = featurePoints.corners[3].y
	        

    
    def control(self, eeVelCmds):
        
	#Image Depth
	#Z = self.z_eespf
	Z = 0.40

	s = zeros((self.numFeatures, 2))

	#Current Features wrt image center
	s[0][0] = -(self.featuresArray[0][0] - self.cu) 
	s[0][1] = -(self.featuresArray[0][1] - self.cv)
	s[1][0] = -(self.featuresArray[1][0] - self.cu)
	s[1][1] = -(self.featuresArray[1][1] - self.cv)
	s[2][0] = -(self.featuresArray[2][0] - self.cu)
	s[2][1] = -(self.featuresArray[2][1] - self.cv)
	s[3][0] = -(self.featuresArray[3][0] - self.cu)
	s[3][1] = -(self.featuresArray[3][1] - self.cv)
	#Desired Features wrt image center
	sd = zeros((self.numFeatures, 2))
	sd[0][0] = -(292- self.cu)
	sd[0][1] = -(208 - self.cv)
	sd[1][0] = -(485 - self.cu)
	sd[1][1] = -(208 - self.cv)
	sd[2][0] = -(292 - self.cu)
	sd[2][1] = -(396 - self.cv)
	sd[3][0] = -(485 - self.cu)
	sd[3][1] = -(396 - self.cv)

	es = zeros((self.numFeatures, 2))
	es[0][0] = s[0][0] - sd[0][0]
	es[0][1] = s[0][1] - sd[0][1]
	es[1][0] = s[1][0] - sd[1][0]
	es[1][1] = s[1][1] - sd[1][1]
	es[2][0] = s[2][0] - sd[2][0]
	es[2][1] = s[2][1] - sd[2][1]
	es[3][0] = s[3][0] - sd[3][0]
	es[3][1] = s[3][1] - sd[3][1]

	

	#es = self.featuresArray - sd
	es = array(es).reshape(8,1)

	print"es:",es
	
	fax = self.fax
	fay = self.fay
	
	Lx=array([ -fax/Z,     0.0,         s[0][0]/Z,          (s[0][0]*s[0][1])/fax,       -(fax**2+s[0][0]**2)/fax,     s[0][1],
                    0.0,    -fay/Z,         s[0][1]/Z,          (fay**2+s[0][1]**2)/fay,       -(s[0][0]*s[0][1])/fay,    -s[0][0],
     
                   -fax/Z,     0.0,         s[1][0]/Z,          (s[1][0]*s[1][1])/fax,       -(fax**2+s[1][0]**2)/fax,     s[1][1],
                    0.0,     -fay/Z,        s[1][1]/Z,          (fay**2+s[1][1]**2)/fay,       -(s[1][0]*s[1][1])/fay,    -s[1][0],

                   -fax/Z,     0.0,         s[2][0]/Z,          (s[2][0]*s[2][1])/fax,       -(fax**2+s[2][0]**2)/fax,     s[2][1],
                    0.0,    -fay/Z,         s[2][1]/Z,          (fay**2+s[2][1]**2)/fay,       -(s[2][0]*s[2][1])/fay,    -s[2][0],
     
     
                   -fax/Z,     0.0,         s[3][0]/Z,          (s[3][0]*s[3][1])/fax,       -(fax**2+s[3][0]**2)/fax,     s[3][1],
                    0.0,    -fay/Z,         s[3][1]/Z,          (fay**2+s[3][1]**2)/fay,       -(s[3][0]*s[3][1])/fay,    -s[3][0]])

	
     

	Lx = array(Lx).reshape(8, 6)
	
	K = 150.0
	
	#Control Output in CAMERA FRAME
        Ucf = -K*dot(pinv(Lx),es)

        Ucf[0]=1.0
	Ucf[1]=0.0
	Ucf[2]=0.0
	Ucf[3]=1.0
	Ucf[4]=0.0
	Ucf[5]=0.0
	
	print "CAMERA FRAME VEL:", Ucf
	#Ucf = -K*dot(pinv(Lx[0:2,:]),es[0:2])
	
	# Jacobian from Camera Frame 2 EE Frame
	Re=array([ 0.0, 1.0,  0.0,  0.0,  0.0,  0.0,
     	    	  -1.0, 0.0,  0.0,  0.0,  0.0,  0.0,
                   0.0, 0.0,  1.0,  0.0,  0.0,  0.0,
                   0.0, 0.0,  0.0,  0.0,  1.0,  0.0,
                   0.0, 0.0,  0.0, -1.0,  0.0,  0.0,
                   0.0, 0.0,  0.0,  0.0,  0.0,  1.0])
	
	Re = array(Re).reshape(6,6)

	#End Effector Velocities to End Effector Frame (Local)
	U_EE = dot(Re,Ucf)

	#Jacobian fromm EE to PSEUDO AUV Frame
	"""R_ci = array([ 0.0,    0.0,    1.0,    0.0,   0.0,    0.0, 
                      -1.0,    0.0,    0.0,    0.0,   0.0,    0.0,
                       0.0,   -1.0,    0.0,    0.0,   0.0,    0.0,
                       0.0,    0.0,    0.0,    0.0,   0.0,    1.0, 
                       0.0,    0.0,    0.0,   -1.0,   0.0,    0.0, 
                       0.0,    0.0,    0.0,    0.0,  -1.0,    0.0 ])"""

	R_ci = array([ 0.0,    0.0,    1.0,    0.0,   0.0,    0.0, 
                      -1.0,    0.0,    0.0,    0.0,   0.0,    0.0,
                       0.0,   -1.0,    0.0,    0.0,   0.0,    0.0,
                       0.0,    0.0,    0.0,    0.0,   0.0,    1.0, 
                       0.0,    0.0,    0.0,   -1.0,   0.0,    0.0, 
                       0.0,    0.0,    0.0,    0.0,  -1.0,    0.0 ])
	
	R_ci = array(R_ci).reshape(6,6)

	#End Effector Velocities to UAV Pseudo Frame
	U_AUV = dot(R_ci,Ucf)
	
	
       #Calculate Jacobian from  EE Frame to World
	phi = self.roll_eewf
	theta = self.pitch_eewf 
	psi = self.yaw_eewf

        '''phi = self.roll_eewf
	theta = self.pitch_eewf 
	psi = self.yaw_eewf'''

        

	REEWF = [cos(theta)*cos(psi),  cos(psi)*sin(theta)*sin(phi)-sin(psi)*cos(phi),  sin(psi)*sin(phi)+cos(psi)*cos(phi)*sin(theta),
      	      cos(theta)*sin(psi),  cos(psi)*cos(phi)+sin(phi)*sin(theta)*sin(psi),  sin(psi)*sin(theta)*cos(phi)-cos(psi)*sin(phi),
              -sin(theta),          cos(theta)*sin(phi),                             cos(theta)*cos(phi)]

	REEWF = array(REEWF).reshape(3,3)

	R_90 = array([cos(-pi/2)  , 0 , sin(-pi/2),
                        0  , 1 ,     0      ,
               -sin(-pi/2) , 0 , cos(-pi/2) ])


	R_90 = array(R_90).reshape(3,3)

	RAUV_WF = dot(REEWF,R_90)

	euler_auv_wf = tf.transformations.euler_from_matrix(RAUV_WF, 'sxyz')

	phi = euler_auv_wf[0]
	theta = euler_auv_wf[1]
	psi = euler_auv_wf[2]

	print "roll:", phi, "pitch:", theta, "yaw:", psi

	J1 = [cos(theta)*cos(psi),  cos(psi)*sin(theta)*sin(phi)-sin(psi)*cos(phi),  sin(psi)*sin(phi)+cos(psi)*cos(phi)*sin(theta),
      	      cos(theta)*sin(psi),  cos(psi)*cos(phi)+sin(phi)*sin(theta)*sin(psi),  sin(psi)*sin(theta)*cos(phi)-cos(psi)*sin(phi),
              -sin(theta),          cos(theta)*sin(phi),                             cos(theta)*cos(phi)]

	J2 = [1.0,  sin(phi)*tan(theta),  cos(phi)*tan(theta),
              0.0,  cos(phi),             -sin(phi),
              0.0,  sin(phi)/cos(theta),  cos(phi)/cos(theta)]

	J1 = array(J1).reshape(3,3)
	J2 = array(J2).reshape(3,3)

	print "J1:", J1

	JEE2W = zeros((6,6),float)
	JEE2W[0:3,0:3] = J1
        JEE2W[3:6,3:6] = J2

	#End Effector Velocities from UAV Pseudo Frame to World Frame
	U = dot(JEE2W,U_AUV) #UVMS Scheme !!!!
	#U = dot(JEE2W,U_EE)
	#Calculate Jacobian from  EE Frame to Arm Base Frame
	'''phi = self.roll_eebf
	theta = self.pitch_eebf 
	psi =   self.yaw_eebf 
	print "roll:", phi, "pitch:", theta, "yaw:", psi
	J1 = [cos(theta)*cos(psi),  cos(psi)*sin(theta)*sin(phi)-sin(psi)*cos(phi),  sin(psi)*sin(phi)+cos(psi)*cos(phi)*sin(theta),
      	      cos(theta)*sin(psi),  cos(psi)*cos(phi)+sin(phi)*sin(theta)*sin(psi),  sin(psi)*sin(theta)*cos(phi)-cos(psi)*sin(phi),
              -sin(theta),          cos(theta)*sin(phi),                             cos(theta)*cos(phi)]

	J2 = [1.0,  sin(phi)*tan(theta),  cos(phi)*tan(theta),
              0.0,  cos(phi),             -sin(phi),
              0.0,  sin(phi)/cos(theta),  cos(phi)/cos(theta)]

	J1 = array(J1).reshape(3,3)
	J2 = array(J2).reshape(3,3)

	JEE2BF = zeros((3,3),float)
	JEE2BF = J1
        
	Ub = dot(JEE2BF, U_AUV[0:3],) #Only Arm Linera Velocity Controller WRT Base Frame
			
	#U = zeros((6,1),float)
	#U[0] = Ub[0]
	#U[1] = Ub[1]
	#U[2] = Ub[2]'''

	eeVelCmds.linear.x = U[0]
	eeVelCmds.linear.y = U[1]
	eeVelCmds.linear.z = U[2]
	eeVelCmds.angular.x = U[3]
	eeVelCmds.angular.y = U[4]
	eeVelCmds.angular.z = U[5]

	print "U:", U
	self.pub_eeVel.publish(eeVelCmds)
	
	
      
if __name__ == '__main__':
    try:
        rospy.init_node('IBVSController')

        IBVSControllerObj = IBVSController(rospy.get_name())

        velCmds = Twist()
	while not rospy.is_shutdown():
                rospy.sleep(IBVSControllerObj.dt/2.0)
		#IBVSControllerObj.getCameratoEE()
		#IBVSControllerObj.getEEtoBase()
		#IBVSControllerObj.getEEtoSubPanelFrame()
		IBVSControllerObj.getEEtoWorldPose()
		IBVSControllerObj.control(velCmds)
		rospy.sleep(IBVSControllerObj.dt/2.0)     
        rospy.spin()

    except rospy.ROSInterruptException: pass
