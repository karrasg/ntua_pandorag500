#!/usr/bin/env python

# ROS imports
import roslib 
roslib.load_manifest('cola2_control')
roslib.load_manifest('pose_ekf_slam')
import rospy

# Msgs imports
from auv_msgs.msg import *
from sensor_msgs.msg import JointState
from sensor_msgs.msg import Joy
from pose_ekf_slam.msg import Map
from geometry_msgs.msg import PoseStamped, Twist


import tf
# Python imports
from PyKDL import ChainJntToJacSolver, Chain, Frame, Joint, Segment, JntArray, Jacobian
from numpy import *
from numpy.linalg import *
from math import *
import nlopt

#Service Import
from cola2_control.srv import ComputeInverseKinematics
from std_srvs.srv import Empty, EmptyResponse, EmptyRequest

# Custom imports
import cola2_lib
import cola2_ros_lib
import sys

class UVMSVelocityController:
    def __init__(self, name):
        self.name = name
	# Create publishers
	#Arm
        self.pub_jointCommands = rospy.Publisher("/csip_e5_arm/joint_voltage_command", JointState)
	#Vehicle	
	self.pub_tau = rospy.Publisher("/cola2_control/body_force_req", BodyForceReq)
    
        # Create Subscribers
	#Arm Joints Position
        rospy.Subscriber("/csip_e5_arm/joint_state", JointState, self.updateJointState)
	#End Effector Pose wrt vehicle base frame
	rospy.Subscriber("/arm/pose_stamped", PoseStamped, self.updateArmPose)
	#Arm Cooeficients for Velocity 2 Voltage Transformation	
	rospy.Subscriber("/csip_e5_arm/joint_state_coef", JointState, self.updateJointCoef)
	#Arm Velocity Cmds as given by IBVS
	rospy.Subscriber("/visual_servo_uvms/eeVelCmds", Twist, self.updateVelCmds)
	#Vehicle States
	rospy.Subscriber("/cola2_navigation/nav_sts", NavSts, self.updateNavSts)
	

	#Get Initial Time
        self.t0 = rospy.Time.now().to_sec()  
	
	#Initialize Arm States & Variables
	#Joint States
	self.jointstates = [0.0, 0.0, 0.0, 0.0, 0.0]
	#Previous Joint States
	self.jointstates_prev = [0.0, 0.0, 0.0, 0.0, 0.0]
	#Coefficients for Motors
        self.jointcoef = [0.0, 0.0, 0.0]
	#Initialize EE velocity Cmds
	self.vxEF = 0.0
	self.vyEF = 0.0
	self.vzEF = 0.0
	self.wxEF = 0.0
	self.wyEF = 0.0
	self.wzEF = 0.0

	#Dt
	self.dt = 0.1

	#Initialize KDL Variables --> Jacobian Calculation
        self.jointAngles =JntArray(4)
        self.jacobian = Jacobian(4)
        self.useJacobian = zeros([3,3], float)



	#Initiliaze Vehicle States & Variables
        #Initilize States
	self.x = 0.
	self.y = 0.
	self.z = 0.
        self.roll = 0.
	self.pitch = 0.
	self.yaw = 0.
        self.u = 0.
        self.v = 0.
        self.w = 0.
        self.p = 0.
        self.q = 0.
        self.r = 0.
	self.t_nav = 0.

	#Initilize Desired Velocities	
	self.u_d = 0.
        self.v_d = 0.
        self.w_d = 0.
        self.p_d = 0.
        self.q_d = 0.
        self.r_d = 0.

	#Initialize Integrals for "I" term
	self.integral_u = 0.
        self.integral_v = 0.
        self.integral_w = 0.
        self.integral_r = 0.  
	



	#Initilalize Transformation States
	self.x_eewf = 0.
	self.y_eewf = 0.
	self.z_eewf = 0.
	self.roll_eewf = 0.
	self.pitch_eewf = 0.
	self.yaw_eewf = 0.

	self.ee2w = lambda : 0
        self.ee2w.rot = None
        self.ee2w.trans = None
        # TF listener to compute the transforms
        self.ee2w.tflistener = tf.TransformListener()


        self.enable = False
        #Create services
	self.enable_srv = rospy.Service('/ntua_control_g500/enable_uvms_velocity_control', Empty, self.enableSrv)
        self.disable_srv = rospy.Service('/ntua_control_g500/disable_uvms_velocity_control', Empty, self.disableSrv)     

    def enableSrv(self, req):
        self.enable = True
        rospy.loginfo('%s Enabled', self.name)
        return EmptyResponse()
    
        
    def disableSrv(self, req):
        self.enable = False
        rospy.loginfo('%s Disabled', self.name)
        return EmptyResponse()
        


    def updateJointState(self, jointstates_msg):

        self.jointstates[0] = jointstates_msg.position[0]
	self.jointstates[1] = jointstates_msg.position[1]
	self.jointstates[2] = jointstates_msg.position[2]
	self.jointstates[3] = jointstates_msg.position[3]
	self.jointstates[4] = jointstates_msg.position[4] #NOT USED

    def updateJointCoef(self, jointcoef_msg):

        self.jointcoef[0] = jointcoef_msg.position[0]
	self.jointcoef[1] = jointcoef_msg.position[1]
	self.jointcoef[2] = jointcoef_msg.position[2]

    def updateNavSts(self, nav_sts):
	self.x = nav_sts.position.north
	self.y = nav_sts.position.east
	self.z = nav_sts.position.depth
        self.roll = nav_sts.orientation.roll
	self.pitch = nav_sts.orientation.pitch
	self.yaw = nav_sts.orientation.yaw
        self.u = nav_sts.body_velocity.x
        self.v = nav_sts.body_velocity.y
        self.w = nav_sts.body_velocity.z
        self.p = nav_sts.orientation_rate.roll
        self.q = nav_sts.orientation_rate.pitch
        self.r = nav_sts.orientation_rate.yaw
	self.t_nav = nav_sts.header.stamp.to_sec()

    def updateArmPose(self, armPose_msg):
	self.x_EF = armPose_msg.pose.position.x
	self.y_EF = armPose_msg.pose.position.y
	self.z_EF = armPose_msg.pose.position.z
	qx_EF = armPose_msg.pose.orientation.x
	qy_EF = armPose_msg.pose.orientation.y
	qz_EF = armPose_msg.pose.orientation.z
	qw_EF = armPose_msg.pose.orientation.w

	anglesFromQuat = tf.transformations.euler_from_quaternion([qx_EF, qy_EF, qz_EF, qw_EF])

	self.phi_EF = anglesFromQuat[0]
	self.theta_EF = anglesFromQuat[1]
	self.psi_EF = anglesFromQuat[2]

    def getEEtoWorldPose(self):
	ee2w = self.ee2w
	try:
           ee2w.trans, ee2w.rot = ee2w.tflistener.lookupTransform("world", "end_effector", rospy.Time(0))
        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
            print "No End Effector to World transform available"
	else:
           self.x_eewf = ee2w.trans[0]
	   self.y_eewf = ee2w.trans[1]
	   self.z_eewf = ee2w.trans[2]

	   angles = tf.transformations.euler_from_quaternion(ee2w.rot)
	   self.roll_eewf = angles[0]
	   self.pitch_eewf = angles[1]
	   self.yaw_eewf = angles[2]
           print "EE TO WORLD Translation:", ee2w.trans, "EE TO WORLD Rotation:", angles
		
	   
	   
   

    def calcUVMSJac (self):

	
	#Current Vehicle Pose
	x_v = self.x
	y_v = self.y
	z_v = self.z
	phi_v = self.roll
	th_v = self.pitch
	psi_v = self.yaw

	#Current Arm Joints Position
	q = [self.jointstates[0], self.jointstates[1], self.jointstates[2], self.jointstates[3]]

	#position of the manipulator base frame(0) relative to vehicle body-fixed frame(V)	
	x_v0 = 0.75 #0.53
	y_v0 = 0. 
	z_v0 = 0.45 #0.36
	#rpy-angles of the manipulator base frame(0) relative to vehicle body-fixed frame(V)     
	phi_v0 = 0. 
	th_v0 = 0. 
	psi_v0 = 0. 
     
	#DH Parameters (Arm)
	d1 = 0.              
	d2 = 0.               
	d3 = 0.              
	d4 = 0.29               

	a1 = 0.1
	a2 = 0.26
	a3 = 0.09
	a4 = 0.

	alpha1 = -pi/2
	alpha2 = 0. 
	alpha3 = pi/2
	alpha4 = 0.


	#Arm DH matrix

	DH = [q[0], d1 , a1 , alpha1 ,
       	      q[1], d2 , a2 , alpha2 ,
       	      q[2], d3 , a3 , alpha3 ,
              q[3], d4 , a4 , alpha4 ]

	DH = array(DH).reshape(4,4)

	# Transformation matrix Tiv from vehicle body-fixed frame(V) to inertial frame(I)

	Tiv = [ cos(psi_v)*cos(th_v) , -sin(psi_v)*cos(phi_v)+cos(psi_v)*sin(th_v)*sin(phi_v) ,  sin(psi_v)*sin(phi_v)+cos(psi_v)*sin(th_v)*cos(phi_v) , x_v ,
        	sin(psi_v)*cos(th_v) ,  cos(psi_v)*cos(phi_v)+sin(psi_v)*sin(th_v)*sin(phi_v) , -cos(psi_v)*sin(phi_v)+sin(psi_v)*sin(th_v)*cos(phi_v) , y_v ,
                  -sin(th_v) ,                                   cos(th_v)*sin(phi_v) ,                                   cos(th_v)*cos(phi_v) , z_v ,
                           0.,                                                      0.,                                                      0., 1.0 ]

	Tiv = array(Tiv).reshape(4,4)	


	# Transformation matrix Tv0 from manipulator base frame(0) to vehicle body-fixed frame(V) (constant matrix)
	Tv0 = [cos(psi_v0)*cos(th_v0),-sin(psi_v0)*cos(phi_v0)+cos(psi_v0)*sin(th_v0)*sin(phi_v0),sin(psi_v0)*sin(phi_v0)+cos(psi_v0)*sin(th_v0)*cos(phi_v0),x_v0,
               sin(psi_v0)*cos(th_v0),cos(psi_v0)*cos(phi_v0)+sin(psi_v0)*sin(th_v0)*sin(phi_v0),-cos(psi_v0)*sin(phi_v0)+sin(psi_v0)*sin(th_v0)*cos(phi_v0),y_v0,
                   -sin(th_v0) ,                                      cos(th_v0)*sin(phi_v0) ,                                      cos(th_v0)*cos(phi_v0),  z_v0,
                             0.,                                                           0.,                                               0.           ,  1.0 ]

	Tv0 = array(Tv0).reshape(4,4)               
	
	#Transformation matrix Tmi from manipulator frame (ii) to manipulator frame(ii-1)

    
	Tm0 = [cos(DH[0,0]), -sin(DH[0,0])*cos(DH[0,3]),  sin(DH[0,0])*sin(DH[0,3]), DH[0,2]*cos(DH[0,0]) ,
               sin(DH[0,0]),  cos(DH[0,0])*cos(DH[0,3]), -cos(DH[0,0])*sin(DH[0,3]), DH[0,2]*sin(DH[0,0]) ,
               0            ,  sin(DH[0,3])              ,  cos(DH[0,3])              , DH[0,1]           ,    
               0            ,  0                          ,  0                          , 1               ]
          
	Tm1 = [cos(DH[1,0]), -sin(DH[1,0])*cos(DH[1,3]),  sin(DH[1,0])*sin(DH[1,3]), DH[1,2]*cos(DH[1,0]) ,
               sin(DH[1,0]),  cos(DH[1,0])*cos(DH[1,3]), -cos(DH[1,0])*sin(DH[1,3]), DH[1,2]*sin(DH[1,0]) ,
               0            ,  sin(DH[1,3])              ,  cos(DH[1,3])              , DH[1,1]           ,
               0            ,  0                          ,  0                          , 1               ]

	Tm2 = [cos(DH[2,0]), -sin(DH[2,0])*cos(DH[2,3]),  sin(DH[2,0])*sin(DH[2,3]), DH[2,2]*cos(DH[2,0]) ,
               sin(DH[2,0]),  cos(DH[2,0])*cos(DH[2,3]), -cos(DH[2,0])*sin(DH[2,3]), DH[2,2]*sin(DH[2,0]) ,
               0            ,  sin(DH[2,3])              ,  cos(DH[2,3])              , DH[2,1]           ,
               0            ,  0                          ,  0                          , 1               ]

	Tm3 = [cos(DH[3,0]), -sin(DH[3,0])*cos(DH[3,3]),  sin(DH[3,0])*sin(DH[3,3]), DH[3,2]*cos(DH[3,0]) ,
               sin(DH[3,0]),  cos(DH[3,0])*cos(DH[3,3]), -cos(DH[3,0])*sin(DH[3,3]), DH[3,2]*sin(DH[3,0]) ,
               0            ,  sin(DH[3,3])              ,  cos(DH[3,3])              , DH[3,1]           ,   
               0            ,  0                          ,  0                          , 1               ]

	Tm0 = array(Tm0).reshape(4,4)
	Tm1 = array(Tm1).reshape(4,4)
	Tm2 = array(Tm2).reshape(4,4)
	Tm3 = array(Tm3).reshape(4,4)
          

	# Transformation matrix T4e from e-e frame(e) to manipulator frame(4) (Constant Matrix)

	T4e = [cos(-pi/2)  , 0 , sin(-pi/2) , 0 ,
                        0  , 1 ,     0      , 0 ,
               -sin(-pi/2) , 0 , cos(-pi/2) , 0 ,
                        0  , 0 ,     0      , 1 ]


	T4e = array(T4e).reshape(4,4)

	# UVMS Transformation matrix Tie from e-e frame(e) to inertial frame(I)

	T01 = Tm0
	T02 = dot(T01,Tm1)
	T03 = dot(T02,Tm2)
	T04 = dot(T03,Tm3)
	T0e = dot(T04,T4e) #Additional Transformation Added by Kambras (potential singularity if removed!!!)
	#T0e = T04

	r0e = T0e[0:3,3]
	r1e = T0e[0:3,3]-T01[0:3,3]
	r2e = T0e[0:3,3]-T02[0:3,3]
	r3e = T0e[0:3,3]-T03[0:3,3]

	b0 = array([0,0,1])
	b1 = T01[0:3,2]
	b2 = T02[0:3,2]
	b3 = T03[0:3,2]

	cb0r0e = cross(b0,r0e) 
	cb1r1e = cross(b1,r1e) 
	cb2r2e = cross(b2,r2e) 
	cb3r3e = cross(b3,r3e)

	cb0r0e = array(cb0r0e).reshape(3,1)
	cb1r1e = array(cb1r1e).reshape(3,1)
	cb2r2e = array(cb2r2e).reshape(3,1)
	cb3r3e = array(cb3r3e).reshape(3,1)

	r0e = array(r0e).reshape(3,1)
	r1e = array(r1e).reshape(3,1)
	r2e = array(r2e).reshape(3,1)
	r3e = array(r3e).reshape(3,1)

	b0 = array(b0).reshape(3,1)
	b1 = array(b1).reshape(3,1)
	b2 = array(b2).reshape(3,1)
	b3 = array(b3).reshape(3,1)


	#print "r1e:", r3e
	#print "T0e:", T0e[:,3] - T03[:,3]

	#SWSTA MEXRI EDW	
	
	#Manipulator geometric Jacobian --> Calculated without KDL
	"""Jm = [ cross(b0,r0e) , cross(b1,r1e) , cross(b2,r2e) , cross(b3,r3e),
                 b0  ,           b1  ,           b2  ,           b3  ] """

	#print "shape cb0r03:", cb0r0e.shape

	#Jm = [cb0r0e, cb1r1e, cb2r2e, cb3r3e, b0, b1, b2, b3]        
        Jm = zeros((6,4),float)
	#print "Shape Jm", cb0r0e.shape
	Jm[0:3,0] = cb0r0e.T
	Jm[0:3,1] = cb1r1e.T
	Jm[0:3,2] = cb2r2e.T
	Jm[0:3,3] = cb3r3e.T
	Jm[3:6,0] = b0.T
	Jm[3:6,1] = b1.T
	Jm[3:6,2] = b2.T
	Jm[3:6,3] = b3.T

	#print "Jm:", Jm

	#MEXRI EDW SWSTA
	
	Jm1 = Jm[0:3, :]             # submatrix of Jacobian corresponding to translational velocity
	Jm2 = Jm[3:6, :]             # submatrix of Jacobian corresponding to rotational velocity  


	Tve = dot(Tv0,T0e)
	p1 = Tve[0:3,3]

	ss = dot(-self.skew(dot(Tiv[0:3,0:3],p1)),Tiv[0:3,0:3])
	ss = array(ss).reshape(3,3)

	#UVMS Jacobian
	Jw = zeros([6,10])

	Jw[0:3,0:3] = Tiv[0:3,0:3]
	Jw[0:3,3:6] = ss
	Jw[0:3,6:10] = dot(dot(Tiv[0:3,0:3],Tv0[0:3,0:3]),Jm1)

	Jw[3:6,0:3] = zeros([3,3])
	
	#Jw[3:6,3:6] = Tiv[0:3,0:3] #Both Wrong after discussing with Kampras
	#Jw[3:6,6:10] =  dot(dot(Tiv[0:3,0:3],Tv0[0:3,0:3]),Jm2)
	
	phi_ee = self.roll_eewf 
	theta_ee = self.pitch_eewf 
	

	J2EE = [1.0,  sin(phi_ee)*tan(theta_ee),  cos(phi_ee)*tan(theta_ee),
                0.0,  cos(phi_ee),             -sin(phi_ee),
                0.0,  sin(phi_ee)/cos(theta_ee),  cos(phi_ee)/cos(theta_ee)]

	

	J2EE = array(J2EE).reshape(3,3)

	R_VE = Tve[0:3,0:3]
	R_VE = array(R_VE).reshape(3,3)
	R_0E = T0e[0:3,0:3]
	R_0E = array(R_0E).reshape(3,3)

	R_VE = R_VE.T
	R_0E = R_0E.T

	Jw[3:6,3:6] = dot(J2EE,R_VE)
	Jw[3:6,6:10] = dot(dot(J2EE,R_0E),Jm2)

	
	Jw = array(Jw).reshape(6,10)
	
	return Jw

    def skew(self,v): #Skew Symmetric Calculation
        if len(v) == 4: v = v[:3]/v[3]
        skv = roll(roll(diag(v.flatten()), 1, 1), -1, 0)
        return skv - skv.T

    def initArmDH(self):
     
        DH_a1 = 0.1
        DH_a2 = 0.26
        DH_a3 = 0.09
        DH_d4 = 0.29
        DH_alpha1 = -1.57
        DH_alpha3 = 1.57     

        self.chain=Chain()

        joint0 = Joint(Joint.RotZ) 
        frame0 = Frame().DH(DH_a1, DH_alpha1, 0, 0)
        segment0 = Segment(joint0,frame0)
        self.chain.addSegment(segment0) 
 

        joint1 = Joint(Joint.RotZ)
        frame1 = Frame().DH(DH_a2, 0, 0, 0 )
        segment1 = Segment(joint1,frame1)
        self.chain.addSegment(segment1)
 
        joint2 = Joint(Joint.RotZ)
        frame2 = Frame().DH(DH_a3, DH_alpha3, 0, 0)
        segment2 = Segment(joint2,frame2)
        self.chain.addSegment(segment2)
         
        joint3 = Joint(Joint.RotZ)
        frame3 = Frame().DH(0, 0, DH_d4, 0)
        segment3 = Segment(joint3,frame3)
        self.chain.addSegment(segment3)

    def updateVelCmds(self,velCmds_msg):
	self.vxEF = velCmds_msg.linear.x
	self.vyEF = velCmds_msg.linear.y
	self.vzEF = velCmds_msg.linear.z
	self.wxEF = velCmds_msg.angular.x
	self.wyEF = velCmds_msg.angular.y
	self.wzEF = velCmds_msg.angular.z

    """"def calcArmJacobian(self): 
     
        self.jointAngles[0] = self.jointstates[0]
        self.jointAngles[1] = self.jointstates[1]
        self.jointAngles[2] = self.jointstates[2]
        self.jointAngles[3] = self.jointstates[3]
        
        solver = ChainJntToJacSolver(self.chain)
        solver.JntToJac(self.jointAngles,self.jacobian)
       

	#Take only the 3x3 part of the 6x3 Jacobian
        self.useJacobian[0,0] = self.jacobian[0,0]
        self.useJacobian[0,1] = self.jacobian[0,1]
        self.useJacobian[0,2] = self.jacobian[0,2]
        self.useJacobian[1,0] = self.jacobian[1,0]
        self.useJacobian[1,1] = self.jacobian[1,1]
        self.useJacobian[1,2] = self.jacobian[1,2]
        self.useJacobian[2,0] = self.jacobian[2,0]
        self.useJacobian[2,1] = self.jacobian[2,1]
        self.useJacobian[2,2] = self.jacobian[2,2]"""

    def UVMSVelocityScheme(self,Jw):

	
	K = eye(10,10) #UVMS Gain Selection Matrix
	K_KSI = 10.0*eye(10,10) #Constraints Gain Matrix

	K[0][0] = 70.0
	K[1][1] = 70.0
	K[2][2] = 70.0
	K[3][3] = 1000000000.
	K[4][4] = 1000000000.
	K[5][5] = 70.0
	K[6][6] = 1.0
	K[7][7] = 1.
	K[8][8] = 1.0
	K[9][9] = 1.

       
	#Velocity Commands for the End Effector (In this case provided by IBVS)
	vCmds = array([self.vxEF, self.vyEF, self.vzEF, self.wxEF, self.wyEF, self.wzEF]) 
	#vCmds = array([0.0, 0.0, 0.0, 0.0, 0.0, 0.0]) 
	#vCmds = array([self.vxEF, self.vyEF, self.vzEF, 0.0, 0.0, 0.0])
	vCmds = array(vCmds).reshape(6,1)

	#Maximun & Minimum Joint Positions
	q_arm_max = array([ 1.46, 1.314, 0.73, 3.14])
        q_arm_min = array([ -0.52, -0.1471, -1.297, -3.14])

	q0 = self.jointstates[0]
	q1 = self.jointstates[1]
	q2 = self.jointstates[2]
	q3 = self.jointstates[3]

	q0max = q_arm_max[0]
	q1max = q_arm_max[1]
	q2max = q_arm_max[2]
	q3max = q_arm_max[3]

	q0min = q_arm_min[0]
	q1min = q_arm_min[1]
	q2min = q_arm_min[2]
	q3min = q_arm_min[3]

	
	Vg = array([0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
	      (q0 - q0min)*(q1 - q1min)*(q1 - q1max)*(q2 - q2min)*(q2 - q2max)*(q3 - q3min)*(q3 - q3max) + (q0 - q0max)*(q1 - q1min)*(q1 - q1max)*(q2 - q2min)*(q2 - q2max)*(q3 - q3min)*(q3 - q3max),
              (q0 - q0min)*(q0 - q0max)*(q1 - q1min)*(q2 - q2min)*(q2 - q2max)*(q3 - q3min)*(q3 - q3max) + (q0 - q0min)*(q0 - q0max)*(q1 - q1max)*(q2 - q2min)*(q2 - q2max)*(q3 - q3min)*(q3 - q3max),
              (q0 - q0min)*(q0 - q0max)*(q1 - q1min)*(q1 - q1max)*(q2 - q2min)*(q3 - q3min)*(q3 - q3max) + (q0 - q0min)*(q0 - q0max)*(q1 - q1min)*(q1 - q1max)*(q2 - q2max)*(q3 - q3min)*(q3 - q3max),
              (q0 - q0min)*(q0 - q0max)*(q1 - q1min)*(q1 - q1max)*(q2 - q2min)*(q2 - q2max)*(q3 - q3min) + (q0 - q0min)*(q0 - q0max)*(q1 - q1min)*(q1 - q1max)*(q2 - q2min)*(q2 - q2max)*(q3 - q3max)])
 
	Vg = array(Vg).reshape(10,1)

	
	JK = dot(Jw,inv(K))
	JwPT = pinv(JK)

	qUVMS = dot(dot(inv(K),JwPT),vCmds) + 1.0*dot(dot( inv(K) , (eye(10,10) - dot(JwPT,JK) ) ) ,dot(K_KSI,Vg) )
	#qUVMS = dot(JwPT,vCmds) 
	
	#VEHICLE CMDS
	qUVMS[0] = 0.1*qUVMS[0]
	qUVMS[1] = 0.1*qUVMS[1]
	qUVMS[2] = 0.1*qUVMS[2]
	qUVMS[3] = 0.1*qUVMS[3]
	qUVMS[4] = 0.1*qUVMS[4]
	qUVMS[5] = 0.1*qUVMS[5]
	
	#ARM CMDS
	qUVMS[6] = 0.1*qUVMS[6]
	qUVMS[7] = 0.1*qUVMS[7]
	qUVMS[8] = 0.1*qUVMS[8]
	qUVMS[9] = 0.1*qUVMS[9]

	

	print "qUVMS:", qUVMS

	"""bdv = array([self.u, self.v,self.w,self.p, self.q, self.r, 0.0, 0.0, 0.0, 0.0])
	bdv = array(bdv).reshape(10,1)
	VEE = dot(Jw,bdv)

	print "VEE:", VEE"""
	
	return qUVMS
	
      
    
    def armVelcontrol(self, desJoints, qvelctrl):     
   
        desJoints.header.stamp = rospy.Time.now()
    	desJoints.header.frame_id = ""
    	
	qvel0 = (self.jointstates[0] - self.jointstates_prev[0])/self.dt
	qvel1 = (self.jointstates[1] - self.jointstates_prev[1])/self.dt
	qvel2 = (self.jointstates[2] - self.jointstates_prev[2])/self.dt
	qvel3 = (self.jointstates[3] - self.jointstates_prev[3])/self.dt
	

	kq = 1000.0

        velCmdJ0 = int(0.6*kq*self.jointcoef[0]*qvelctrl[0]*65535)
        velCmdJ1= int(0.5*kq*self.jointcoef[1]*qvelctrl[1]*65535)
        velCmdJ2 = int(0.85*kq*self.jointcoef[2]*qvelctrl[2]*65535)	
	velCmdJ3 = int(qvelctrl[3]*65535)

	

	"""velCmdJ0 = int(-self.jointcoef[0]*kq*(qvel0 - qvelctrl[0])*65535)
        velCmdJ1= int(-self.jointcoef[1]*kq*(qvel1 - qvelctrl[1])*65535)
        velCmdJ2 = int(-self.jointcoef[2]*kq*(qvel2 - qvelctrl[2])*65535)	
	velCmdJ3 = int(-kq*(qvel3 - qvelctrl[3])*65535)"""
	#print "qvel0:", qvel0, "qvel1:", qvel1, "qvel2:", qvel2, "qvel3:", qvel3
	print " JOINT VELOCITY ERRORS --> ", "eq0:", qvel0 - qvelctrl[0], "eq1:", qvel1 - qvelctrl[1], "eq2:", qvel2 - qvelctrl[2], "eq3:", qvel3 - qvelctrl[3]	
	
        desJoints.position = [  velCmdJ0,  velCmdJ1,  velCmdJ2, 1.0*velCmdJ3  ,  0]
        desJoints.effort = [0.0, 0.0, 0.0, 0.0, 0.0]
        
        #desJoints.position = [  -0.01*invJac[0,2],   -0.01*invJac[1,2],   -0.01*invJac[2,2],   0,  0]
  	#print desJoints.position
	self.pub_jointCommands.publish(desJoints)

	self.jointstates_prev[0] = self.jointstates[0]
        self.jointstates_prev[1] = self.jointstates[1]
        self.jointstates_prev[2] = self.jointstates[2]
        self.jointstates_prev[3] = self.jointstates[3]
        self.jointstates_prev[4] = self.jointstates[4]
      
       
    def vehicleVelCtrl(self, wrenchMsg, VelDes):

   	#Filling up states
	x = self.x
	y = self.y
	z = self.z
	phi = self.roll
	theta = self.pitch
	psi = self.yaw
	u = self.u
	v = self.v
	w = self.w
	p = self.p
	q = self.q
	r = self.r

	#Model Free Robust Velocity Controller

	u_d = VelDes[0]
	v_d = VelDes[1]
	w_d = VelDes[2]
	r_d = VelDes[5]

	eu = u - u_d
	ev = v - v_d
	ew = w - w_d
	er = r - r_d
	
	print " VEHICLE VELOCITY ERRORS --> ", "eu:", eu, "ev:", ev, "ew:", ew, "er:", er

	k_u=400.0 * 0.25
        k_v=400.0 *0.25
	k_w=420.0 * 0.25
	k_q=0.0
	k_r=400.0 * 0.25

        ki_u = 5.0
        ki_v = 5.0
	ki_w = 5.0
	ki_q = 5.0
	ki_r = 5.0

	

	self.integral_u = self.integral_u + eu*self.dt
        self.integral_v = self.integral_v + ev*self.dt
        self.integral_w = self.integral_w + ew*self.dt
        self.integral_r = self.integral_r + er*self.dt      

        if self.integral_u > 10.0:
           self.integral_u = 10.0
        if self.integral_u < -10.0:
           self.integral_u = -10.0 

	if self.integral_v > 10.0:
           self.integral_v = 10.0
        if self.integral_v < -10.0:
           self.integral_v = -10.0
  
        if self.integral_w > 10.0:
           self.integral_w = 10.0
        if self.integral_w < -10.0:
           self.integral_w = -10.0 

        if self.integral_r > 10.0:
           self.integral_r = 10.0
        if self.integral_r < -10.0:
           self.integral_r = -10.0


	X = -k_u*eu - ki_u*self.integral_u
	Y = -k_v*ev - ki_v*self.integral_v
        Z = -k_w*ew - ki_w*self.integral_w
        #M = -k_theta*tempR[0]
        M = 0.0
	N = -k_r*er - ki_r*self.integral_r

        #X=0.0
        #Y=0.0
        #Z = 0.0
        #M=0.0
        #N=0.0	

	#Saturate Control Signals (Body Forces)
	if X > 100.0:
	   X = 100.0
	elif X < -100.0:
             X = -100.0	

	if Y > 100.0:
	   Y = 100.0
	elif Y < -100.0:
             Y = -100.0	

	if Z > 100.0:
	   Z = 100.0
	elif Z < -100.0:
             Z = -100.0	

	if M > 100.0:
	   M = 100.0
	elif M < -100.0:
             M = -100.0

	if N > 100.0:
	   N = 100.0
	elif N < -100.0:
             N = -100.0		

	U = array([X, Y, Z, M, N])
	#U = array([-X, 0, 0, 0, 0])

	#print "U:", U
                
        wrenchMsg.header.stamp = rospy.Time.now()
        wrenchMsg.header.frame_id = "vehicle_frame"
        wrenchMsg.goal.requester = "uvms velocity controller"
        wrenchMsg.goal.id = 0
        wrenchMsg.goal.priority = GoalDescriptor.PRIORITY_NORMAL
        wrenchMsg.wrench.force.x = U[0]
        wrenchMsg.wrench.force.y = U[1]
        wrenchMsg.wrench.force.z = U[2]        
        wrenchMsg.wrench.torque.x = 0.
        wrenchMsg.wrench.torque.y = U[3]
        wrenchMsg.wrench.torque.z = U[4]
            
        wrenchMsg.disable_axis.x = False
        wrenchMsg.disable_axis.y = False
        wrenchMsg.disable_axis.z = False
        wrenchMsg.disable_axis.roll = True
        wrenchMsg.disable_axis.pitch = True
        wrenchMsg.disable_axis.yaw = False
            
        self.pub_tau.publish(wrenchMsg)
	
	return 
   


if __name__ == '__main__':
    try:
        rospy.init_node('UVMSG500VelocityController')
	rospy.loginfo('UVMSG500VelocityController Node is up')
        UVMSVelObj = UVMSVelocityController(rospy.get_name())

	

	jointCmd_msg = JointState()
	wrench_msg = BodyForceReq()
	#WP_msg = WorldWaypointReq()
	
	while not rospy.is_shutdown():
		t =  rospy.Time.now().to_sec() - UVMSVelObj.t0 
		t1 = rospy.Time.now().to_sec()
                if UVMSVelObj.enable == True:
		   UVMSVelObj.getEEtoWorldPose()
		   Jw = UVMSVelObj.calcUVMSJac()
		   qUVMS = UVMSVelObj.UVMSVelocityScheme(Jw)
		   UVMSVelObj.armVelcontrol(jointCmd_msg,qUVMS[6:10])
	           UVMSVelObj.vehicleVelCtrl(wrench_msg, qUVMS[0:6])
	           #UVMSVelObj.armVelcontrol(jointCmd_msg,array([0.00,0.00,0.00,0.00]))
		   #UVMSVelObj.vehicleVelCtrl(wrench_msg, array([0,0,0,0,0,0]))
		   rospy.sleep(UVMSVelObj.dt)
		t2 = rospy.Time.now().to_sec()
		#print "Elapsed:", t2-t1
		#print "qUVMS:", qUVMS
        rospy.spin()

    except rospy.ROSInterruptException: pass
