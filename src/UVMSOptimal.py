#!/usr/bin/env python

# ROS imports
import roslib 
roslib.load_manifest('cola2_control')
roslib.load_manifest('pose_ekf_slam')
import rospy

# Msgs imports
from auv_msgs.msg import *
from sensor_msgs.msg import JointState
from sensor_msgs.msg import Joy
from pose_ekf_slam.msg import Map
from geometry_msgs.msg import PoseStamped

import tf
# Python imports
from numpy import *
from numpy.linalg import *
from math import *
import nlopt

# Custom imports
import cola2_lib
import cola2_ros_lib
import sys

class UVMSOptimal:
    def __init__(self, name):
        
	# Create publishers
	#Arm
        self.pub_armMSG = rospy.Publisher("/csip_e5_arm/joint_state_command", JointState)
	#Vehicle	
	self.pub_auvMSG = rospy.Publisher("/ntua_planner/world_waypoint_req", WorldWaypointReq)
    
        # Create Subscribers
	#Arm Joints Position
        rospy.Subscriber("/csip_e5_arm/joint_state", JointState, self.updateJointState)
	#Vehicle States
	rospy.Subscriber("/cola2_navigation/nav_sts", NavSts, self.updateNavSts)
	#Desired Arm & Vehicle States	
	rospy.Subscriber("/IBVS_UVMS/EE_World", JointState, self.updateEEDesiredState) #TYPE is JointState BUT Contains EE Pose!!!!

	#Get Initial Time
        self.t0 = rospy.Time.now().to_sec()  
	
	#Initialize Arm States & Variables
	#Joint States
	self.jointstates = [0.0, 0.0, 0.0, 0.0, 0.0]

	#Initilize AUV States
	self.x = 0.
	self.y = 0.
	self.z = 0.
        self.roll = 0.
	self.pitch = 0.
	self.yaw = 0.
        self.u = 0.
        self.v = 0.
        self.w = 0.
        self.p = 0.
        self.q = 0.
        self.r = 0.
	self.t_nav = 0.
	
	#Initilize Desired AUV States
	self.x_des = 0.0
	self.y_des = 0.0
	self.z_des = 0.0
	self.theta_des = 0.0
	self.psi_des = 0.0
	
	#Initialize Desired EE States WRT World 
	self.xe_d = -0.9  
	self.ye_d = -0.5
	self.ze_d = -0.4 
	self.phi_ed = 0.0	
	self.th_ed = -pi/9.0
	self.psi_ed = -pi/10.0


	#Force and Torque Metrics
	self.Fuvms = 0.0
	self.Tuvms = 0.0
	
	#Initialize Optimization Stuff
	#Equality Constraints
	self.hc = zeros((8,1),float)
	self.UVMSStates = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
	self.uvmsOpt = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]

       
    def updateJointState(self, jointstates_msg):

        self.jointstates[0] = jointstates_msg.position[0]
	self.jointstates[1] = jointstates_msg.position[1]
	self.jointstates[2] = jointstates_msg.position[2]
	self.jointstates[3] = jointstates_msg.position[3]
	self.jointstates[4] = jointstates_msg.position[4] #NOT USED


    def updateNavSts(self, nav_sts):
	self.x = nav_sts.position.north
	self.y = nav_sts.position.east
	self.z = nav_sts.position.depth
        self.roll = nav_sts.orientation.roll
	self.pitch = nav_sts.orientation.pitch
	self.yaw = nav_sts.orientation.yaw
        self.u = nav_sts.body_velocity.x
        self.v = nav_sts.body_velocity.y
        self.w = nav_sts.body_velocity.z
        self.p = nav_sts.orientation_rate.roll
        self.q = nav_sts.orientation_rate.pitch
        self.r = nav_sts.orientation_rate.yaw
	self.t_nav = nav_sts.header.stamp.to_sec()

    def updateEEDesiredState(self, EEDesired_msg):

        self.xe_d = EEDesired_msg.position[0]
	self.ye_d = EEDesired_msg.position[1]
	self.ze_d = EEDesired_msg.position[2]
	self.phi_ed = EEDesired_msg.position[3]
	self.th_ed = EEDesired_msg.position[4]
        self.psi_ed = EEDesired_msg.position[5]

    
    def objFun(self,x, grad):

         w = 0.1
         norm = -(w*x[10] + (1-w)*x[11])

	 if grad.size > 0:
            print 'error'
	    exit(0)


         """if grad.size > 0:
            grad[0] = 0.0
            grad[1] = 0.0
	    grad[2] = 0.0
	    grad[3] = 0.0
	    grad[4] = 0.0
	    grad[5] = 0.0
	    grad[6] = 0.0
	    grad[7] = 0.0
	    grad[8] = 0.0
	    grad[9] = 0.0
	    grad[10] = -w
	    grad[11] = w-1"""	

         return norm


    def myconstraintsEQ(self,result, x, grad):
	
	if grad.size > 0:
           print 'error'
	   exit(0)


        #equality constraints
        #hc(1,1) = x4 ----> phi(roll) AUV na einai mhden
        #hc(2,1) = x5 ----> th(pitch) AUV na einai mhden
        #hc(3,1) = x_er ----> x tou e-e na einai to desired
        #hc(4,1) = y_er ----> y tou e-e na einai to desired
        #hc(5,1) = z_er ----> z tou e-e na einai to desired
        #hc(6,1) = phi_er ----> phi tou e-e na einai to desired
        #hc(7,1) = th_er ----> th tou e-e na einai to desired
        #hc(8,1) = psi_er ----> psi tou e-e na einai to desired

        # inequality constraints
        #gc(1:10,1) = t_lb - C*[x11 ; x12] - g' ----> thrusters capability range
        #gc(11:20,1) = C*[x11 ; x12] + g' - t_ub ----> thrusters capability range


        #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        #syms x1 x2 x3 x4 x5 x6 x7 x8 x9 x10 x11 x12 psi_ed th_ed phi_ed xe_d ye_d ze_d

        #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	x1 = x[0]
	x2 = x[1]
	x3 = x[2]
	x4 = x[3]
	x5 = x[4]
	x6 = x[5]
	x7 = x[6]
	x8 = x[7]
	x9 = x[8]
	x10 = x[9]
	x11 = x[10]
	x12 = x[11]
	
	#EE Desired States WRT World Frame
	xe_d = self.xe_d    
	ye_d = self.ye_d
	ze_d = self.ze_d
	psi_ed = self.psi_ed    
	th_ed = self.th_ed
	phi_ed = self.phi_ed
	
	# Simplify Tie

	# simplify Tie(2,1)
	Tie21 = sin(x8 + x9)*sin(x7)*(cos(x4)*cos(x6) + sin(x4)*sin(x5)*sin(x6)) - cos(x8 + x9)*(cos(x6)*sin(x4) - cos(x4)*sin(x5)*sin(x6)) + sin(x8 + x9)*cos(x5)*cos(x7)*sin(x6)

	# simplify Tie(1,1)
	Tie11 = cos(x8 + x9)*(sin(x4)*sin(x6) + cos(x4)*cos(x6)*sin(x5)) - sin(x8 + x9)*sin(x7)*(cos(x4)*sin(x6) - cos(x6)*sin(x4)*sin(x5)) + sin(x8 + x9)*cos(x5)*cos(x6)*cos(x7)

	# simplify Tie(3,1)
	Tie31 = cos(x8 + x9)*cos(x4)*cos(x5) - sin(x8 + x9)*cos(x7)*sin(x5) + sin(x8 + x9)*cos(x5)*sin(x4)*sin(x7)

	# simplify Tie(3,2)
	Tie32 = sin(x5)*(cos(x10)*sin(x7) + cos(x8 + x9)*cos(x7)*sin(x10)) + cos(x5)*sin(x4)*(cos(x7)*cos(x10) - cos(x8 + x9)*sin(x7)*sin(x10)) + sin(x8 + x9)*cos(x4)*cos(x5)*sin(x10)

	# simplify Tie(3,3)
	Tie33 = sin(x5)*(+ cos(x10)*cos(x8 + x9)*cos(x7) - sin(x10)*sin(x7)) + cos(x4)*cos(x5)*sin(x8 + x9)*cos(x10) + cos(x5)*sin(x4)*(- sin(x10)*cos(x7) - cos(x10)*cos(x8 + x9)*sin(x7))

	# simplify pe1(1)=Tie(1,4)
	pe1_1 = x1 + (3*cos(x5)*cos(x6))/4 + (9*sin(x4)*sin(x6))/20 - (sin(x4)*sin(x6) + cos(x4)*cos(x6)*sin(x5))*((9*sin(x8 + x9))/100 - (29*cos(x8 + x9))/100 + (13*sin(x8))/50) + (9*cos(x4)*cos(x6)*sin(x5))/20 - (sin(x7)*(cos(x4)*sin(x6) - cos(x6)*sin(x4)*sin(x5))*(9*cos(x8 + x9) + 29*sin(x8 + x9) + 26*cos(x8) + 10))/100 + (cos(x5)*cos(x6)*cos(x7)*(9*cos(x8 + x9) + 29*sin(x8 + x9) + 26*cos(x8) + 10))/100

	# simplify pe1(2)=Tie(2,4)
	pe1_2 = x2 - (9*cos(x6)*sin(x4))/20 + (3*cos(x5)*sin(x6))/4 + (cos(x6)*sin(x4) - cos(x4)*sin(x5)*sin(x6))*((9*sin(x8 + x9))/100 - (29*cos(x8 + x9))/100 + (13*sin(x8))/50) + (9*cos(x4)*sin(x5)*sin(x6))/20 + (sin(x7)*(cos(x4)*cos(x6) + sin(x4)*sin(x5)*sin(x6))*(9*cos(x8 + x9) + 29*sin(x8 + x9) + 26*cos(x8) + 10))/100 + (cos(x5)*cos(x7)*sin(x6)*(9*cos(x8 + x9) + 29*sin(x8 + x9) + 26*cos(x8) + 10))/100

	# simplify pe1(2)=Tie(3,4)
	pe1_3 = x3 - (3*sin(x5))/4 + (9*cos(x4)*cos(x5))/20 - (cos(x7)*sin(x5)*(9*cos(x8 + x9) + 29*sin(x8 + x9) + 26*cos(x8) + 10))/100 - cos(x4)*cos(x5)*((9*sin(x8 + x9))/100 - (29*cos(x8 + x9))/100 + (13*sin(x8))/50) + (cos(x5)*sin(x4)*sin(x7)*(9*cos(x8 + x9) + 29*sin(x8 + x9) + 26*cos(x8) + 10))/100

#

	th_e = atan((-Tie31)/(sqrt(Tie11**2+Tie21**2)))   # th
	cr = cos(th_e)
	psi_e = atan((Tie21/cr)/(Tie11/cr))       # psi
	phi_e = atan((Tie32/cr)/(Tie33/cr))       # phi

	# e-e position error
	x_er = pe1_1 - xe_d       
	y_er = pe1_2 - ye_d
	z_er = pe1_3 - ze_d
	# e-e orientation error
	psi_er = psi_e - psi_ed    
	th_er = th_e - th_ed
	phi_er = phi_e - phi_ed

	hc = zeros((8,1),float)
	hc[0] = x4 
	hc[1] = x5
	hc[2] = x_er 
	hc[3] = y_er 
	hc[4] = z_er
	hc[5] = phi_er 
	hc[6] = th_er 
	hc[7] = psi_er

	#result = zeros((8,1),float)

	result[0] = hc[0]
	result[1] = hc[1]
	result[2] = hc[2]
	result[3] = hc[3]
	result[4] = hc[4]
	result[5] = hc[5]
	result[6] = hc[6]
	result[7] = hc[7]
	


    def myconstraintsINEQ(self,result, x, grad):

	#EE Desired States WRT World Frame
	xe_d = self.xe_d    
	ye_d = self.ye_d
	ze_d = self.ze_d
	psi_ed = self.psi_ed    
	th_ed = self.th_ed
	phi_ed = self.phi_ed

	if grad.size > 0:
           print 'error'
	   exit(0)

	
	x1 = x[0]
	x2 = x[1]
	x3 = x[2]
	x4 = x[3]
	x5 = x[4]
	x6 = x[5]
	x7 = x[6]
	x8 = x[7]
	x9 = x[8]
	x10 = x[9]
	x11 = x[10]
	x12 = x[11]

	
	

	ef = array([1.0, 0.0, 0.0])  # w
	ef = array(ef).reshape(3,1)
	em = array([1.0, 0.0, 0.0])  # 1-w
	em = array(em).reshape(3,1)

	#%% Desired Unit Wrench Vector (task space) %%


	Rid = array([cos(th_ed)*cos(psi_ed),  cos(psi_ed)*sin(th_ed)*sin(phi_ed)-sin(psi_ed)*cos(phi_ed),  sin(psi_ed)*sin(phi_ed)+cos(psi_ed)*cos(phi_ed)*sin(th_ed), cos(th_ed)*sin(psi_ed),  cos(psi_ed)*cos(phi_ed)+sin(phi_ed)*sin(th_ed)*sin(psi_ed),  sin(psi_ed)*sin(th_ed)*cos(phi_ed)-cos(psi_ed)*sin(phi_ed), -sin(th_ed), cos(th_ed)*sin(phi_ed), cos(th_ed)*cos(phi_ed)])

	
	Rid = array(Rid).reshape(3,3)

	fe_d = dot(Rid,ef)
	me_d = dot(Rid,em)


 	Jf = array([ cos(x5)*cos(x6), cos(x5)*sin(x6), -sin(x5), cos(x6)*sin(x4)*sin(x5) - cos(x4)*sin(x6), cos(x4)*cos(x6) + sin(x4)*sin(x5)*sin(x6), cos(x5)*sin(x4), sin(x4)*sin(x6) + cos(x4)*cos(x6)*sin(x5), cos(x4)*sin(x5)*sin(x6) - cos(x6)*sin(x4), cos(x4)*cos(x5), sin(x5)*(cos(x5)*sin(x6)*((cos(x7)*(9*cos(x8 + x9) + 29*sin(x8 + x9) + 26*cos(x8) + 10))/100 + 3/4) - (cos(x6)*sin(x4) - cos(x4)*sin(x5)*sin(x6))*((29*cos(x8 + x9))/100 - (9*sin(x8 + x9))/100 - (13*sin(x8))/50 + 9/20) + (sin(x7)*(cos(x4)*cos(x6) + sin(x4)*sin(x5)*sin(x6))*(9*cos(x8 + x9) + 29*sin(x8 + x9) + 26*cos(x8) + 10))/100) + cos(x5)*sin(x6)*(cos(x4)*cos(x5)*((29*cos(x8 + x9))/100 - (9*sin(x8 + x9))/100 - (13*sin(x8))/50 + 9/20) - sin(x5)*((cos(x7)*(9*cos(x8 + x9) + 29*sin(x8 + x9) + 26*cos(x8) + 10))/100 + 3/4) + (cos(x5)*sin(x4)*sin(x7)*(9*cos(x8 + x9) + 29*sin(x8 + x9) + 26*cos(x8) + 10))/100), - sin(x5)*((sin(x4)*sin(x6) + cos(x4)*cos(x6)*sin(x5))*((29*cos(x8 + x9))/100 - (9*sin(x8 + x9))/100 - (13*sin(x8))/50 + 9/20) + cos(x5)*cos(x6)*((cos(x7)*(9*cos(x8 + x9) + 29*sin(x8 + x9) + 26*cos(x8) + 10))/100 + 3/4) - (sin(x7)*(cos(x4)*sin(x6) - cos(x6)*sin(x4)*sin(x5))*(9*cos(x8 + x9) + 29*sin(x8 + x9) + 26*cos(x8) + 10))/100) - cos(x5)*cos(x6)*(cos(x4)*cos(x5)*((29*cos(x8 + x9))/100 - (9*sin(x8 + x9))/100 - (13*sin(x8))/50 + 9/20) - sin(x5)*((cos(x7)*(9*cos(x8 + x9) + 29*sin(x8 + x9) + 26*cos(x8) + 10))/100 + 3/4) + (cos(x5)*sin(x4)*sin(x7)*(9*cos(x8 + x9) + 29*sin(x8 + x9) + 26*cos(x8) + 10))/100), (cos(x5)*(10*cos(x4)*sin(x7) - 45*sin(x4) + 26*sin(x4)*sin(x8) + 26*cos(x4)*cos(x8)*sin(x7) - 29*cos(x8)*cos(x9)*sin(x4) + 9*cos(x8)*sin(x4)*sin(x9) + 9*cos(x9)*sin(x4)*sin(x8) + 29*sin(x4)*sin(x8)*sin(x9) + 9*cos(x4)*cos(x8)*cos(x9)*sin(x7) + 29*cos(x4)*cos(x8)*sin(x7)*sin(x9) + 29*cos(x4)*cos(x9)*sin(x7)*sin(x8) - 9*cos(x4)*sin(x7)*sin(x8)*sin(x9)))/100, (cos(x4)*cos(x6) + sin(x4)*sin(x5)*sin(x6))*(cos(x4)*cos(x5)*((29*cos(x8 + x9))/100 - (9*sin(x8 + x9))/100 - (13*sin(x8))/50 + 9/20) - sin(x5)*((cos(x7)*(9*cos(x8 + x9) + 29*sin(x8 + x9) + 26*cos(x8) + 10))/100 + 3/4) + (cos(x5)*sin(x4)*sin(x7)*(9*cos(x8 + x9) + 29*sin(x8 + x9) + 26*cos(x8) + 10))/100) - cos(x5)*sin(x4)*(cos(x5)*sin(x6)*((cos(x7)*(9*cos(x8 + x9) + 29*sin(x8 + x9) + 26*cos(x8) + 10))/100 + 3/4) - (cos(x6)*sin(x4) - cos(x4)*sin(x5)*sin(x6))*((29*cos(x8 + x9))/100 - (9*sin(x8 + x9))/100 - (13*sin(x8))/50 + 9/20) + (sin(x7)*(cos(x4)*cos(x6) + sin(x4)*sin(x5)*sin(x6))*(9*cos(x8 + x9) + 29*sin(x8 + x9) + 26*cos(x8) + 10))/100), (cos(x4)*sin(x6) - cos(x6)*sin(x4)*sin(x5))*(cos(x4)*cos(x5)*((29*cos(x8 + x9))/100 - (9*sin(x8 + x9))/100 - (13*sin(x8))/50 + 9/20) - sin(x5)*((cos(x7)*(9*cos(x8 + x9) + 29*sin(x8 + x9) + 26*cos(x8) + 10))/100 + 3/4) + (cos(x5)*sin(x4)*sin(x7)*(9*cos(x8 + x9) + 29*sin(x8 + x9) + 26*cos(x8) + 10))/100) + cos(x5)*sin(x4)*((sin(x4)*sin(x6) + cos(x4)*cos(x6)*sin(x5))*((29*cos(x8 + x9))/100 - (9*sin(x8 + x9))/100 - (13*sin(x8))/50 + 9/20) + cos(x5)*cos(x6)*((cos(x7)*(9*cos(x8 + x9) + 29*sin(x8 + x9) + 26*cos(x8) + 10))/100 + 3/4) - (sin(x7)*(cos(x4)*sin(x6) - cos(x6)*sin(x4)*sin(x5))*(9*cos(x8 + x9) + 29*sin(x8 + x9) + 26*cos(x8) + 10))/100), (13*sin(x5)*sin(x8))/50 - (3*cos(x4)*cos(x5))/4 - (9*sin(x5))/20 - (29*cos(x8)*cos(x9)*sin(x5))/100 + (9*cos(x8)*sin(x5)*sin(x9))/100 + (9*cos(x9)*sin(x5)*sin(x8))/100 + (29*sin(x5)*sin(x8)*sin(x9))/100 - (cos(x4)*cos(x5)*cos(x7))/10 - (13*cos(x4)*cos(x5)*cos(x7)*cos(x8))/50 - (9*cos(x4)*cos(x5)*cos(x7)*cos(x8)*cos(x9))/100 - (29*cos(x4)*cos(x5)*cos(x7)*cos(x8)*sin(x9))/100 - (29*cos(x4)*cos(x5)*cos(x7)*cos(x9)*sin(x8))/100 + (9*cos(x4)*cos(x5)*cos(x7)*sin(x8)*sin(x9))/100, - (cos(x6)*sin(x4) - cos(x4)*sin(x5)*sin(x6))*(cos(x4)*cos(x5)*((29*cos(x8 + x9))/100 - (9*sin(x8 + x9))/100 - (13*sin(x8))/50 + 9/20) - sin(x5)*((cos(x7)*(9*cos(x8 + x9) + 29*sin(x8 + x9) + 26*cos(x8) + 10))/100 + 3/4) + (cos(x5)*sin(x4)*sin(x7)*(9*cos(x8 + x9) + 29*sin(x8 + x9) + 26*cos(x8) + 10))/100) - cos(x4)*cos(x5)*(cos(x5)*sin(x6)*((cos(x7)*(9*cos(x8 + x9) + 29*sin(x8 + x9) + 26*cos(x8) + 10))/100 + 3/4) - (cos(x6)*sin(x4) - cos(x4)*sin(x5)*sin(x6))*((29*cos(x8 + x9))/100 - (9*sin(x8 + x9))/100 - (13*sin(x8))/50 + 9/20) + (sin(x7)*(cos(x4)*cos(x6) + sin(x4)*sin(x5)*sin(x6))*(9*cos(x8 + x9) + 29*sin(x8 + x9) + 26*cos(x8) + 10))/100), cos(x4)*cos(x5)*((sin(x4)*sin(x6) + cos(x4)*cos(x6)*sin(x5))*((29*cos(x8 + x9))/100 - (9*sin(x8 + x9))/100 - (13*sin(x8))/50 + 9/20) + cos(x5)*cos(x6)*((cos(x7)*(9*cos(x8 + x9) + 29*sin(x8 + x9) + 26*cos(x8) + 10))/100 + 3/4) - (sin(x7)*(cos(x4)*sin(x6) - cos(x6)*sin(x4)*sin(x5))*(9*cos(x8 + x9) + 29*sin(x8 + x9) + 26*cos(x8) + 10))/100) - (sin(x4)*sin(x6) + cos(x4)*cos(x6)*sin(x5))*(cos(x4)*cos(x5)*((29*cos(x8 + x9))/100 - (9*sin(x8 + x9))/100 - (13*sin(x8))/50 + 9/20) - sin(x5)*((cos(x7)*(9*cos(x8 + x9) + 29*sin(x8 + x9) + 26*cos(x8) + 10))/100 + 3/4) + (cos(x5)*sin(x4)*sin(x7)*(9*cos(x8 + x9) + 29*sin(x8 + x9) + 26*cos(x8) + 10))/100), (3*cos(x5)*sin(x4))/4 + (sin(x5)*sin(x7))/10 + (cos(x5)*cos(x7)*sin(x4))/10 + (13*cos(x8)*sin(x5)*sin(x7))/50 - (9*sin(x5)*sin(x7)*sin(x8)*sin(x9))/100 + (13*cos(x5)*cos(x7)*cos(x8)*sin(x4))/50 + (9*cos(x8)*cos(x9)*sin(x5)*sin(x7))/100 + (29*cos(x8)*sin(x5)*sin(x7)*sin(x9))/100 + (29*cos(x9)*sin(x5)*sin(x7)*sin(x8))/100 + (9*cos(x5)*cos(x7)*cos(x8)*cos(x9)*sin(x4))/100 + (29*cos(x5)*cos(x7)*cos(x8)*sin(x4)*sin(x9))/100 + (29*cos(x5)*cos(x7)*cos(x9)*sin(x4)*sin(x8))/100 - (9*cos(x5)*cos(x7)*sin(x4)*sin(x8)*sin(x9))/100, -((cos(x4)*cos(x7)*sin(x6) + cos(x5)*cos(x6)*sin(x7) - cos(x6)*cos(x7)*sin(x4)*sin(x5))*(9*cos(x8 + x9) + 29*sin(x8 + x9) + 26*cos(x8) + 10))/100, ((cos(x4)*cos(x6)*cos(x7) - cos(x5)*sin(x6)*sin(x7) + cos(x7)*sin(x4)*sin(x5)*sin(x6))*(9*cos(x8 + x9) + 29*sin(x8 + x9) + 26*cos(x8) + 10))/100, ((sin(x5)*sin(x7) + cos(x5)*cos(x7)*sin(x4))*(9*cos(x8 + x9) + 29*sin(x8 + x9) + 26*cos(x8) + 10))/100, (sin(x7)*(cos(x4)*sin(x6) - cos(x6)*sin(x4)*sin(x5))*(26*sin(x8) - 29*cos(x8 + x9) + 9*sin(x8 + x9)))/100 - (sin(x4)*sin(x6) + cos(x4)*cos(x6)*sin(x5))*((13*cos(x8))/50 + (9*cos(x8 + x9))/100 + (29*sin(x8 + x9))/100) - (cos(x5)*cos(x6)*cos(x7)*(26*sin(x8) - 29*cos(x8 + x9) + 9*sin(x8 + x9)))/100, (cos(x6)*sin(x4) - cos(x4)*sin(x5)*sin(x6))*((13*cos(x8))/50 + (9*cos(x8 + x9))/100 + (29*sin(x8 + x9))/100) - (sin(x7)*(cos(x4)*cos(x6) + sin(x4)*sin(x5)*sin(x6))*(26*sin(x8) - 29*cos(x8 + x9) + 9*sin(x8 + x9)))/100 - (cos(x5)*cos(x7)*sin(x6)*(26*sin(x8) - 29*cos(x8 + x9) + 9*sin(x8 + x9)))/100, (cos(x7)*sin(x5)*(26*sin(x8) - 29*cos(x8 + x9) + 9*sin(x8 + x9)))/100 - cos(x4)*cos(x5)*((13*cos(x8))/50 + (9*cos(x8 + x9))/100 + (29*sin(x8 + x9))/100) - (cos(x5)*sin(x4)*sin(x7)*(26*sin(x8) - 29*cos(x8 + x9) + 9*sin(x8 + x9)))/100, (cos(x5)*cos(x6)*cos(x7)*(29*cos(x8 + x9) - 9*sin(x8 + x9)))/100 - (sin(x7)*(29*cos(x8 + x9) - 9*sin(x8 + x9))*(cos(x4)*sin(x6) - cos(x6)*sin(x4)*sin(x5)))/100 - ((9*cos(x8 + x9))/100 + (29*sin(x8 + x9))/100)*(sin(x4)*sin(x6) + cos(x4)*cos(x6)*sin(x5)), ((9*cos(x8 + x9))/100 + (29*sin(x8 + x9))/100)*(cos(x6)*sin(x4) - cos(x4)*sin(x5)*sin(x6)) + (sin(x7)*(29*cos(x8 + x9) - 9*sin(x8 + x9))*(cos(x4)*cos(x6) + sin(x4)*sin(x5)*sin(x6)))/100 + (cos(x5)*cos(x7)*sin(x6)*(29*cos(x8 + x9) - 9*sin(x8 + x9)))/100, (cos(x5)*sin(x4)*sin(x7)*(29*cos(x8 + x9) - 9*sin(x8 + x9)))/100 - cos(x4)*cos(x5)*((9*cos(x8 + x9))/100 + (29*sin(x8 + x9))/100) - (cos(x7)*sin(x5)*(29*cos(x8 + x9) - 9*sin(x8 + x9)))/100, 0, 0, 0])

	

	Jf = array(Jf).reshape(10,3)
 
 	Jm = array([ 0, 0, 0, 0, 0, 0, 0, 0, 0, cos(x5)*cos(x6), cos(x5)*sin(x6), -sin(x5), cos(x6)*sin(x4)*sin(x5) - cos(x4)*sin(x6), cos(x4)*cos(x6) + sin(x4)*sin(x5)*sin(x6), cos(x5)*sin(x4), sin(x4)*sin(x6) + cos(x4)*cos(x6)*sin(x5), cos(x4)*sin(x5)*sin(x6) - cos(x6)*sin(x4), cos(x4)*cos(x5), sin(x4)*sin(x6) + cos(x4)*cos(x6)*sin(x5), cos(x4)*sin(x5)*sin(x6) - cos(x6)*sin(x4), cos(x4)*cos(x5), - cos(x7)*(cos(x4)*sin(x6) - cos(x6)*sin(x4)*sin(x5)) - cos(x5)*cos(x6)*sin(x7), cos(x7)*(cos(x4)*cos(x6) + sin(x4)*sin(x5)*sin(x6)) - cos(x5)*sin(x6)*sin(x7), sin(x5)*sin(x7) + cos(x5)*cos(x7)*sin(x4), - cos(x7)*(cos(x4)*sin(x6) - cos(x6)*sin(x4)*sin(x5)) - cos(x5)*cos(x6)*sin(x7), cos(x7)*(cos(x4)*cos(x6) + sin(x4)*sin(x5)*sin(x6)) - cos(x5)*sin(x6)*sin(x7), sin(x5)*sin(x7) + cos(x5)*cos(x7)*sin(x4), cos(x8 + x9)*(sin(x4)*sin(x6) + cos(x4)*cos(x6)*sin(x5)) - sin(x7)*sin(x8 + x9)*(cos(x4)*sin(x6) - cos(x6)*sin(x4)*sin(x5)) + cos(x5)*cos(x6)*cos(x7)*sin(x8 + x9), sin(x7)*sin(x8 + x9)*(cos(x4)*cos(x6) + sin(x4)*sin(x5)*sin(x6)) - cos(x8 + x9)*(cos(x6)*sin(x4) - cos(x4)*sin(x5)*sin(x6)) + cos(x5)*cos(x7)*sin(x6)*sin(x8 + x9), cos(x4)*cos(x5)*cos(x8 + x9) - cos(x7)*sin(x5)*sin(x8 + x9) + cos(x5)*sin(x4)*sin(x7)*sin(x8 + x9)])

	Jm = array(Jm).reshape(10,3)

	
	C1 = dot(Jf,fe_d) 
	C2 = dot(Jm,me_d)
	C = [C1,C2]
	C = array(C).reshape(10,2)
	

	#g = array([ -(38259*sin(x5))/1000, (38259*cos(x5)*sin(x4))/1000,(38259*cos(x4)*cos(x5))/1000, (981*cos(x5)*sin(x4)*((27*sin(x8 + x9))/4000 - (29*cos(x8 + x9))/4000 + (13*sin(x8))/400 + 19567/1000))/100 + (981*cos(x4)*cos(x5)*sin(x7)*(27*cos(x8 + x9) + 29*sin(x8 + x9) + 130*cos(x8) + 70))/400000, (981*sin(x5)*((27*sin(x8 + x9))/4000 - (29*cos(x8 + x9))/4000 + (13*sin(x8))/400 + 19567/1000))/100 - (981*cos(x4)*cos(x5)*((7*cos(x7))/400 + (13*cos(x7)*cos(x8))/400 + (29*cos(x7)*cos(x8)*sin(x9))/4000 + (29*cos(x7)*cos(x9)*sin(x8))/4000 - (27*cos(x7)*sin(x8)*sin(x9))/4000 + (27*cos(x7)*cos(x8)*cos(x9))/4000 + 3/20))/100, (981*sin(x5)*sin(x7)*(27*cos(x8 + x9) + 29*sin(x8 + x9) + 130*cos(x8) + 70))/400000 + (981*cos(x5)*sin(x4)*((7*cos(x7))/400 + (13*cos(x7)*cos(x8))/400 + (29*cos(x7)*cos(x8)*sin(x9))/4000 + (29*cos(x7)*cos(x9)*sin(x8))/4000 - (27*cos(x7)*sin(x8)*sin(x9))/4000 + (27*cos(x7)*cos(x8)*cos(x9))/4000 + 3/20))/100, (981*(sin(x5)*sin(x7) + cos(x5)*cos(x7)*sin(x4))*(27*cos(x8 + x9) + 29*sin(x8 + x9) + 130*cos(x8) + 70))/400000, (12753*cos(x7)*sin(x5)*sin(x8))/40000 - (12753*cos(x4)*cos(x5)*cos(x8))/40000 - (26487*cos(x4)*cos(x5)*cos(x8)*cos(x9))/400000 - (28449*cos(x4)*cos(x5)*cos(x8)*sin(x9))/400000 - (28449*cos(x4)*cos(x5)*cos(x9)*sin(x8))/400000 - (28449*cos(x7)*cos(x8)*cos(x9)*sin(x5))/400000 + (26487*cos(x4)*cos(x5)*sin(x8)*sin(x9))/400000 + (26487*cos(x7)*cos(x8)*sin(x5)*sin(x9))/400000 + (26487*cos(x7)*cos(x9)*sin(x5)*sin(x8))/400000 - (12753*cos(x5)*sin(x4)*sin(x7)*sin(x8))/40000 + (28449*cos(x7)*sin(x5)*sin(x8)*sin(x9))/400000 + (28449*cos(x5)*cos(x8)*cos(x9)*sin(x4)*sin(x7))/400000 - (26487*cos(x5)*cos(x8)*sin(x4)*sin(x7)*sin(x9))/400000 - (26487*cos(x5)*cos(x9)*sin(x4)*sin(x7)*sin(x8))/400000 - (28449*cos(x5)*sin(x4)*sin(x7)*sin(x8)*sin(x9))/400000, (26487*cos(x4)*cos(x5)*sin(x8)*sin(x9))/400000 - (28449*cos(x4)*cos(x5)*cos(x8)*sin(x9))/400000 - (28449*cos(x4)*cos(x5)*cos(x9)*sin(x8))/400000 - (28449*cos(x7)*cos(x8)*cos(x9)*sin(x5))/400000 - (26487*cos(x4)*cos(x5)*cos(x8)*cos(x9))/400000 + (26487*cos(x7)*cos(x8)*sin(x5)*sin(x9))/400000 + (26487*cos(x7)*cos(x9)*sin(x5)*sin(x8))/400000 + (28449*cos(x7)*sin(x5)*sin(x8)*sin(x9))/400000 + (28449*cos(x5)*cos(x8)*cos(x9)*sin(x4)*sin(x7))/400000 - (26487*cos(x5)*cos(x8)*sin(x4)*sin(x7)*sin(x9))/400000 - (26487*cos(x5)*cos(x9)*sin(x4)*sin(x7)*sin(x8))/400000 - (28449*cos(x5)*sin(x4)*sin(x7)*sin(x8)*sin(x9))/400000, 0])

	g = zeros((1,10),float)
	g = array(g).reshape(1,10) 

	#AUV thruster1 limits
	th1_min = -153
	th1_max = 137.7
	#AUV thruster2 limits  
	th2_min = -153 
	th2_max = 137.7
	#AUV thruster3 limits  
	th3_min = -153 
	th3_max = 137.7
	#AUV thruster4 limits  
	th4_min = -153 
	th4_max = 137.7
	#AUV thruster5 limits  
	th5_min = -153 
	th5_max = 137.7  

	#Thrusters distances
	dv = 0.559
	dh = 0.259

	# joint torque limits
	m1_min = -30
	m2_min = -30
	m3_min = -30 
	m4_min = -30    
	m1_max = 30  
	m2_max = 30  
	m3_max = 30 
	m4_max = 30 

	t_lb = array([ -(th1_max+th2_max) , th5_min , -(th3_max+th4_max) , -1e-6 , dv*(th3_min-th4_max) , dh*(th2_min-th1_max) , m1_min , m2_min , m3_min , m4_min])
	t_ub = array([ -(th1_min+th2_min) , th5_max , -(th3_min+th4_min) ,  1e-6 , dv*(th3_max-th4_min) , dh*(th2_max-th1_min) , m1_max , m2_max , m3_max , m4_max])

	t_lb = array(t_lb).reshape(10,1)
	t_ub = array(t_ub).reshape(10,1)	
	
	c1 = dot(C,array([x11, x12]).reshape(2,1)) + g.T - t_ub
	c2 = t_lb - dot(C,array([x11, x12]).reshape(2,1)) - g.T

	gc = zeros((20,1), float)

	gc[0:10] = c2
	gc[10:20] = c1    
	
	#result = zeros((20,1),float)
	
	result[0] = gc[0]
	result[1] = gc[1]
	result[2] = gc[2]
	result[3] = gc[3]
	result[4] = gc[4]
	result[5] = gc[5]
	result[6] = gc[6]
	result[7] = gc[7]
	result[8] = gc[8]
	result[9] = gc[9]
	result[10] = gc[10]
	result[11] = gc[11]
	result[12] = gc[12]
	result[13] = gc[13]
	result[14] = gc[14]
	result[15] = gc[15]
	result[16] = gc[16]
	result[17] = gc[17]
	result[18] = gc[18]
	result[19] = gc[19]


    def Optimizer(self):
       
	self.UVMSStates = [self.x, self.y, self.z, self.roll, self.pitch, self.yaw, self.jointstates[0], self.jointstates[1], self.jointstates[2], self.jointstates[3], self.Fuvms, self.Tuvms]

	opt = nlopt.opt(nlopt.LN_COBYLA, 12)
	#opt = nlopt.opt(nlopt.LN_BOBYQA, 12)
	
	lb = [ -2.5 , -3.5 , -1.5 , -pi/18 , -pi/18 , -pi/2 , 1e-10 , -0.1471 , -1.297 , -pi, -float('inf') , -float('inf') ]   # lower bounds of system states
	ub = [ 0 , 3.5 , 1 , pi/18 , pi/18 , pi/2 , 1.46 , 1.314 , 0.73 , pi , float('inf'), float('inf') ]   # upper bounds of system states
	
	#lb = [-10.0 ,-10.0 , 0.0 ,-pi/18 ,-pi/18 , -pi , 1e-10 , -0.1471 , -1.297 , -pi, -float('inf') , -float('inf') ]   # lower bounds of system states
	#ub = [ 10.0 , 10.0 , 5.0 , pi/18 , pi/18 ,  pi , 1.46 ,   1.314 ,    0.73 ,  pi , float('inf'), float('inf') ]     # upper bounds of system states	
	#lb = [-float('inf')]*12   # lower bounds of system states
	#ub = [float('inf')]*12   # upper bounds of system states
	#lb = [-180.0]*12   # lower bounds of system states
	#ub = [180.0]*12   # upper bounds of system states
        opt.set_lower_bounds(lb) #Set lower input bounds				
        opt.set_upper_bounds(ub) #Set upper input bounds

	
	opt.set_min_objective(self.objFun)
	#opt.set_max_objective(self.objFun)
	opt.add_inequality_mconstraint(lambda result,x,grad: self.myconstraintsINEQ(result,x,grad), [1e-8]*20) #Set inequality constraints
	opt.add_equality_mconstraint(lambda result,x,grad: self.myconstraintsEQ(result,x,grad), [1e-8]*8) #Set equality constraints

        opt.set_ftol_abs(1e-6)
        #opt.set_maxeval(maxeval)
	
	self.uvmsOpt = opt.optimize([-1.5015, 1.2254, -0.6519, 0, 0,   -1.5708,    1.2566,    0.4917,    0.7300,   -0.0000,  104.4228,    5.1812])
	#self.uvmsOpt = opt.optimize([ -1.2699, 1.1044, -0.6519, 0.0, 0.0, -1.5708, 1.1220, 0.4917,  0.7300, 0.0,  104.4228, 3.4958])
        minf = opt.last_optimum_value()
        print "optimum at ", self.uvmsOpt
        print "minimum value = ", minf
        print "result code = ", opt.last_optimize_result()



  
	
    def UVMSPlanningOpt (self,auvMSG, armMSG,t):

	#AUV Message
	#Define Desired SetPoints (5 WPs in 3D space)

	#Define Desired SetPoints
        x_des = self.uvmsOpt[0]
        y_des = self.uvmsOpt[1]
        z_des = self.uvmsOpt[2]
	#NOT Sending Roll, Pitch
	psi_des = self.uvmsOpt[5]

        auvMSG.header.stamp = rospy.Time.now()
    	auvMSG.header.frame_id = ""
	auvMSG.goal.priority = 0
	auvMSG.altitude_mode = False   

    	auvMSG.position.north =  x_des
	auvMSG.position.east =  y_des
	auvMSG.position.depth =  z_des
    	 
	auvMSG.altitude =  0.0

	auvMSG.orientation.roll = 0.0
	auvMSG.orientation.pitch = 0.0
	auvMSG.orientation.yaw = psi_des

	auvMSG.disable_axis.x = False
	auvMSG.disable_axis.y = False
	auvMSG.disable_axis.z = False
	auvMSG.disable_axis.roll = True
	auvMSG.disable_axis.pitch = True
	auvMSG.disable_axis.yaw = False

	auvMSG.position_tolerance.x = 0.0
	auvMSG.position_tolerance.y = 0.0
	auvMSG.position_tolerance.z = 0.0
	auvMSG.orientation_tolerance.roll = 0.0
	auvMSG.orientation_tolerance.pitch = 0.0
	auvMSG.orientation_tolerance.yaw = 0.0

	#Arm Message
	armMSG.header.stamp = rospy.Time.now()
    	armMSG.header.frame_id = ""
	
    	armMSG.position = [self.uvmsOpt[6], self.uvmsOpt[7], self.uvmsOpt[8], self.uvmsOpt[9]]
	armMSG.effort = [0.0, 0.0, 0.0, 0.0]

	e = 0.025	
		
	if abs(self.jointstates[0] - armMSG.position[0]) > e or abs(self.jointstates[1] - armMSG.position[1]) > e or abs(self.jointstates[2] - armMSG.position[2]) > e or abs(self.jointstates[3] - armMSG.position[3]) > e:
	 #print "current:", " ", "q1: %.2f" %  self.jointstates[0], "q2: %.2f " %  self.jointstates[1], "q3: %.2f "  %  self.jointstates[2], "q4: %.2f" %  self.jointstates[3], "q5: %.2f " %  self.jointstates[4], "\n"
	 #print "desired:", " ", "q1: %.2f" %  self.q1, "q2: %.2f "  % self.q2, "q3: %.2f " % self.q3, "q4: %.2f" % self.q4, "q5: %.2f " % self.q5, "\n"
	 self.pub_armMSG.publish(armMSG)

	
	self.pub_auvMSG.publish(auvMSG)


if __name__ == '__main__':
    try:
        rospy.init_node('UVMSOptimal')
        UVMSOptimalObj = UVMSOptimal(rospy.get_name())

	dt = 0.1

	armMSG = JointState()
	auvMSG = WorldWaypointReq()
	
	while not rospy.is_shutdown():
		t =  rospy.Time.now().to_sec() - UVMSOptimalObj.t0 
		t1 = rospy.Time.now().to_sec()
		print "Loop Works"
		UVMSOptimalObj.Optimizer()
		#UVMSOptimalObj.UVMSPlanningOpt(auvMSG,armMSG,t)
	
		rospy.sleep(dt)
		t2 = rospy.Time.now().to_sec()
		print "Elapsed:", t2-t1
        rospy.spin()

    except rospy.ROSInterruptException: pass
