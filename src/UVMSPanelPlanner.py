#!/usr/bin/env python

# ROS imports
import roslib 
roslib.load_manifest('cola2_control')
roslib.load_manifest('pose_ekf_slam')
import rospy

# Msgs imports
from auv_msgs.msg import *
from sensor_msgs.msg import JointState
from sensor_msgs.msg import Joy
from pose_ekf_slam.msg import Map
from geometry_msgs.msg import PoseStamped
from std_msgs.msg import Bool
from std_srvs.srv import Empty, EmptyResponse
import tf
# Python imports
from numpy import *
from numpy.linalg import *
from math import *
import nlopt

# Custom imports
import cola2_lib
import cola2_ros_lib
import sys

class UVMSPlanner:
    def __init__(self, name):
        self.name = name
	# Create publishers
	#Arm
        self.pub_jointCommands = rospy.Publisher("/csip_e5_arm/joint_state_command", JointState)
	#Vehicle	
	self.pub_msg = rospy.Publisher("/ntua_planner/world_waypoint_req", WorldWaypointReq)
	#Decision Making    
        self.pub_panelReach = rospy.Publisher("/ntua_decision_making/panel_reach", Bool)
        
	self.armReach = False
        self.vehicleReach = False

        # Create Subscribers
	#Arm Joints Position
        rospy.Subscriber("/csip_e5_arm/joint_state", JointState, self.updateJointState)
	#End Effector Pose wrt vehicle base frame
	rospy.Subscriber("/arm/pose_stamped", PoseStamped, self.updateArmPose)
	#Vehicle States
	rospy.Subscriber("/cola2_navigation/nav_sts", NavSts, self.updateNavSts)
	#Get Panel Pose as seen from bumblebee wrt world frame
        rospy.Subscriber("/pose_ekf_slam/map", Map, self.updateLandmarkPose)
	

	#Get Initial Time
        self.t0 = rospy.Time.now().to_sec()  
	
	#Initialize Arm States & Variables
	#Joint States
	self.jointstates = [0.0, 0.0, 0.0, 0.0, 0.0]

	#Desired Joint States 
	self.q1 = 0.0
	self.q2 = 0.0#1.16
	self.q3 = 0.0#0.4
	self.q4 = 0.0
	self.q5 = 0.0 #NOT USED

	#Initialize Vehicle States & Variables
	#Initilize Desired States
	self.x_des = 0.0
	self.y_des = 0.0
	self.z_des = 0.0
	self.theta_des = 0.0
	self.psi_des = 0.0

	#Initialize Panel Landmark States
	self.x_lan = 0.0
	self.y_lan = 0.0
	self.z_lan = 0.0
	self.psi_lan = 0.0

	#Declare Desired Offsets of the vehicle wrt to panel
	self.x_offset = -2.0
	self.y_offset = 0.0
	self.z_offset = 0.80
	self.psi_offset = 90.0*pi/180.0
 

        #Initilize States
	self.x = 0.
	self.y = 0.
	self.z = 0.
        self.roll = 0.
	self.pitch = 0.
	self.yaw = 0.
        self.u = 0.
        self.v = 0.
        self.w = 0.
        self.p = 0.
        self.q = 0.
        self.r = 0.
	self.t_nav = 0.
	
	#Initilalize Transformation States
	self.x_eewf = 0.
	self.y_eewf = 0.
	self.z_eewf = 0.
	self.roll_eewf = 0.
	self.pitch_eewf = 0.
	self.yaw_eewf = 0.

	self.ee2w = lambda : 0
        self.ee2w.rot = None
        self.ee2w.trans = None
        # TF listener to compute the transforms
        self.ee2w.tflistener = tf.TransformListener()

        self.enable = True
        

        #Create services
        self.enable_srv = rospy.Service('/ntua_control_g500/enable_UVMS_panel_planner', Empty, self.enableSrv)
        self.disable_srv = rospy.Service('/ntua_control_g500/disable_UVMS_panel_planner', Empty, self.disableSrv)

    def enableSrv(self, req):
        self.enable = True
        rospy.loginfo('%s Enabled', self.name)
        return EmptyResponse()


    def disableSrv(self, req):
        self.enable = False
        rospy.loginfo('%s Disabled', self.name)
        return EmptyResponse()


    def updateJointState(self, jointstates_msg):

        self.jointstates[0] = jointstates_msg.position[0]
	self.jointstates[1] = jointstates_msg.position[1]
	self.jointstates[2] = jointstates_msg.position[2]
	self.jointstates[3] = jointstates_msg.position[3]
	self.jointstates[4] = jointstates_msg.position[4] #NOT USED

    def updateNavSts(self, nav_sts):
	self.x = nav_sts.position.north
	self.y = nav_sts.position.east
	self.z = nav_sts.position.depth
        self.roll = nav_sts.orientation.roll
	self.pitch = nav_sts.orientation.pitch
	self.yaw = nav_sts.orientation.yaw
        self.u = nav_sts.body_velocity.x
        self.v = nav_sts.body_velocity.y
        self.w = nav_sts.body_velocity.z
        self.p = nav_sts.orientation_rate.roll
        self.q = nav_sts.orientation_rate.pitch
        self.r = nav_sts.orientation_rate.yaw
	self.t_nav = nav_sts.header.stamp.to_sec()

   #When Multiple Landmarks Detected
    def updateLandmarkPose(self, landm_msg):
        self.landmark_init = True
        num_lan = len(landm_msg.landmark)
        
        for i in range(0,num_lan):
          if landm_msg.landmark[i].landmark_id == '/pose_ekf_slam/landmark_update/panel_centre':
             self.x_lan = landm_msg.landmark[i].pose.pose.position.x
             self.y_lan = landm_msg.landmark[i].pose.pose.position.y
             self.z_lan = landm_msg.landmark[i].pose.pose.position.z
             qx = landm_msg.landmark[i].pose.pose.orientation.x
	     qy = landm_msg.landmark[i].pose.pose.orientation.y
	     qz = landm_msg.landmark[i].pose.pose.orientation.z
             qw = landm_msg.landmark[i].pose.pose.orientation.w	
             angles = tf.transformations.euler_from_quaternion([qx,qy,qz,qw])
	     self.psi_lan = angles[2] 
             self.seq_lan = landm_msg.header.seq

    def updateArmPose(self, armPose_msg):
	self.x_EF = armPose_msg.pose.position.x
	self.y_EF = armPose_msg.pose.position.y
	self.z_EF = armPose_msg.pose.position.z
	self.phi_EF = 0.0
	self.theta_EF = 0.0
	self.psi_EF = 0.0

    def getEEtoWorldPose(self):
	ee2w = self.ee2w
	try:
           ee2w.trans, ee2w.rot = ee2w.tflistener.lookupTransform("world", "end_effector", rospy.Time(0))
        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
            print "No End Effector to World transform available"
	else:
           self.x_eewf = ee2w.trans[0]
	   self.y_eewf = ee2w.trans[1]
	   self.z_eewf = ee2w.trans[2]

	   angles = tf.transformations.euler_from_quaternion(ee2w.rot)
	   self.roll_eewf = angles[0]
	   self.pitch_eewf = angles[1]
	   self.yaw_eewf = angles[2]
           print "Translation:", ee2w.trans, "Rotation:", angles

    def armPlanning (self,desJoints):

     
        desJoints.header.stamp = rospy.Time.now()
    	desJoints.header.frame_id = ""
	
    	desJoints.position = [self.q1, self.q2, self.q3, self.q4, self.q5]
	desJoints.effort = [0.0, 0.0, 0.0, 0.0, 0.0]

	e = 0.025	
		
	if abs(self.jointstates[0] - desJoints.position[0]) > e or abs(self.jointstates[1] - desJoints.position[1]) > e or abs(self.jointstates[2] - desJoints.position[2]) > e or abs(self.jointstates[3] - desJoints.position[3]) > e:
	 print "current:", " ", "q1: %.2f" %  self.jointstates[0], "q2: %.2f " %  self.jointstates[1], "q3: %.2f "  %  self.jointstates[2], "q4: %.2f" %  self.jointstates[3], "q5: %.2f " %  self.jointstates[4], "\n"
	 print "desired:", " ", "q1: %.2f" %  self.q1, "q2: %.2f "  % self.q2, "q3: %.2f " % self.q3, "q4: %.2f" % self.q4, "q5: %.2f " % self.q5, "\n"
	 self.pub_jointCommands.publish(desJoints)
         self.armReach = False
	 #self.pub_panelReach.publish(Bool(False))
        else:
         #self.pub_panelReach.publish(Bool(True))
         self.armReach = True

    def armPlanningObjF (self,q, grad):

	if grad.size > 0:
           print 'error (JCOST): There is no grad definition'
       	   exit(0)	     
	
	#Position of the Panel wrt World (Inertial) Frame
	x_d = self.x_lan
	y_d = self.y_lan
	z_d = self.z_lan
	
	#Current Vehicle Pose
	x_v = self.x
	y_v = self.y
	z_v = self.z
	phi_v = self.roll
	th_v = self.pitch
	psi_v = self.yaw

	#position of the manipulator base frame(0) relative to vehicle body-fixed frame(V)	
	x_v0 = 0.83#0.75 #0.53
	y_v0 = 0.047
	z_v0 = 0.41 #0.36
	#rpy-angles of the manipulator base frame(0) relative to vehicle body-fixed frame(V)     
	phi_v0 = 0. 
	th_v0 = 0. 
	psi_v0 = 0. 
     
	#DH Parameters (Arm)
	d1 = 0.              
	d2 = 0.               
	d3 = 0.              
	d4 = 0.29               

	a1 = 0.1
	a2 = 0.26
	a3 = 0.09
	a4 = 0.

	alpha1 = -pi/2
	alpha2 = 0. 
	alpha3 = pi/2
	alpha4 = 0.


	#Arm DH matrix

	DH = [q[0], d1 , a1 , alpha1 ,
       	      q[1], d2 , a2 , alpha2 ,
       	      q[2], d3 , a3 , alpha3 ,
              q[3], d4 , a4 , alpha4 ]

	DH = array(DH).reshape(4,4)

	# Transformation matrix Tiv from vehicle body-fixed frame(V) to inertial frame(I)

	Tiv = [ cos(psi_v)*cos(th_v) , -sin(psi_v)*cos(phi_v)+cos(psi_v)*sin(th_v)*sin(phi_v) ,  sin(psi_v)*sin(phi_v)+cos(psi_v)*sin(th_v)*cos(phi_v) , x_v ,
        	sin(psi_v)*cos(th_v) ,  cos(psi_v)*cos(phi_v)+sin(psi_v)*sin(th_v)*sin(phi_v) , -cos(psi_v)*sin(phi_v)+sin(psi_v)*sin(th_v)*cos(phi_v) , y_v ,
                  -sin(th_v) ,                                   cos(th_v)*sin(phi_v) ,                                   cos(th_v)*cos(phi_v) , z_v ,
                           0.,                                                      0.,                                                      0., 1.0 ]

	Tiv = array(Tiv).reshape(4,4)	


	# Transformation matrix Tv0 from manipulator base frame(0) to vehicle body-fixed frame(V) (constant matrix)
	Tv0 = [cos(psi_v0)*cos(th_v0),-sin(psi_v0)*cos(phi_v0)+cos(psi_v0)*sin(th_v0)*sin(phi_v0),sin(psi_v0)*sin(phi_v0)+cos(psi_v0)*sin(th_v0)*cos(phi_v0),x_v0,
               sin(psi_v0)*cos(th_v0),cos(psi_v0)*cos(phi_v0)+sin(psi_v0)*sin(th_v0)*sin(phi_v0),-cos(psi_v0)*sin(phi_v0)+sin(psi_v0)*sin(th_v0)*cos(phi_v0),y_v0,
                   -sin(th_v0) ,                                      cos(th_v0)*sin(phi_v0) ,                                      cos(th_v0)*cos(phi_v0),  z_v0,
                             0.,                                                           0.,                                               0.           ,  1.0 ]

	Tv0 = array(Tv0).reshape(4,4)               
	
	#Transformation matrix Tmi from manipulator frame (ii) to manipulator frame(ii-1)

    
	Tm0 = [cos(DH[0,0]), -sin(DH[0,0])*cos(DH[0,3]),  sin(DH[0,0])*sin(DH[0,3]), DH[0,2]*cos(DH[0,0]) ,
               sin(DH[0,0]),  cos(DH[0,0])*cos(DH[0,3]), -cos(DH[0,0])*sin(DH[0,3]), DH[0,2]*sin(DH[0,0]) ,
               0            ,  sin(DH[0,3])              ,  cos(DH[0,3])              , DH[0,1]           ,    
               0            ,  0                          ,  0                          , 1               ]
          
	Tm1 = [cos(DH[1,0]), -sin(DH[1,0])*cos(DH[1,3]),  sin(DH[1,0])*sin(DH[1,3]), DH[1,2]*cos(DH[1,0]) ,
               sin(DH[1,0]),  cos(DH[1,0])*cos(DH[1,3]), -cos(DH[1,0])*sin(DH[1,3]), DH[1,2]*sin(DH[1,0]) ,
               0            ,  sin(DH[1,3])              ,  cos(DH[1,3])              , DH[1,1]           ,
               0            ,  0                          ,  0                          , 1               ]

	Tm2 = [cos(DH[2,0]), -sin(DH[2,0])*cos(DH[2,3]),  sin(DH[2,0])*sin(DH[2,3]), DH[2,2]*cos(DH[2,0]) ,
               sin(DH[2,0]),  cos(DH[2,0])*cos(DH[2,3]), -cos(DH[2,0])*sin(DH[2,3]), DH[2,2]*sin(DH[2,0]) ,
               0            ,  sin(DH[2,3])              ,  cos(DH[2,3])              , DH[2,1]           ,
               0            ,  0                          ,  0                          , 1               ]

	Tm3 = [cos(DH[3,0]), -sin(DH[3,0])*cos(DH[3,3]),  sin(DH[3,0])*sin(DH[3,3]), DH[3,2]*cos(DH[3,0]) ,
               sin(DH[3,0]),  cos(DH[3,0])*cos(DH[3,3]), -cos(DH[3,0])*sin(DH[3,3]), DH[3,2]*sin(DH[3,0]) ,
               0            ,  sin(DH[3,3])              ,  cos(DH[3,3])              , DH[3,1]           ,   
               0            ,  0                          ,  0                          , 1               ]

	Tm0 = array(Tm0).reshape(4,4)
	Tm1 = array(Tm1).reshape(4,4)
	Tm2 = array(Tm2).reshape(4,4)
	Tm3 = array(Tm3).reshape(4,4)
          
	          

	# Transformation matrix T4e from e-e frame(e) to manipulator frame(4) (Constant Matrix)

	T4e = [cos(-pi/2)  , 0 , sin(-pi/2) , 0 ,
                        0  , 1 ,     0      , 0 ,
               -sin(-pi/2) , 0 , cos(-pi/2) , 0 ,
                        0  , 0 ,     0      , 1 ]

	T4e = array(T4e).reshape(4,4)

	# UVMS Transformation matrix Tie from e-e frame(e) to inertial frame(I)

	#Tie = Tiv*Tv0*Tm0*Tm1*Tm2*Tm3*T4e
	Tie = dot(dot(dot(dot(dot(dot(Tiv,Tv0),Tm0),Tm1),Tm2),Tm3),T4e)


        n = Tie[0:3,0]

        p_e = Tie[0:3,3]

	
        p_d = [x_d, y_d, z_d]
	
	

        norm_p = sqrt(abs(p_d[0]-p_e[0])**2 + abs(p_d[1]-p_e[1])**2 + abs(p_d[2]-p_e[2])**2)

        zzz = ( dot(n.transpose(),(p_d-p_e)/norm_p) -1)**2

	
	return zzz

    def armPlanningOpt(self):
       
	opt = nlopt.opt(nlopt.LN_COBYLA, 4)
	aa = 1.0
        bb = 1.0
	lb = [ -0.52*aa , -0.1471*aa , -1.297*aa , -pi*aa ];   # lower bounds of q
	ub = [ 1.46*bb , 1.314*bb , 0.73*bb , pi*bb ];   # upper bounds of q
        opt.set_lower_bounds(lb) #Set lower input bounds				
        opt.set_upper_bounds(ub) #Set upper input bounds

	opt.set_min_objective(self.armPlanningObjF)
        opt.set_ftol_abs(1e-6)
        #opt.set_maxeval(maxeval)
	
	q_opt = opt.optimize(self.jointstates[0:4])
        minf = opt.last_optimum_value()
        print "optimum at ", q_opt[0], q_opt[1], q_opt[2], q_opt[3]
        #print "minimum value = ", minf
        print "result code = ", opt.last_optimize_result()

	self.q1 = q_opt[0]
	self.q2 = q_opt[1]
	self.q3 = q_opt[2]
        self.q4 = 0.0 #q_opt[3] The optimization turns this DoF up-side down

    def vehiclePlanning (self,data,t):


	#Define Desired SetPoints (5 WPs in 3D space)
	 
	#Define Desired SetPoints
        x_des = self.x_lan +2.3*cos(self.psi_lan+1.57)
        y_des = self.y_lan  + 2.3*sin(self.psi_lan+1.57)
        z_des = self.z_lan - self.z_offset
	#psi_des = self.psi_lan  - self.psi_offset
        psi_des = atan2(self.y_lan-self.y,self.x_lan-self.x)
        theta_des = self.theta_des

	#Define Desired SetPoints
        """x_des = self.x_lan - self.x_offset
        y_des = self.y_lan - self.y_offset
        z_des = self.z_lan - self.z_offset
	psi_des = self.psi_lan  - self.psi_offset
        theta_des = self.theta_des"""

        data.header.stamp = rospy.Time.now()
    	data.header.frame_id = ""
	data.goal.priority = 0
	data.altitude_mode = False   

    	data.position.north =  x_des
	data.position.east =  y_des
	data.position.depth =  z_des
    	 
	data.altitude =  0.0

	data.orientation.roll = 0.0
	data.orientation.pitch = theta_des
	data.orientation.yaw = psi_des

	data.disable_axis.x = False
	data.disable_axis.y = False
	data.disable_axis.z = False
	data.disable_axis.roll = True
	data.disable_axis.pitch = True
	data.disable_axis.yaw = False

	data.position_tolerance.x = 0.0
	data.position_tolerance.y = 0.0
	data.position_tolerance.z = 0.0
	data.orientation_tolerance.roll = 0.0
	data.orientation_tolerance.pitch = 0.0
	data.orientation_tolerance.yaw = 0.0

        ex = self.x - x_des
        ey = self.y - y_des
        ez = self.z - z_des
        epsi = self.yaw - psi_des

        criteria = 0.3
        #print sqrt(ex**2+ey**2+ez**2), epsi

        if sqrt(ex**2+ey**2+ez**2) < criteria and epsi < 6.28+0.034:
           self.vehicleReach = True
        else:
           self.vehicleReach = False

	#print "THE PANEL IS:","XLAN:", self.x_lan, "YLAN:", self.y_lan,"ZLAN:",self.z_lan, "PSI_LAN:",self.psi_lan   
             
	print "THE ROBOT IS:","\n"
	print "current:", " ", "North: %.2f" % self.x, "East: %.2f " % self.y, "Depth: %.2f " % self.z, "Pitch: %.2f" % self.pitch, "Yaw: %.2f " % self.yaw, "\n"
	#print "current:", " ", "North:", self.x, " ",  "East:", " ", self.y, " ", "Depth:", self.z, " ", "Pitch:", self.pitch, " ","Yaw:", self.yaw, "\n"
	#print "THE PLANNER SAYS GO TO:","\n"	
	#print "desired:", " ", "North:", x_des, " ",  "East:", " ", y_des, " ", "Depth:", z_des, " ", "Pitch:", theta_des, " ","Yaw:", psi_des, "\n"
	print "desired:", " ", "North: %.2f" % x_des, "East: %.2f " % y_des, "Depth: %.2f " % z_des, "Pitch: %.2f" % theta_des, "Yaw: %.2f " % psi_des, "\n"
	self.pub_msg.publish(data)


if __name__ == '__main__':
    try:
        rospy.init_node('UVMSPanelPlanner')
	rospy.loginfo('UVMSPanelPlanner Node is up')
        UVMSPlannerObj = UVMSPlanner(rospy.get_name())

	dt = 0.1

	joint_msg = JointState()
	WP_msg = WorldWaypointReq()
	
	while not rospy.is_shutdown():
		t =  rospy.Time.now().to_sec() - UVMSPlannerObj.t0 
		t1 = rospy.Time.now().to_sec()
                if UVMSPlannerObj.enable == True:
		   #UVMSPlannerObj.getEEtoWorldPose()
	           UVMSPlannerObj.armPlanningOpt()
		   UVMSPlannerObj.armPlanning (joint_msg )
		   UVMSPlannerObj.vehiclePlanning (WP_msg, t)
	           print "Vehicle Status:", UVMSPlannerObj.vehicleReach, "Arm Status:",  UVMSPlannerObj.armReach
                   if UVMSPlannerObj.armReach == True and UVMSPlannerObj.vehicleReach == True:
                      UVMSPlannerObj.pub_panelReach.publish(Bool(True))
                   else:
	              UVMSPlannerObj.pub_panelReach.publish(Bool(False))
		   rospy.sleep(dt)
		t2 = rospy.Time.now().to_sec()
		print "Elapsed:", t2-t1
        rospy.spin()

    except rospy.ROSInterruptException: pass
