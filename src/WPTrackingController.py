#!/usr/bin/env python

# ROS imports
import roslib 
roslib.load_manifest('cola2_control')
import rospy

# Msgs imports
from auv_msgs.msg import *
from nav_msgs.msg import Odometry
from std_srvs.srv import Empty, EmptyResponse, EmptyRequest
from std_msgs.msg import Float32

# Python imports
from numpy import *
from numpy.linalg import *
from math import *

# Custom imports
import cola2_lib
import cola2_ros_lib



class G500WPTrackingController:
    def __init__(self, name):
        """ Controls the pose of G500 AUV  """
        self.name = name
	self.enable = True
        self.power_limit = 0.2
	#Get Initial Time
        self.t0 = rospy.Time.now().to_sec()  
	
        #Initilize States
	self.x = 0.
	self.y = 0.
	self.z = 0.
        self.roll = 0.
	self.pitch = 0.
	self.yaw = 0.
        self.u = 0.
        self.v = 0.
        self.w = 0.
        self.p = 0.
        self.q = 0.
        self.r = 0.
	self.t_nav = 0.
       
	#Initilize Desired Values for Wps
	self.x_des = 0.0
	self.y_des = 0.0
	self.z_des = 1.0
	self.psi_des = 0.0
        self.theta_des = 0.0

        # Create publisher
        self.pub_tau = rospy.Publisher("/cola2_control/body_force_req", BodyForceReq)
        
        # Create Subscriber
        rospy.Subscriber("/cola2_navigation/nav_sts", NavSts, self.updateNavSts)
	rospy.Subscriber("/ntua_planner/world_waypoint_req",WorldWaypointReq, self.updateWPs) 

        #Create services
	self.enable_srv = rospy.Service('/ntua_controlGIRONA500/enable_wptrackingcontroller', Empty, self.enableSrv)
        self.disable_srv = rospy.Service('/ntua_controlGIRONA500/disable_wptrackingcontroller', Empty, self.disableSrv)     

    def enableSrv(self, req):
        self.enable = True
        rospy.loginfo('%s Enabled', self.name)
        return EmptyResponse()
    
        
    def disableSrv(self, req):
        self.enable = False
        rospy.loginfo('%s Disabled', self.name)
        return EmptyResponse()


    def updateNavSts(self, nav_sts):
	self.x = nav_sts.position.north
	self.y = nav_sts.position.east
	self.z = nav_sts.position.depth
        self.roll = nav_sts.orientation.roll
	self.pitch = nav_sts.orientation.pitch
	self.yaw = nav_sts.orientation.yaw
        self.u = nav_sts.body_velocity.x
        self.v = nav_sts.body_velocity.y
        self.w = nav_sts.body_velocity.z
        self.p = nav_sts.orientation_rate.roll
        self.q = nav_sts.orientation_rate.pitch
        self.r = nav_sts.orientation_rate.yaw
	self.t_nav = nav_sts.header.stamp.to_sec()

    def updateWPs(self, wp_msg):
	self.x_des = wp_msg.position.north
	self.y_des = wp_msg.position.east
	self.z_des = wp_msg.position.depth
	self.psi_des = wp_msg.orientation.yaw
        self.theta_des = wp_msg.orientation.pitch
	self.t_wp = wp_msg.header.stamp.to_sec()


    def controlScheme(self,t):

   	#Filling up states
	x = self.x
	y = self.y
	z = self.z
	phi = self.roll
	theta = self.pitch
	psi = self.yaw
	u = self.u
	v = self.v
	w = self.w
	p = self.p
	q = self.q
	r = self.r

	#Calculating Jacobians
	J1 = [cos(theta)*cos(psi),  cos(psi)*sin(theta)*sin(phi)-sin(psi)*cos(phi),  sin(psi)*sin(phi)+cos(psi)*cos(phi)*sin(theta),
      	      cos(theta)*sin(psi),  cos(psi)*cos(phi)+sin(phi)*sin(theta)*sin(psi),  sin(psi)*sin(theta)*cos(phi)-cos(psi)*sin(phi),
              -sin(theta),          cos(theta)*sin(phi),                             cos(theta)*cos(phi)]

	J2 = [1.0,  sin(phi)*tan(theta),  cos(phi)*tan(theta),
              0.0,  cos(phi),             -sin(phi),
              0.0,  sin(phi)/cos(theta),  cos(phi)/cos(theta)]

	J1 = array(J1).reshape(3,3)
	J2 = array(J2).reshape(3,3)

	J1_INV = inv(J1)
	J2_INV = inv(J2)

	print "t:", t

	#Define Desired SetPoints (5 WPs in 3D space)
	#As read from the WorldWaypointReq Msg
	x_des = self.x_des
	y_des = self.y_des
	z_des = self.z_des
	psi_des = self.psi_des
	theta_des = self.theta_des

	#psi_des = 0.#0.3
	dx_des = 0.
	dy_des = 0.
	dz_des = 0.
	dtheta_des = 0.
	dpsi_des = 0.
	d2x_des = 0.
	d2y_des = 0.
	d2z_des = 0.
	d2theta_des = 0.
	d2psi_des = 0.

	#Calculating errors
	e_x = x-x_des
	e_y = y-y_des
	e_z = z-z_des
	e_theta = theta-theta_des
	e_psi = psi-psi_des	

	tempT = dot(J1_INV,array([[e_x], [e_y], [e_z]]))
        tempR = dot(J2_INV[1:3,1:3],array([[e_theta], [e_psi]]))

	#G500 Dynamic model parameters !!!Must Be Edited For G500!!!
	Mu = 126.5000
	Mv = 273.8433
	Mw = 213.0535
	Mp = 55.0000
	Mq = 67.8951
	Mr = 67.8951
	zB = -0.02
	W  = 539.5500
	B  = 546.1106
	Xuu = -31.8086
        Yvv = -222.8960
        Zww = -263.4225
        Kpp = -0.0000
        Mqq = -40.5265
        Nrr = -40.5265
	Xu = -2.0000
        Yv = -23.0000
        Zw = -0.0000
        Kp = -10.0000
        Mq = -0.0000
        Nr = -0.0000


	k_u=200.0
        k_x=100.0
	k_v=200.0
        k_y=110.0
	k_w=220.0
        k_z=170.0
	k_q=200.0
        k_theta=150.0
	k_r=200.0
        k_psi=100.0
	nu=0.
	nv=0.
	nw=0.
	nq=0.
	nr=0.	


	"""k_u=1.5
        k_x=0.6
	k_v=1.5
        k_y=0.9
	k_w=1.75
        k_z=0.8
	k_q=2.0
        k_theta=2.0
	k_r=4.0
        k_psi=2.5
	nu=0.
	nv=0.
	nw=0.
	nq=0.
	nr=0."""	
	
	#Model Based Robust Pose Controller

        X_model = Mw*w*q-Mv*v*r+(W-B)*sin(theta)-Xu*u-Xuu*u*abs(u)
	Y_model = -Mw*w*p+Mu*u*r-(W-B)*cos(theta)*sin(phi)-Yv*v-Yvv*v*abs(v)
	Z_model = Mv*v*p-Mu*u*q-(W-B)*cos(theta)*cos(phi)-Zw*w-Zww*w*abs(w)
	M_model = -Mw*w*u+Mu*u*w-Mr*r*p+Mp*p*r-zB*B*sin(theta)-Mq*q-Mqq*q*abs(q)
	N_model = Mv*v*u-Mu*u*v+Mq*q*p-Mp*p*q-Nr*r-Nrr*r*abs(r)

	#Model Based Robust Pose Controller
        #X = -k_u*u-k_x*tempT[0] + X_model
	#Y = -k_v*v-k_y*tempT[1] + Y_model 
        #Z = -k_w*w-k_z*tempT[2] + Z_model + 0.05*cos(theta)*cos(phi)
        #M = -k_q*q-k_theta*tempR[0] + M_model - zB*B*sin(theta)*0.05
        #N = -k_r*r-k_psi*tempR[1] + N_model

	#Model Free Robust Pose Controller
	X = -k_u*u-k_x*tempT[0]
	Y = -k_v*v-k_y*tempT[1]
        Z = -k_w*w-k_z*tempT[2] 
        #M = -k_q*q-k_theta*tempR[0]-4.4*sin(theta_des)*100.0
        M = 0.0
	N = -k_r*r-k_psi*tempR[1]

	"""X = -k_u*u-k_x*e_x
	Y = -k_v*v-k_y*e_y
        Z = -k_w*w-k_z*e_z 
	#Z = 0.0
        #M = -k_q*q-k_theta*tempR[0]-zB*B*sin(theta)*0.05
        M = 0.0
	N = -k_r*r-k_psi*e_psi"""


        #X=0.0
        #Y=0.0
        #Z = 0.0
        #M=0.0
        #N=0.0
	
        print "e_x:", str(e_x)," ", "e_y:", str(e_y)," ",  "e_z:", str(e_z)," ","e_theta:",str(e_theta*180.0/pi)," ",  "e_psi:", str(e_psi*180.0/pi)	

	#Saturate Control Signals (Body Forces)
	if X > 100.0:
	   X = 100.0
	elif X < -100.0:
             X = -100.0	

	if Y > 100.0:
	   Y = 100.0
	elif Y < -100.0:
             Y = -100.0	

	if Z > 100.0:
	   Z = 100.0
	elif X < -100.0:
             X = -100.0	

	if M > 100.0:
	   M = 100.0
	elif M < -100.0:
             M = -100.0

	if N > 100.0:
	   N = 100.0
	elif N < -100.0:
             N = -100.0		


	#Scale Control Signals (Body Forces)
	"""X = X/100.00
	Y = Y/100.00
	Z = Z/100.00
	M = M/100.00
	N = N/100.00"""
	
	U = array([X, Y, Z, M, N])

	return U
         
    def iterate(self): 
        
        # Main loop
	t = rospy.Time.now().to_sec() - self.t0  
        
        print "current state:", str(self.x), str(self.y), str(self.z), str(self.pitch), str(self.yaw)
	
	if self.enable == False:
           print "WP Controller is down", "\n"
               
	else:
	   print "WP Controller is up: VEHICLE IN CLOSED LOOP MODE", "\n"
		
	   U = self.controlScheme(t)
            
           data = BodyForceReq()
                
           data.header.stamp = rospy.Time.now()
           data.header.frame_id = "vehicle_frame"
           data.goal.requester = "wp tracking controller"
           data.goal.id = 0
           data.goal.priority = GoalDescriptor.PRIORITY_NORMAL
           data.wrench.force.x = U[0]*self.power_limit
           data.wrench.force.y = U[1]*self.power_limit
           data.wrench.force.z = U[2]*self.power_limit+4.4        
           data.wrench.torque.x = 0.
           data.wrench.torque.y = U[3]*self.power_limit
           data.wrench.torque.z = U[4]*self.power_limit
            
           data.disable_axis.x = False
           data.disable_axis.y = False
           data.disable_axis.z = False
           data.disable_axis.roll = True
           data.disable_axis.pitch = True
           data.disable_axis.yaw = False
            
           self.pub_tau.publish(data)


if __name__ == '__main__':
    try:
        rospy.init_node('G500WPTrackingController')
        controlObject = G500WPTrackingController(rospy.get_name())
	
	while not rospy.is_shutdown():
		rospy.sleep(0.05)
		controlObject.iterate()
		rospy.sleep(0.05)

        rospy.spin()

    except rospy.ROSInterruptException: pass
