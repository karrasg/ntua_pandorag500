#!/usr/bin/env python

# ROS imports
import roslib 
roslib.load_manifest('cola2_control')
import rospy

# Msgs imports
from sensor_msgs.msg import JointState
from sensor_msgs.msg import Joy
from geometry_msgs.msg import PoseStamped

#Service Import
from cola2_control.srv import ComputeInverseKinematics

# Python imports
from PyKDL import ChainJntToJacSolver, Chain, Frame, Joint, Segment, JntArray, Jacobian
from numpy import *
from numpy.linalg import *

# Custom imports
import cola2_lib
import cola2_ros_lib
import sys

class armEFJacControllerNTUA:
    def __init__(self, name):
        
	# Create publishers
	self.pub_jointCommands = rospy.Publisher("/csip_e5_arm/joint_voltage_command", JointState)
        
        # Create Subscribers
        rospy.Subscriber("/csip_e5_arm/joint_state", JointState, self.updateJointState)
	
        rospy.Subscriber("/csip_e5_arm/joint_state_coef", JointState, self.updateJointCoef)
        
	rospy.Subscriber("/arm/pose_stamped", PoseStamped, self.updateArmPose)
	#Get Initial Time
        self.t0 = rospy.Time.now().to_sec()  
	
	#Initialize 
	#Joint States
	self.jointstates = [0.0, 0.0, 0.0, 0.0, 0.0]
	#Previous Joint States
	self.jointstates_prev = [0.0, 0.0, 0.0, 0.0, 0.0]

	#Coefficients for Motors
        self.jointcoef = [0.0, 0.0, 0.0]

	#Desired End Effector Pose States (given in m/s and deg/s -> transformed into rad/s)
	self.x_dEF = (float (sys.argv[1]))
	self.y_dEF = (float (sys.argv[2]))
	self.z_dEF = (float (sys.argv[3]))
	self.phi_dEF = (float (sys.argv[4]))*pi/180.0
	self.theta_dEF = (float (sys.argv[5]))*pi/180.0
	self.psi_dEF = (float (sys.argv[6]))*pi/180.0

	#Initialitation of the EF pose
	self.x_EF = 0.0
	self.y_EF = 0.0
	self.z_EF = 0.0
	self.phi_EF = 0.0
	self.theta_EF = 0.0
	self.psi_EF = 0.0
      
	#Control Gain
	self.kp = 100.0

        #Dt
	self.dt = 0.2

	#Initialize KDL Variables --> Jacobian Calculation
        self.jointAngles =JntArray(4)
        self.jacobian = Jacobian(4)
        self.useJacobian = zeros([3,3], float)

    def initArmDH(self):
     
        DH_a1 = 0.1
        DH_a2 = 0.26
        DH_a3 = 0.09
        DH_d4 = 0.29
        DH_alpha1 = -1.57
        DH_alpha3 = 1.57     

        self.chain=Chain()

        joint0 = Joint(Joint.RotZ) 
        frame0 = Frame().DH(DH_a1, DH_alpha1, 0, 0)
        segment0 = Segment(joint0,frame0)
        self.chain.addSegment(segment0) 
 

        joint1 = Joint(Joint.RotZ)
        frame1 = Frame().DH(DH_a2, 0, 0, 0 )
        segment1 = Segment(joint1,frame1)
        self.chain.addSegment(segment1)
 
        joint2 = Joint(Joint.RotZ)
        frame2 = Frame().DH(DH_a3, DH_alpha3, 0, 0)
        segment2 = Segment(joint2,frame2)
        self.chain.addSegment(segment2)
         
        joint3 = Joint(Joint.RotZ)
        frame3 = Frame().DH(0, 0, DH_d4, 0)
        segment3 = Segment(joint3,frame3)
        self.chain.addSegment(segment3)

    def calcJacobian(self): 
     
        self.jointAngles[0] = self.jointstates[0]
        self.jointAngles[1] = self.jointstates[1]
        self.jointAngles[2] = self.jointstates[2]
        self.jointAngles[3] = self.jointstates[3]
        
        solver = ChainJntToJacSolver(self.chain)
        solver.JntToJac(self.jointAngles,self.jacobian)
       

	#Take only the 3x3 part of the 6x3 Jacobian
        self.useJacobian[0,0] = self.jacobian[0,0]
        self.useJacobian[0,1] = self.jacobian[0,1]
        self.useJacobian[0,2] = self.jacobian[0,2]
        self.useJacobian[1,0] = self.jacobian[1,0]
        self.useJacobian[1,1] = self.jacobian[1,1]
        self.useJacobian[1,2] = self.jacobian[1,2]
        self.useJacobian[2,0] = self.jacobian[2,0]
        self.useJacobian[2,1] = self.jacobian[2,1]
        self.useJacobian[2,2] = self.jacobian[2,2]
      
        
    def updateJointState(self, jointstates_msg):

        self.jointstates[0] = jointstates_msg.position[0]
	self.jointstates[1] = jointstates_msg.position[1]
	self.jointstates[2] = jointstates_msg.position[2]
	self.jointstates[3] = jointstates_msg.position[3]
	self.jointstates[4] = jointstates_msg.position[4]

    def updateJointCoef(self, jointcoef_msg):

        self.jointcoef[0] = jointcoef_msg.position[0]
	self.jointcoef[1] = jointcoef_msg.position[1]
	self.jointcoef[2] = jointcoef_msg.position[2]
	
    def updateArmPose(self, armPose_msg):
	self.x_EF = armPose_msg.pose.position.x
	self.y_EF = armPose_msg.pose.position.y
	self.z_EF = armPose_msg.pose.position.z
	self.phi_EF = 0.0
	self.theta_EF = 0.0
	self.psi_EF = 0.0
    
    def control(self, desJoints):
	
	# End Effector Velocity Controller
        invJac = inv(self.useJacobian)
        pos_d = [self.x_dEF, self.y_dEF, self.z_dEF]
	pos = [self.x_EF, self.y_EF,self.z_EF]
	vel = [-self.kp*(pos[0]-pos_d[0]),-self.kp*(pos[1]-pos_d[1]),-self.kp*(pos[2]-pos_d[2])]
        velvec = array(vel).reshape(3,1)
        qvelctrl = dot(invJac,velvec.flatten())        
   
        desJoints.header.stamp = rospy.Time.now()
    	desJoints.header.frame_id = ""
    	
        velCmdX = int(self.jointcoef[0]*qvelctrl[0]*65535)
        velCmdY = int(self.jointcoef[1]*qvelctrl[1]*65535)
        velCmdZ = int(self.jointcoef[2]*qvelctrl[2]*65535)	

	#velCmdX = int(qvelctrl[0]*65535)
        #velCmdY = int(qvelctrl[1]*65535)
        #velCmdZ = int(qvelctrl[2]*65535)

        desJoints.position = [  velCmdX,  velCmdY,  velCmdZ,   0,  0]
        desJoints.effort = [0.0, 0.0, 0.0, 0.0, 0.0]
        
        #desJoints.position = [  -0.01*invJac[0,2],   -0.01*invJac[1,2],   -0.01*invJac[2,2],   0,  0]
  	print desJoints.position
	self.pub_jointCommands.publish(desJoints)
      
        self.jointstates_prev[0] = self.jointstates[0]
        self.jointstates_prev[1] = self.jointstates[1]
        self.jointstates_prev[2] = self.jointstates[2]
        self.jointstates_prev[3] = self.jointstates[3]
        self.jointstates_prev[4] = self.jointstates[4]
      
if __name__ == '__main__':
    try:
        rospy.init_node('armEFJacControllerNTUA')

        armEFControllerObj = armEFJacControllerNTUA(rospy.get_name())

        armEFControllerObj.initArmDH()  

	joint_msg = JointState()
	
	while not rospy.is_shutdown():
                
                
                armEFControllerObj.calcJacobian()
		
                rospy.sleep(armEFControllerObj.dt/2.0)
		armEFControllerObj.control (joint_msg)
		rospy.sleep(armEFControllerObj.dt/2.0)
                 
                #print "EF VELOCITY CONTROL -->", "vx:", armEFControllerObj.vxEF, "vy:", armEFControllerObj.vyEF, "vz:", armEFControllerObj.vzEF, 
             
        rospy.spin()

    except rospy.ROSInterruptException: pass
