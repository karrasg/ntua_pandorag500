#!/usr/bin/env python

# ROS imports
import roslib 
roslib.load_manifest('cola2_control')
import rospy

# Msgs imports
from sensor_msgs.msg import JointState
from sensor_msgs.msg import Joy

# Python imports
from numpy import *
from numpy.linalg import *
from math import *

# Custom imports
import cola2_lib
import cola2_ros_lib
import sys

class armJointControllerNTUA:
    def __init__(self, name):
        
	# Create publishers
	self.pub_jointCommands = rospy.Publisher("/csip_e5_arm/joint_voltage_command", JointState)
        
        # Create Subscribers
        rospy.Subscriber("/csip_e5_arm/joint_state", JointState, self.updateJointState)
	
	#Get Initial Time
        self.t0 = rospy.Time.now().to_sec()  
	
	#Initialize 
	#Joint States
	self.jointstates = [0.0, 0.0, 0.0, 0.0, 0.0]
	#Previous Joint States
	self.jointstates_prev = [0.0, 0.0, 0.0, 0.0, 0.0]

	#Desired Joint States (given in deg and transformed into rad)
	self.q0d = (float (sys.argv[1]))*pi/180.0
	self.q1d = (float (sys.argv[2]))*pi/180.0
	self.q2d = (float (sys.argv[3]))*pi/180.0
	self.q3d = (float (sys.argv[4]))*pi/180.0
	self.q4d = (float (sys.argv[5]))*pi/180.0

	#PD Gains
	self.kq = 5.0
        self.kdq = 0.2
       
        #Dt
	self.dt = 0.2

        
    def updateJointState(self, jointstates_msg):

        self.jointstates[0] = jointstates_msg.position[0]
	self.jointstates[1] = jointstates_msg.position[1]
	self.jointstates[2] = jointstates_msg.position[2]
	self.jointstates[3] = jointstates_msg.position[3]
	self.jointstates[4] = jointstates_msg.position[4]


    def control (self,desJoints):

        q0dot_des = -self.kq*(self.jointstates[0] - self.q0d) - 0*self.kdq*(self.jointstates[0] - self.jointstates_prev[0])/self.dt
        q1dot_des = -1.5*self.kq*(self.jointstates[1] - self.q1d) - 0*self.kdq*(self.jointstates[1] - self.jointstates_prev[1])/self.dt
        q2dot_des = -self.kq*(self.jointstates[2] - self.q2d) - 0*self.kdq*(self.jointstates[2] - self.jointstates_prev[2])/self.dt
        q3dot_des = -0.08*(self.jointstates[3] - self.q3d) - 0.20*(self.jointstates[3] - self.jointstates_prev[3])/self.dt
        q4dot_des = 0.0
 
	if q0dot_des > 1.0:
	 q0dot_des = 1.0
        elif q0dot_des < -1.0:
         q0dot_des = -1.0

        if q1dot_des > 1.0:
	 q1dot_des = 1.0
        elif q1dot_des < -1.0:
         q1dot_des = -1.0

        if q2dot_des > 1.0:
	 q2dot_des = 1.0
        elif q2dot_des < -1.0:
         q2dot_des = -1.0
     
        if q3dot_des > 0.5:
	 q3dot_des = 0.5
        elif q3dot_des < -0.5:
         q3dot_des = -0.5
   
        if q4dot_des > 1.0:
	 q4dot_des = 1.0
        elif q4dot_des < -1.0:
         q4dot_des = -1.0

        q0cmd = int(q0dot_des*65535)
        q1cmd = int(q1dot_des*65535)
        q2cmd = int(q2dot_des*65535)
	q3cmd = int(q3dot_des*65535)
	q4cmd = int(q4dot_des*65535)


        if q3cmd < 3125 and q3cmd > 0:
           q3cmd = 3125
        elif q3cmd > -3250 and q3cmd < 0:
           q3cmd = -3250
   
        if abs(self.jointstates[3] - self.q3d) < 0.02:
           q3cmd = 0
   
        desJoints.header.stamp = rospy.Time.now()
    	desJoints.header.frame_id = ""
    	
	
	desJoints.position = [ q0cmd,  q1cmd,  q2cmd,  q3cmd,  0]
        #desJoints.position = [0, q1cmd,  0,  0,  0]
        desJoints.effort = [0.0, 0.0, 0.0, 0.0, 0.0]
         
	self.pub_jointCommands.publish(desJoints)
      
        self.jointstates_prev[0] = self.jointstates[0]
        self.jointstates_prev[1] = self.jointstates[1]
        self.jointstates_prev[2] = self.jointstates[2]
        self.jointstates_prev[3] = self.jointstates[3]
        self.jointstates_prev[4] = self.jointstates[4]
      
        print "current:", " ", "q0: %.2f" %  self.jointstates[0], "q1: %.2f " %  self.jointstates[1], "q2: %.2f "  %  self.jointstates[2], "q3: %.2f" %  self.jointstates[3], "q4: %.2f " %  self.jointstates[4], "\n"
        print "desired:", " ", "q0: %.2f" %  self.q0d, "q1: %.2f "  % self.q1d, "q2: %.2f " % self.q2d, "q3: %.2f" % self.q3d, "q4: %.2f " % self.q4d, "\n"
	


if __name__ == '__main__':
    try:
        rospy.init_node('armJointControllerNTUA')
        armControllerObj = armJointControllerNTUA(rospy.get_name())

	joint_msg = JointState()
	
	for i in range (0,10):
	  armControllerObj.jointstates_prev[0] = armControllerObj.jointstates[0]
          armControllerObj.jointstates_prev[1] = armControllerObj.jointstates[1]
          armControllerObj.jointstates_prev[2] = armControllerObj.jointstates[2]
          armControllerObj.jointstates_prev[3] = armControllerObj.jointstates[3]
          armControllerObj.jointstates_prev[4] = armControllerObj.jointstates[4]

	while not rospy.is_shutdown():
                rospy.sleep(armControllerObj.dt/2.0)
		armControllerObj.control (joint_msg)
		rospy.sleep(armControllerObj.dt/2.0)
        rospy.spin()

    except rospy.ROSInterruptException: pass
