#!/usr/bin/env python

# ROS imports
import roslib 
roslib.load_manifest('cola2_control')
import rospy

# Msgs imports
from sensor_msgs.msg import JointState
from sensor_msgs.msg import Joy

# Python imports
from numpy import *
from numpy.linalg import *
from math import *

# Custom imports
import cola2_lib
import cola2_ros_lib
import sys

class armJointPlanner:
    def __init__(self, name):
        
	# Create publishers
        #self.pub_jointCommands = rospy.Publisher("/csip_e5_arm/joint_state_command", JointState)
	self.pub_jointCommands = rospy.Publisher("/uwsim/joint_state_command", JointState)
    
        # Create Subscribers
        #rospy.Subscriber("/csip_e5_arm/joint_state", JointState, self.updateJointState)
	rospy.Subscriber("/uwsim/joint_state", JointState, self.updateJointState)
	
	#Get Initial Time
        self.t0 = rospy.Time.now().to_sec()  
	
	#Initialize 
	#Joint States
	self.jointstates = [0.0, 0.0, 0.0, 0.0, 0.0]

	#Desired Joint States (given in deg and transformed into rad)
	self.q1 = (float (sys.argv[1]))#*pi/180.0
	self.q2 = (float (sys.argv[2]))#*pi/180.0
	self.q3 = (float (sys.argv[3]))#*pi/180.0
	self.q4 = (float (sys.argv[4]))#*pi/180.0
	self.q5 = (float (sys.argv[5]))#*pi/180.0
	


    def updateJointState(self, jointstates_msg):

        self.jointstates[0] = jointstates_msg.position[0]
	self.jointstates[1] = jointstates_msg.position[1]
	self.jointstates[2] = jointstates_msg.position[2]
	self.jointstates[3] = jointstates_msg.position[3]
	self.jointstates[4] = jointstates_msg.position[4]


    def planning (self,desJoints):

     
        desJoints.header.stamp = rospy.Time.now()
    	desJoints.header.frame_id = ""
	
    	desJoints.position = [self.q1, self.q2, self.q3, self.q4, self.q5]
	desJoints.effort = [0.0, 0.0, 0.0, 0.0, 0.0]

	e = 0.025	
		
	if abs(self.jointstates[0] - desJoints.position[0]) > e or abs(self.jointstates[1] - desJoints.position[1]) > e or abs(self.jointstates[2] - desJoints.position[2]) > e or abs(self.jointstates[3] - desJoints.position[3]) > e:
	 print "current:", " ", "q1: %.2f" %  self.jointstates[0], "q2: %.2f " %  self.jointstates[1], "q3: %.2f "  %  self.jointstates[2], "q4: %.2f" %  self.jointstates[3], "q5: %.2f " %  self.jointstates[4], "\n"
	 print "desired:", " ", "q1: %.2f" %  self.q1, "q2: %.2f "  % self.q2, "q3: %.2f " % self.q3, "q4: %.2f" % self.q4, "q5: %.2f " % self.q5, "\n"
	 self.pub_jointCommands.publish(desJoints)


if __name__ == '__main__':
    try:
        rospy.init_node('armJointPlanner')
        armJointObject = armJointPlanner(rospy.get_name())

	joint_msg = JointState()
	
	while not rospy.is_shutdown():
		armJointObject.planning (joint_msg )
		rospy.sleep(0.1)

        rospy.spin()

    except rospy.ROSInterruptException: pass
