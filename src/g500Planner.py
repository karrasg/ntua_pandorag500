#!/usr/bin/env python

# ROS imports
import roslib 
import rospy
roslib.load_manifest('auv_msgs')
# Msgs imports
from auv_msgs.msg import *
from geometry_msgs.msg import WrenchStamped
from nav_msgs.msg import Odometry
from std_srvs.srv import Empty, EmptyResponse, EmptyRequest
from std_msgs.msg import Float32

# Python imports
from numpy import *
import sys

class g500Planner:
    def __init__(self, name):
       
        self.name = name
        #Get Initial Time
	self.t0 = rospy.Time.now().to_sec()
	#Initialize WP counter
	self.counter = 0.0 

	#Initilize States
	self.x_des = float (sys.argv[1])
	self.y_des = float (sys.argv[2])
	self.z_des = float (sys.argv[3])
	self.theta_des = (float (sys.argv[4]))*pi/180.0
	self.psi_des = (float (sys.argv[5]))*pi/180.0
        

	#Create Publisher
	self.pub_msg = rospy.Publisher("/planner/world_waypoint_req", WorldWaypointReq)
	
	
        
    def planning (self,data,t):


	#Define Desired SetPoints (5 WPs in 3D space)
	
	x_des = self.x_des
	y_des = self.y_des
	z_des = self.z_des

	psi_des = self.psi_des
	theta_des = self.theta_des

        data.header.stamp = rospy.Time.now()
    	data.header.frame_id = ""
	data.goal.priority = 0
	data.altitude_mode = False   

    	data.position.north =  x_des
	data.position.east =  y_des
	data.position.depth =  z_des
    	 
	data.altitude =  0.0

	data.orientation.roll = 0.0
	data.orientation.pitch = theta_des
	data.orientation.yaw = psi_des

	data.disable_axis.x = False
	data.disable_axis.y = False
	data.disable_axis.z = False
	data.disable_axis.roll = True
	data.disable_axis.pitch = False
	data.disable_axis.yaw = False

	data.position_tolerance.x = 0.0
	data.position_tolerance.y = 0.0
	data.position_tolerance.z = 0.0
	data.orientation_tolerance.roll = 0.0
	data.orientation_tolerance.pitch = 0.0
	data.orientation_tolerance.yaw = 0.0

	print "time:", t, " ", "x_d:", x_des, " ",  "y_d:", " ", y_des, " ", "z_d:", z_des, " ", "theta_d:", theta_des, " ","psi_d:", psi_des, "\n"
	
	self.pub_msg.publish(data)
              
        
	
if __name__ == '__main__':
    try:
        rospy.init_node('g500Planner')
	rospy.loginfo('g500Planner Node is up')
        dt = 0.1
	
        g500PlannerObj = g500Planner(rospy.get_name())
        WP_msg = WorldWaypointReq()
	
	while not rospy.is_shutdown():
              t =  rospy.Time.now().to_sec() - g500PlannerObj.t0 
	      g500PlannerObj.planning (WP_msg, t)
              rospy.sleep(dt)
	rospy.spin()
    except rospy.ROSInterruptException: pass 
