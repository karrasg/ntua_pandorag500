#!/usr/bin/env python

# ROS imports
import roslib 
import rospy
roslib.load_manifest('ntua_pandora')
# Msgs imports
from auv_msgs.msg import *
from geometry_msgs.msg import WrenchStamped
from sensor_msgs.msg import JointState
from nav_msgs.msg import Odometry
from std_srvs.srv import Empty, EmptyResponse, EmptyRequest
from std_msgs.msg import Float32

# Python imports
from numpy import *
import sys
import tf
class g500PosePlanner:
    def __init__(self, name):
       
        self.name = name
        #Get Initial Time
	self.t0 = rospy.Time.now().to_sec()
	#Initialize WP counter
	self.counter = 0.0 

	#Initilize Desired States
	self.x_des = float (sys.argv[1])
	self.y_des = float (sys.argv[2])
	self.z_des = float (sys.argv[3])
	self.phi_des = float (sys.argv[4])
	self.theta_des = float (sys.argv[5])
	self.psi_des = float (sys.argv[6])
	self.q0_des = float (sys.argv[7])
	self.q1_des = float (sys.argv[8])
	self.q2_des = float (sys.argv[9])
	self.q3_des = float (sys.argv[10])


        #Initilize AUV States
	self.x = 0.
	self.y = 0.
	self.z = 0.
        self.roll = 0.
	self.pitch = 0.
	self.yaw = 0.
        self.u = 0.
        self.v = 0.
        self.w = 0.
        self.p = 0.
        self.q = 0.
        self.r = 0.
	self.t_nav = 0.
	
	#Initialize Joint States
	self.jointstates = [0.0, 0.0, 0.0, 0.0, 0.0]


	#Transformation Panel (Target Frame) --> World Frame (Reference Frame)
	#Initilalize Transformation States
	self.x_pawf = 0.
	self.y_pawf = 0.
	self.z_pawf = 0.
	self.roll_pawf = 0.
	self.pitch_pawf = 0.
	self.yaw_pawf = 0.

	self.pa2w = lambda : 0
        self.pa2w.rot = None
        self.pa2w.trans = None
        # TF listener to compute the transforms
        self.pa2w.tflistener = tf.TransformListener()
        

	#Create AUV Publisher
	self.pub_msg = rospy.Publisher("/ntua_planner/world_waypoint_req", WorldWaypointReq)
	# Create Arm publisher
        #self.pub_jointCommands = rospy.Publisher("/csip_e5_arm/joint_state_command", JointState)
	self.pub_jointCommands = rospy.Publisher("/uwsim/joint_state_command", JointState)
        # Create AUV Subscriber
        rospy.Subscriber("/cola2_navigation/nav_sts", NavSts, self.updateNavSts)
	# Create Arm Subscriber
        #rospy.Subscriber("/csip_e5_arm/joint_state", JointState, self.updateJointState)
	rospy.Subscriber("/uwsim/joint_state", JointState, self.updateJointState)

    def updateJointState(self, jointstates_msg):

        self.jointstates[0] = jointstates_msg.position[0]
	self.jointstates[1] = jointstates_msg.position[1]
	self.jointstates[2] = jointstates_msg.position[2]
	self.jointstates[3] = jointstates_msg.position[3]
	self.jointstates[4] = jointstates_msg.position[4]

    def updateNavSts(self, nav_sts):
	self.x = nav_sts.position.north
	self.y = nav_sts.position.east
	self.z = nav_sts.position.depth
        self.roll = nav_sts.orientation.roll
	self.pitch = nav_sts.orientation.pitch
	self.yaw = nav_sts.orientation.yaw
        self.u = nav_sts.body_velocity.x
        self.v = nav_sts.body_velocity.y
        self.w = nav_sts.body_velocity.z
        self.p = nav_sts.orientation_rate.roll
        self.q = nav_sts.orientation_rate.pitch
        self.r = nav_sts.orientation_rate.yaw
	self.t_nav = nav_sts.header.stamp.to_sec()

    def getPaneltoWorldPose(self):
	pa2w = self.pa2w
	try:
           pa2w.trans, pa2w.rot = pa2w.tflistener.lookupTransform("world", "panel_centre", rospy.Time(0))
        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
            print "No Panel to World transform available"
            exit(0)
	else:
           self.x_pawf = pa2w.trans[0]
	   self.y_pawf = pa2w.trans[1]
	   self.z_pawf = pa2w.trans[2]

	   angles = tf.transformations.euler_from_quaternion(pa2w.rot)
	   self.roll_pawf = angles[0]
	   self.pitch_pawf = angles[1]
	   self.yaw_pawf = angles[2]
           print "Translation:", pa2w.trans, "Rotation:", angles
	
	
        
    def AUVplanning (self,data,t):


	#Define Desired SetPoints (5 WPs in 3D space)
	
	x_des = self.x_des
	y_des = self.y_des
	z_des = self.z_des

	psi_des = self.psi_des
	theta_des = self.theta_des

        data.header.stamp = rospy.Time.now()
    	data.header.frame_id = ""
	data.goal.priority = 0
	data.altitude_mode = False   

    	data.position.north =  x_des
	data.position.east =  y_des
	data.position.depth =  z_des
    	 
	data.altitude =  0.0

	data.orientation.roll = 0.0
	data.orientation.pitch = theta_des
	data.orientation.yaw = psi_des

	data.disable_axis.x = False
	data.disable_axis.y = False
	data.disable_axis.z = False
	data.disable_axis.roll = True
	data.disable_axis.pitch = False
	data.disable_axis.yaw = False

	data.position_tolerance.x = 0.0
	data.position_tolerance.y = 0.0
	data.position_tolerance.z = 0.0
	data.orientation_tolerance.roll = 0.0
	data.orientation_tolerance.pitch = 0.0
	data.orientation_tolerance.yaw = 0.0

	print "current:", " ", "North: %.2f" % self.x, "East: %.2f " % self.y, "Depth: %.2f " % self.z, "Pitch: %.2f" % self.pitch, "Yaw: %.2f " % self.yaw, "\n"
	#print "current:", " ", "North:", self.x, " ",  "East:", " ", self.y, " ", "Depth:", self.z, " ", "Pitch:", self.pitch, " ","Yaw:", self.yaw, "\n"
	#print "desired:", " ", "North:", x_des, " ",  "East:", " ", y_des, " ", "Depth:", z_des, " ", "Pitch:", theta_des, " ","Yaw:", psi_des, "\n"
	print "desired:", " ", "North: %.2f" % x_des, "East: %.2f " % y_des, "Depth: %.2f " % z_des, "Pitch: %.2f" % theta_des, "Yaw: %.2f " % psi_des, "\n"
	self.pub_msg.publish(data)
              

    def Armplanning (self,desJoints):

     
        desJoints.header.stamp = rospy.Time.now()
    	desJoints.header.frame_id = ""
	
    	desJoints.position = [self.q0_des, self.q1_des, self.q2_des, self.q3_des, 0.0]
	desJoints.effort = [0.0, 0.0, 0.0, 0.0, 0.0]

	e = 0.025	
		
	if abs(self.jointstates[0] - desJoints.position[0]) > e or abs(self.jointstates[1] - desJoints.position[1]) > e or abs(self.jointstates[2] - desJoints.position[2]) > e or abs(self.jointstates[3] - desJoints.position[3]) > e:
	 print "current:", " ", "q1: %.2f" %  self.jointstates[0], "q2: %.2f " %  self.jointstates[1], "q3: %.2f "  %  self.jointstates[2], "q4: %.2f" %  self.jointstates[3], "q5: %.2f " %  self.jointstates[4], "\n"
	 print "desired:", " ", "q1: %.2f" %  self.q0_des, "q2: %.2f "  % self.q1_des, "q3: %.2f " % self.q2_des, "q4: %.2f" % self.q3_des,  "\n"
	 self.pub_jointCommands.publish(desJoints)

        
	
if __name__ == '__main__':
    try:
        rospy.init_node('g500PosePlannerNTUA')
	rospy.loginfo('g500 Pose Planner Node is up')
        dt = 0.1
	
        g500PlannerObj = g500PosePlanner(rospy.get_name())
        WP_msg = WorldWaypointReq()
	joint_msg = JointState()
	while not rospy.is_shutdown():
              t =  rospy.Time.now().to_sec() - g500PlannerObj.t0 
	      g500PlannerObj.AUVplanning (WP_msg, t)
	      g500PlannerObj.Armplanning (joint_msg)
              rospy.sleep(dt)
	rospy.spin()
    except rospy.ROSInterruptException: pass 
