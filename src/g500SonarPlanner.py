#!/usr/bin/env python

# ROS imports
import roslib 
import rospy
roslib.load_manifest('ntua_pandora')
# Msgs imports
from auv_msgs.msg import *
from geometry_msgs.msg import WrenchStamped
from nav_msgs.msg import Odometry
from std_srvs.srv import Empty, EmptyResponse, EmptyRequest
from std_msgs.msg import Float32, Bool
from visualization_msgs.msg import Marker, MarkerArray

import tf

# Python imports
import numpy as np
import sys
import matplotlib.pyplot as plt

import threading
import math as math1
from scipy.optimize import fsolve

class g500SonarPlanner:
    def __init__(self, name):
       
        self.name = name
        #Get Initial Time
	self.t0 = rospy.Time.now().to_sec()
	#Initialize WP counter
	self.counter = 0

	#Initilize Desired States
	self.x_des = 0.0
	self.y_des = 0.0
	self.z_des = 1.0
	self.theta_des = 0.0
	self.psi_des = 0.0


        #Initilize States
	self.x = 0.
	self.y = 0.
	self.z = 0.
        self.roll = 0.
	self.pitch = 0.
	self.yaw = 0.
        self.u = 0.
        self.v = 0.
        self.w = 0.
        self.p = 0.
        self.q = 0.
        self.r = 0.
	self.t_nav = 0.

	#Initialize Sonar Variables
	self.numberWPS = 1
	self.WPS = np.empty((self.numberWPS, 6), dtype=float) 
	self.final_points = np.zeros((self.numberWPS, 4))
	#self.final_points = np.array([self.x_des, self.y_des, self.z_des,self.psi_des])
	self.msg_id = -10
        self.param = [0.0, 0.0, 0.0, 0.0]
        self.iter_WPs = 0
	self.xi = 0.0
	self.yi = 0.0
	self.xWP = 0.0
	self.yWP = 0.0
	self.sonar_range = 2.0
	self.class_threshold = 0.15
	self.error_threshold = 0.15
	self.lock = threading.Lock()


        self.circle_start = False
        self.circle_stop = False

        self.counter_start = 0
        self.counter_stop = 0

	#Create Publisher
	self.pub_msg = rospy.Publisher("/ntua_planner/world_waypoint_req", WorldWaypointReq)
	self.pub_sonar_wps = rospy.Publisher("/ntua_planner/sonar_waypoints", MarkerArray)
        #Decision Making
        #self.pub_start = rospy.Publisher("/sonar_decision_making/start", Bool)    
        self.pub_start_circle = rospy.Publisher("/ntua_decision_making/start_circle", Bool)
        #self.pub_stop = rospy.Publisher("/sonar_decision_making/stop", Bool)   

        # Create Subscriber
        rospy.Subscriber("/cola2_navigation/nav_sts", NavSts, self.updateNavSts)
        rospy.Subscriber("/ntua_decision_making/stop_cirle", Bool, self.decisionStopCircle)
	#Sonar Data Subscriber
	rospy.Subscriber("/link_pose", MarkerArray, self.updateSonarData)

    def decisionStopCircle(self, msg):
        self.circle_stop = msg.data

    def updateNavSts(self, nav_sts):
	self.x = nav_sts.position.north
	self.y = nav_sts.position.east
	self.z = nav_sts.position.depth
        self.roll = nav_sts.orientation.roll
	self.pitch = nav_sts.orientation.pitch
	self.yaw = nav_sts.orientation.yaw
        self.u = nav_sts.body_velocity.x
        self.v = nav_sts.body_velocity.y
        self.w = nav_sts.body_velocity.z
        self.p = nav_sts.orientation_rate.roll
        self.q = nav_sts.orientation_rate.pitch
        self.r = nav_sts.orientation_rate.yaw
	self.t_nav = nav_sts.header.stamp.to_sec()

    def updateSonarData(self, sonarPoints):
        self.lock.acquire()
        try:
		self.numberWPS = len(sonarPoints.markers)
        	#print "WPS Number:", self.numberWPS
        	self.WPS = np.empty((self.numberWPS, 6), dtype=float)          
               
        	for i in range (0,self.numberWPS):
            		self.WPS[i][0] = sonarPoints.markers[i].pose.position.y
	    		self.WPS[i][1] = sonarPoints.markers[i].pose.position.x
	    		self.WPS[i][2] = sonarPoints.markers[i].pose.position.z
		
	    		qx =  sonarPoints.markers[i].pose.orientation.x
            		qy =  sonarPoints.markers[i].pose.orientation.y
	    		qz =  sonarPoints.markers[i].pose.orientation.z
            		qw =  sonarPoints.markers[i].pose.orientation.w
            		RPY = tf.transformations.euler_from_quaternion([qx, qy, qz, qw]) 

       	    		self.WPS[i][3] = RPY[0]
	    		self.WPS[i][4] = RPY[1]
	    		self.WPS[i][5] = RPY[2]
	finally:
            self.lock.release()
	#print "WPS:", self.WPS, "\n"
		
	
    def calcChain(self):
              
	self.lock.acquire()
	try:
		x = np.array(self.WPS[:,0])
		y1 = np.array(self.WPS[:,1])
		psi = np.array(self.WPS[:,5])
	finally:
            self.lock.release()


	z1 = pow(x,3)
	z2 = pow(x,2)
	z3 = x
	z4 = np.ones((len(x),1),float)

	
	z1 = np.array(z1).reshape(len(z1),1)
	z2 = np.array(z2).reshape(len(z2),1)
	z3 = np.array(z3).reshape(len(z3),1)
	z4 = np.array(z4).reshape(len(z4),1)
	
	

	z11 = z1
	z22 = z2
	z33 = z3
	z44 = z4


	z = np.array([z11, z22, z33, z44])
	
	z = np.array(z).reshape(4,len(z11))
	
	y1d = np.tan(psi)
	
	y1 =  np.array(y1).reshape(len(y1),1)
	y1d = np.array(y1d).reshape(len(y1d),1)

	
	y = y1
	
	self.param = np.dot(np.dot(np.linalg.pinv(np.dot(z,z.T)),z),y)

	x_sol = np.zeros((len(x),1),float)
	y_sol = np.zeros((len(y),1),float)
	z_sol = np.array(self.WPS[:,2])
	y_sort = np.zeros((len(y),1),float)
	z_sort = np.zeros((len(y),1),float)

	"""for j in range (0,len(x)):
	    xi = x[i]
	    yi = y[i]

	    xs = Symbol('xs')
	    x_sol[j] = nsolve(((xs-xi)+(p[0]*(xs**3.0)+p[1]*(xs**2.0)+p[2]*xs+p[3]-yi)*(3*p[0]*(xs**2.0)+2*p[1]*xs+p[2]) ),xs,xi )
            y_sol[j] = p[0]*(x_sol[j]**3.0)+p[1]*(x_sol[j]**2.0)+p[2]*x_sol[j]+p[3]"""


	for j in range (0,len(x)):
	    self.xi = x[j]
	    self.yi = y[j]
            
	    
	    x_sol[j] = fsolve(self.myfunc,self.xi)
            y_sol[j] = self.param[0]*(x_sol[j]**3.0)+self.param[1]*(x_sol[j]**2.0)+self.param[2]*x_sol[j]+self.param[3]

	x_sol = np.array(x_sol)
	ind_sort = np.argsort(x_sol.T)
	ind_sort = np.array(ind_sort).reshape(len(x),1)
	
	#print "ind_sort:", ind_sort
        x_sort = np.sort(x_sol.T)
	x_sort = np.array(x_sort).reshape(len(x),1)
	for j in range (0,len(x)):	
	    y_sort[j] = y_sol[ind_sort[j]]
	    z_sort[j] = z_sol[ind_sort[j]]

	#print "x_sort:", x_sort

	
	if len(x_sort) == 1:
           x_class = x_sort
           y_class = y_sort
	   z_class = z_sort
        else:
           dx = x_sort[:-1]-x_sort[1:]
           dy = y_sort[:-1]-y_sort[1:]
           e = np.sqrt(dx**2.0+dy**2.0)
	   ##############################
	   
	   class_num = [k for (k, val) in enumerate(e) if val > self.class_threshold]
           print "class_num:", len(class_num)+1
	   #class_num = find(e>link_dist_thres);
           ##############################
           if class_num==[]:
              x_class = np.mean(x_sort)
              y_class = np.mean(y_sort)
              z_class = np.mean(z_sort)
           else:
              x_class = np.zeros((len(class_num)+1,1),float)
              y_class = np.zeros((len(class_num)+1,1),float)
	      z_class = np.zeros((len(class_num)+1,1),float)
              init_iter = 0
              for j in range (0,len(class_num)):
                  x_class[j] = np.mean(x_sort[init_iter:class_num[j]+1])
                  y_class[j] = np.mean(y_sort[init_iter:class_num[j]+1])
		  z_class[j] = np.mean(z_sort[init_iter:class_num[j]+1])
                  init_iter = class_num[j]+1
              x_class[-1] = np.mean(x_sort[init_iter:])
              y_class[-1] = np.mean(y_sort[init_iter:])
	      z_class[-1] = np.mean(z_sort[init_iter:])
	
        #psi_class = np.arctan(3.0*self.param[0]*(x_class**2.0)+2.0*self.param[1]*x_class+self.param[2]*np.ones((len(x_class),1),float))
	
	#print "x_class:", x_class
	#print "y_class:", y_class

	x_class = np.array(x_class)
	y_class = np.array(y_class)
	z_class = np.array(z_class)
	psi_class = np.zeros((len(x_class),1))
	for j in range(0,len(x_class)):
	    self.xWP = x_class[j]
	    self.yWP = y_class[j]
	    #x_new_point = fsolve(self.myfunc1,self.xWP+np.cos(np.arctan(3*self.param[0]*(self.xWP**2.0)+2*self.param[1]*self.xWP+self.param[2]))*self.sonar_range)
	    #y_new_point = self.param[0]*(x_new_point**3.0)+self.param[1]*(x_new_point**2.0)+self.param[2]*x_new_point+self.param[3]
	    #psi_class[j] = np.arctan((y_new_point-self.yWP)/(x_new_point-self.xWP))
            psi_class[j] = np.arctan((y_class[-1]-self.yWP)/(x_class[-1]-self.xWP))
	psi_class[-1] = np.arctan(3*self.param[0]*(x_class[-1]**2.0)+2*self.param[1]*x_class[-1]+self.param[2])
        psi_class[0] = np.arctan(3*self.param[0]*(x_class[0]**2.0)+2*self.param[1]*x_class[0]+self.param[2]) + math1.pi
	psi_class = np.array(psi_class)
	

	self.final_points = np.zeros((len(x_class), 4))
	
	self.final_points[:,0] = x_class.T
	self.final_points[:,1] = y_class.T
	self.final_points[:,2] = z_class.T
	self.final_points[:,3] = psi_class.T
	
	print "WPs:", self.final_points
	
	markerArray = MarkerArray()
	
	for i in range(0, len(x_class)):
            marker = Marker()
	    marker.pose.position.x = x_class[i]
	    marker.pose.position.y = y_class[i]
	    marker.pose.position.z = z_class[i]
            psi_rviz   = psi_class[i]

            #q = tf.transformations.quaternion_from_euler(psi_rviz, 0, 0,'rzyx')
	    marker.pose.orientation.x = 0.0
	    marker.pose.orientation.y = 0.0
	    marker.pose.orientation.z = math1.sin(psi_rviz/2.0)
	    marker.pose.orientation.w = math1.cos(psi_rviz/2.0)
	    marker.header.frame_id = '/world'
	    marker.header.stamp = rospy.Time.now()
	    marker.scale.x = 0.25
            marker.scale.y = 0.25
	    marker.scale.z = 0.25
	    marker.color.r = 1.0
            marker.color.g = 1.0
            marker.color.b = 0.0
            marker.color.a = 1.0
	    marker.id = i
	    markerArray.markers.append(marker)
        
        self.pub_sonar_wps.publish(markerArray)
	
        
    def myfunc(self,x):
	
	
        p0 = float(self.param[0])
	p1 = float(self.param[1])
	p2 = float(self.param[2])
	p3 = float(self.param[3])

	return ((x-self.xi)+(p0*(x**3.0)+p1*(x**2.0)+p2*x+p3-self.yi)*(3*p0*(x**2.0)+2*p1*x+p2) )

    def myfunc1(self,x):
        p0 = float(self.param[0])
	p1 = float(self.param[1])
	p2 = float(self.param[2])
	p3 = float(self.param[3])
	return ( ((x-self.xWP)**2.0) + ((p0*(x**3.0)+p1*(x**2.0)+p2*x+p3-self.yWP)**2.0) - (self.sonar_range**2.0) )

    def planning (self,data):

	self.iter_WPs= min(self.iter_WPs,len(self.final_points[:,0])-1) 
	x_des = self.final_points[self.iter_WPs][0]
	y_des = self.final_points[self.iter_WPs][1]
	z_des = self.final_points[self.iter_WPs][2]
        psi_des = self.final_points[self.iter_WPs][3]


	ex = self.x - x_des
        ey = self.y - y_des
        ez = self.z - z_des
        eyaw = self.yaw - psi_des

	#Criteria for reaching WP
        #error = np.sqrt(ex**2+ey**2+ez**2+eyaw**2)
        error = np.sqrt(ex**2+ey**2+ez**2)
        
        print "Error Criteria:", error,"\n"

        
	"""print "COUNTER:", self.counter_start
	#Trajectory via WPs
	if error < self.error_threshold and eyaw < 0.035:
           if self.iter_WPs == 0 and self.counter_start < 1100: #First WP
              self.counter_start = self.counter_start + 1
              self.circle_start = True
           #elif self.iter_WPs == len(self.final_points[:,0]-1) and self.counter_stop < 1100: #Last WP
              #self.circle_start = True
           else:
              #Normal Operation
              self.circle_start = False
	      self.iter_WPs = self.iter_WPs + 1""" 

	if error < self.error_threshold and eyaw < 0.035:
           self.iter_WPs = self.iter_WPs + 1 


	#psi_des = 0.0
	theta_des = 0.0

        data.header.stamp = rospy.Time.now()
    	data.header.frame_id = ""
	data.goal.priority = 0
	data.altitude_mode = False   

    	data.position.north =  x_des
	data.position.east =  y_des
	data.position.depth =  z_des
    	 
	data.altitude =  0.0

	data.orientation.roll = 0.0
	data.orientation.pitch = theta_des
	data.orientation.yaw = psi_des

	data.disable_axis.x = False
	data.disable_axis.y = False
	data.disable_axis.z = False
	data.disable_axis.roll = True
	data.disable_axis.pitch = False
	data.disable_axis.yaw = False

	data.position_tolerance.x = 0.0
	data.position_tolerance.y = 0.0
	data.position_tolerance.z = 0.0
	data.orientation_tolerance.roll = 0.0
	data.orientation_tolerance.pitch = 0.0
	data.orientation_tolerance.yaw = 0.0

	print "TOTAL WPS:", len(self.final_points),"\n"
        print "NEXT WP:",  self.iter_WPs, " ", x_des, y_des, z_des, psi_des
        
	print "current:", " ", "North: %.2f" % self.x, "East: %.2f " % self.y, "Depth: %.2f " % self.z, "Pitch: %.2f" % self.pitch, "Yaw: %.2f " % self.yaw, "\n"
	#print "current:", " ", "North:", self.x, " ",  "East:", " ", self.y, " ", "Depth:", self.z, " ", "Pitch:", self.pitch, " ","Yaw:", self.yaw, "\n"
	#print "desired:", " ", "North:", x_des, " ",  "East:", " ", y_des, " ", "Depth:", z_des, " ", "Pitch:", theta_des, " ","Yaw:", psi_des, "\n"
	print "desired:", " ", "North: %.2f" % x_des, "East: %.2f " % y_des, "Depth: %.2f " % z_des, "Pitch: %.2f" % theta_des, "Yaw: %.2f " % psi_des, "\n"
	self.pub_msg.publish(data)
        self.pub_start_circle.publish(Bool(self.circle_start))
        
	
if __name__ == '__main__':
    try:
        rospy.init_node('g500SonarPlanner')
        dt = 0.2
	
        g500SonarPlannerObj = g500SonarPlanner(rospy.get_name())
        WP_msg = WorldWaypointReq()
        
	#plt.ion()
        rospy.sleep(2.0)
	while not rospy.is_shutdown():
              t =  rospy.Time.now().to_sec() - g500SonarPlannerObj.t0 
	  
	      g500SonarPlannerObj.calcChain()
              g500SonarPlannerObj.planning (WP_msg)
	      
	      #plt.plot(g500SonarPlannerObj.WPS[:,0], g500SonarPlannerObj.WPS[:,1], 'ro')
	      #max_x = max(g500SonarPlannerObj.WPS[:,0])
	      #min_x = min(g500SonarPlannerObj.WPS[:,0])
	      #xe = np.linspace(min_x,max_x,100)	      
	      #ye = g500SonarPlannerObj.param[0]*(xe**3) + g500SonarPlannerObj.param[1]*(xe**2) + g500SonarPlannerObj.param[2]*xe + 1.0*g500SonarPlannerObj.param[3]*(xe**0)
	      #plt.plot(xe, ye, 'bo')
	      #plt.plot(WP_msg.position.north,WP_msg.position.east, 'go')       
	      #plt.axes().set_aspect('equal')
	      #plt.axis([-10, 10, -10, 10])
              #plt.draw()

              rospy.sleep(dt)
	rospy.spin()
    except rospy.ROSInterruptException: pass 
