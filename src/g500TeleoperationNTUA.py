#!/usr/bin/env python

# ROS imports
import roslib 
roslib.load_manifest('cola2_control')
import rospy

# Msgs imports
from auv_msgs.msg import *
from nav_msgs.msg import Odometry
from std_srvs.srv import Empty, EmptyResponse, EmptyRequest
from std_msgs.msg import Float32, Bool
from sensor_msgs.msg import Joy

# Python imports
from numpy import *
from numpy.linalg import *
from math import *

# Custom imports
import cola2_lib
#import cola2_ros_lib



class g500Teleoperation:
    def __init__(self, name):
        """ Joystick Control of G500 AUV  """
        self.name = name
	self.enable = True
        self.power_limit = 1.0

	#Get Initial Time
        self.t0 = rospy.Time.now().to_sec()  
	self.dt = 0.1

        #Initilize States
	#Vehicle
	self.x = 0.
	self.y = 0.
	self.z = 0.
        self.roll = 0.
	self.pitch = 0.
	self.yaw = 0.
	self.u = 0.
        self.v = 0.
        self.w = 0.
        self.p = 0.
        self.q = 0.
        self.r = 0.
	#Arm
	#TBD
       
	#Initilize Desired Velocities
	#Vehicle	
	self.u_d = 0.
        self.v_d = 0.
        self.w_d = 0.
        self.p_d = 0.
        self.q_d = 0.
        self.r_d = 0.
	#Arm
	
	#Initialize Integrals for "I" term
	self.integral_u = 0.
        self.integral_v = 0.
        self.integral_w = 0.
        self.integral_r = 0.      

        # Create publisher
        self.pub_tau = rospy.Publisher("/cola2_control/body_force_req", BodyForceReq)
        
        # Create Subscriber
        rospy.Subscriber("/cola2_navigation/nav_sts", NavSts, self.updateNavSts)
	rospy.Subscriber("/joy", Joy, self.updateJoystick)

        #Decision Making
        #rospy.Subscriber("/sonar_decision_making/start", Bool, self.decisionStart)
        #rospy.Subscriber("/ntua_decision_making/panel_reach", Bool, self.decisionPanelReach)
        #rospy.Subscriber("/ntua_decision_making/sonar", Bool, self.decisionSonar)

	#self.start_flag = False
        #self.panelReach_flag = False
        #self.sonar_flag = False

        #Create services
	self.enable_srv = rospy.Service('/ntua_control_g500/enable_teleoperation', Empty, self.enableSrv)
        self.disable_srv = rospy.Service('/ntua_control_g500/disable_teleoperation', Empty, self.disableSrv)     

    def enableSrv(self, req):
        self.enable = True
        rospy.loginfo('%s Enabled', self.name)
        return EmptyResponse()
    
        
    def disableSrv(self, req):
        self.enable = False
        rospy.loginfo('%s Disabled', self.name)
        return EmptyResponse()


    #def decisionStart(self, msg):
        #self.start_flag = msg.data

    #def decisionPanelReach(self, msg):
        #self.panelReach_flag = msg.data
    
    #def decisionSonar(self, msg):
        #self.sonar_flag = msg.data
    
        
    def updateNavSts(self, nav_sts):
	self.x = nav_sts.position.north
	self.y = nav_sts.position.east
	self.z = nav_sts.position.depth
        self.roll = nav_sts.orientation.roll
	self.pitch = nav_sts.orientation.pitch
	self.yaw = nav_sts.orientation.yaw
        self.u = nav_sts.body_velocity.x
        self.v = nav_sts.body_velocity.y
        self.w = nav_sts.body_velocity.z
        self.p = nav_sts.orientation_rate.roll
        self.q = nav_sts.orientation_rate.pitch
        self.r = nav_sts.orientation_rate.yaw
	self.t_nav = nav_sts.header.stamp.to_sec()

    def updateJoystick(self, msgJOY):
        self.u_d = self.power_limit*msgJOY.axes[1]
        self.v_d = -self.power_limit*msgJOY.axes[0]
        self.w_d = -self.power_limit*msgJOY.axes[3]
        self.r_d = -self.power_limit*msgJOY.axes[2]

	#for i in range (0,4):
	"""if msgJOY.buttons[6] == 1:
	   if msgJOY.buttons[0] == 1:
              self.bufferJoy[0] += 0.001
	   if msgJOY.buttons[1] == 1:
	      self.bufferJoy[1] += 0.001
	   if msgJOY.buttons[2] == 1:
              self.bufferJoy[2] += 0.001
	   if msgJOY.buttons[3] == 1:
	      self.bufferJoy[3] += 0.001
	else:
	   self.bufferJoy[0] = 0.
	   self.bufferJoy[1] = 0.
	   self.bufferJoy[2] = 0.
           self.bufferJoy[3] = 0.

        self.q0_d =  self.bufferJoy[0]
        self.q1_d =  self.bufferJoy[1]
        self.q2_d =  self.bufferJoy[2]
        self.q3_d =  self.bufferJoy[3]"""

    def vehicleVelCtrl(self):

   	#Filling up states
	x = self.x
	y = self.y
	z = self.z
	phi = self.roll
	theta = self.pitch
	psi = self.yaw
	u = self.u
	v = self.v
	w = self.w
	p = self.p
	q = self.q
	r = self.r

	#Calculating Jacobians
	#NOT NEEDED FOR THE TIME...
	J1 = [cos(theta)*cos(psi),  cos(psi)*sin(theta)*sin(phi)-sin(psi)*cos(phi),  sin(psi)*sin(phi)+cos(psi)*cos(phi)*sin(theta),
      	      cos(theta)*sin(psi),  cos(psi)*cos(phi)+sin(phi)*sin(theta)*sin(psi),  sin(psi)*sin(theta)*cos(phi)-cos(psi)*sin(phi),
              -sin(theta),          cos(theta)*sin(phi),                             cos(theta)*cos(phi)]

	J2 = [1.0,  sin(phi)*tan(theta),  cos(phi)*tan(theta),
              0.0,  cos(phi),             -sin(phi),
              0.0,  sin(phi)/cos(theta),  cos(phi)/cos(theta)]

	J1 = array(J1).reshape(3,3)
	J2 = array(J2).reshape(3,3)

	J1_INV = inv(J1)
	J2_INV = inv(J2)

	
	#G500 Dynamic model parameters !!!Must Be Edited For G500!!!
	Mu = 126.5000
	Mv = 273.8433
	Mw = 213.0535
	Mp = 55.0000
	Mq = 67.8951
	Mr = 67.8951
	zB = -0.02
	W  = 539.5500
	B  = 546.1106
	Xuu = -31.8086
        Yvv = -222.8960
        Zww = -263.4225
        Kpp = -0.0000
        Mqq = -40.5265
        Nrr = -40.5265
	Xu = -2.0000
        Yv = -23.0000
        Zw = -0.0000
        Kp = -10.0000
        Mq = -0.0000
        Nr = -0.0000


	"""k_u=200.0
        k_x=100.0
	k_v=200.0
        k_y=110.0
	k_w=220.0
        k_z=170.0
	k_q=200.0
        k_theta=150.0
	k_r=200.0
        k_psi=100.0
	nu=0.
	nv=0.
	nw=0.
	nq=0.
	nr=0."""	
        
	#Model Based Robust Pose Controller

        X_model = Mw*w*q-Mv*v*r+(W-B)*sin(theta)-Xu*u-Xuu*u*abs(u)
	Y_model = -Mw*w*p+Mu*u*r-(W-B)*cos(theta)*sin(phi)-Yv*v-Yvv*v*abs(v)
	Z_model = Mv*v*p-Mu*u*q-(W-B)*cos(theta)*cos(phi)-Zw*w-Zww*w*abs(w)
	M_model = -Mw*w*u+Mu*u*w-Mr*r*p+Mp*p*r-zB*B*sin(theta)-Mq*q-Mqq*q*abs(q)
	N_model = Mv*v*u-Mu*u*v+Mq*q*p-Mp*p*q-Nr*r-Nrr*r*abs(r)

	#Model Based Robust Pose Controller
        #X = -k_u*self.u_d + X_model
	#Y = -k_v*self.v_d + Y_model 
        #Z = -k_w*self.w_d + Z_model + 0.05*cos(theta)*cos(phi)
        #M = -k_q*self.q_d + M_model - zB*B*sin(theta)*0.05
        #N = -k_r*self.r_d + N_model

	#Model Free Robust Pose Controller

	eu = u - self.u_d
	ev = v - self.v_d
	ew = w - self.w_d
	er = r - self.r_d

	k_u=400.0 * 0.25
        k_v=400.0 *0.25
	k_w=420.0 * 0.25
	k_q=0.0
	k_r=400.0 * 0.25

        ki_u=0.0
        ki_v=0.0
	ki_w=0.0
	ki_q=0.0
	ki_r=0.0

	

	self.integral_u = self.integral_u + eu*self.dt
        self.integral_v = self.integral_v + ev*self.dt
        self.integral_w = self.integral_w + ew*self.dt
        self.integral_r = self.integral_r + er*self.dt      

        if self.integral_u > 10.0:
           self.integral_u = 10.0
        if self.integral_u < -10.0:
           self.integral_u = -10.0 

	if self.integral_v > 10.0:
           self.integral_v = 10.0
        if self.integral_v < -10.0:
           self.integral_v = -10.0
  
        if self.integral_w > 10.0:
           self.integral_w = 10.0
        if self.integral_w < -10.0:
           self.integral_w = -10.0 

        if self.integral_r > 10.0:
           self.integral_r = 10.0
        if self.integral_r < -10.0:
           self.integral_r = -10.0


	X = -k_u*eu + ki_u*self.integral_u
	Y = -k_v*ev + ki_v*self.integral_v
        Z = -k_w*ew + ki_w*self.integral_w
        #M = -k_theta*tempR[0]
        M = 0.0
	N = -k_r*er + ki_r*self.integral_r

        #X=0.0
        #Y=0.0
        #Z = 0.0
        #M=0.0
        #N=0.0	

	#Saturate Control Signals (Body Forces)
	if X > 100.0:
	   X = 100.0
	elif X < -100.0:
             X = -100.0	

	if Y > 100.0:
	   Y = 100.0
	elif Y < -100.0:
             Y = -100.0	

	if Z > 100.0:
	   Z = 100.0
	elif Z < -100.0:
             Z = -100.0	

	if M > 100.0:
	   M = 100.0
	elif M < -100.0:
             M = -100.0

	if N > 100.0:
	   N = 100.0
	elif N < -100.0:
             N = -100.0		


	#Scale Control Signals (Body Forces)
	"""X = X/100.00
	Y = Y/100.00
	Z = Z/100.00
	M = M/100.00
	N = N/100.00"""
	
	U = array([X, Y, Z, M, N])
	#print U
	return U
         
    def iterate(self): 
        
        # Main loop
	t = rospy.Time.now().to_sec() - self.t0  
        
        #print "current velocity:", str(self.u), str(self.v), str(self.w), str(self.q), str(self.r), "\n"
	#print "desired velocity:", str(self.u_d), str(self.v_d), str(self.w_d), str(self.q_d), str(self.r_d), "\n"
	#print "joints:", self.q0d_d, self.q1d_d, self.q2d_d, self.q3d_d
	if self.enable == False:# and self.sonar_flag == False and self.panelReach_flag == False:
           print "UVMS Teleoperation DOWN", "\n"
               
	else:
           """if self.sonar_flag == True:
              self.u_d = 0.0
	      self.v_d = 0.0
	      self.w_d = 0.0
	      self.r_d = 0.1"""

	   #print "UVMS Teleoperation UP", "\n"
	   U = self.vehicleVelCtrl()
            
           data = BodyForceReq()
                
           data.header.stamp = rospy.Time.now()
           data.header.frame_id = "vehicle_frame"
           data.goal.requester = "uvms velocity controller"
           data.goal.id = 0
           data.goal.priority = GoalDescriptor.PRIORITY_NORMAL
           data.wrench.force.x = U[0]
           data.wrench.force.y = U[1]
           data.wrench.force.z = U[2]-0.175   
           data.wrench.torque.x = 0.
           data.wrench.torque.y = U[3]
           data.wrench.torque.z = U[4]
            
           data.disable_axis.x = False
           data.disable_axis.y = False
           data.disable_axis.z = False
           data.disable_axis.roll = True
           data.disable_axis.pitch = True
           data.disable_axis.yaw = False

            
           self.pub_tau.publish(data)


if __name__ == '__main__':
    try:
        rospy.init_node('g500TeleoperationNTUA')
        teleObj = g500Teleoperation(rospy.get_name())
	
	while not rospy.is_shutdown():
		rospy.sleep(0.05)
		teleObj.iterate()
		rospy.sleep(0.05)

        rospy.spin()

    except rospy.ROSInterruptException: pass
