#!/usr/bin/env python

# ROS imports
import roslib 
roslib.load_manifest('cola2_control')
import rospy

# Msgs imports
from auv_msgs.msg import *
from nav_msgs.msg import Odometry
from std_srvs.srv import Empty, EmptyResponse, EmptyRequest
from std_msgs.msg import Float32

# Python imports
from numpy import *
from numpy.linalg import *
from math import *

# Custom imports
import cola2_lib

from dynamic_reconfigure.server import Server as DynamicReconfigureServer
from ntua_pandorag500.cfg import control_parametersConfig as ConfigType


class G500WPTrackingController:
    def __init__(self, name):
        """ Controls the pose of G500 AUV  """
        self.name = name
	self.enable = True
        self.power_limit = 1.0
	#Get Initial Time
        self.t0 = rospy.Time.now().to_sec()  
        
        self.dt = 0.1
	
        #Initilize States
	self.x = 0.
	self.y = 0.
	self.z = 0.
        self.roll = 0.
	self.pitch = 0.
	self.yaw = 0.
        self.u = 0.
        self.v = 0.
        self.w = 0.
        self.p = 0.
        self.q = 0.
        self.r = 0.
	self.t_nav = 0.

	
	#Initialize Integrals for "I" term
	self.integral_x = 0.
        self.integral_y = 0.
        self.integral_z = 0.
        self.integral_theta = 0.
        self.integral_psi = 0.      

       
	#Initilize Desired Values for Wps
	self.x_des = 0.0
	self.y_des = 0.0
	self.z_des = 1.5
	self.psi_des = 0.0
        self.theta_des = 0.0

        # Create publisher
        self.pub_tau = rospy.Publisher("/cola2_control/body_force_req", BodyForceReq)
        
        # Create Subscriber
        rospy.Subscriber("/cola2_navigation/nav_sts", NavSts, self.updateNavSts)
	rospy.Subscriber("/ntua_wp_planner/world_waypoint_req",WorldWaypointReq, self.updateWPs) 
	

        #Create services
	self.enable_srv = rospy.Service('/ntua_controller/enable', Empty, self.enableSrv)
        self.disable_srv = rospy.Service('/ntua_controller/disable', Empty, self.disableSrv)   

        # Create a dynamic reconfigure server.
        self.server = DynamicReconfigureServer(ConfigType, self.reconfigure)  

    def enableSrv(self, req):
        self.enable = True
        rospy.loginfo('%s Enabled', self.name)
        return EmptyResponse()
    
        
    def disableSrv(self, req):
        self.enable = False
        rospy.loginfo('%s Disabled', self.name)
        return EmptyResponse()


    def updateNavSts(self, nav_sts):
	self.x = nav_sts.position.north
	self.y = nav_sts.position.east
	self.z = nav_sts.position.depth
        self.roll = nav_sts.orientation.roll
	self.pitch = -nav_sts.orientation.pitch
	self.yaw = nav_sts.orientation.yaw
        self.u = nav_sts.body_velocity.x
        self.v = nav_sts.body_velocity.y
        self.w = nav_sts.body_velocity.z
        self.p = nav_sts.orientation_rate.roll
        self.q = -nav_sts.orientation_rate.pitch
        self.r = nav_sts.orientation_rate.yaw
	self.t_nav = nav_sts.header.stamp.to_sec()

    def updateWPs(self, wp_msg):
	self.x_des = wp_msg.position.north
	self.y_des = wp_msg.position.east
	self.z_des = wp_msg.position.depth
	self.psi_des = wp_msg.orientation.yaw
        self.theta_des = wp_msg.orientation.pitch
	self.t_wp = wp_msg.header.stamp.to_sec()


    def controlScheme(self,t):

   	#Filling up states
	x = self.x
	y = self.y
	z = self.z
	phi = self.roll
	theta = self.pitch
	psi = self.yaw
	u = self.u
	v = self.v
	w = self.w
	p = self.p
	q = self.q
	r = self.r

	#Calculating Jacobians
	J1 = [cos(theta)*cos(psi),  cos(psi)*sin(theta)*sin(phi)-sin(psi)*cos(phi),  sin(psi)*sin(phi)+cos(psi)*cos(phi)*sin(theta),
      	      cos(theta)*sin(psi),  cos(psi)*cos(phi)+sin(phi)*sin(theta)*sin(psi),  sin(psi)*sin(theta)*cos(phi)-cos(psi)*sin(phi),
              -sin(theta),          cos(theta)*sin(phi),                             cos(theta)*cos(phi)]

	J2 = [1.0,  sin(phi)*tan(theta),  cos(phi)*tan(theta),
              0.0,  cos(phi),             -sin(phi),
              0.0,  sin(phi)/cos(theta),  cos(phi)/cos(theta)]

	J1 = array(J1).reshape(3,3)
	J2 = array(J2).reshape(3,3)

	J1_INV = inv(J1)
	J2_INV = inv(J2)

	print "t:", t

	#Define Desired SetPoints (5 WPs in 3D space)
	#As read from the WorldWaypointReq Msg
	x_des = self.x_des
	y_des = self.y_des
	z_des = self.z_des
	psi_des = self.psi_des
	theta_des = self.theta_des

	#psi_des = 0.#0.3
	dx_des = 0.
	dy_des = 0.
	dz_des = 0.
	dtheta_des = 0.
	dpsi_des = 0.
	d2x_des = 0.
	d2y_des = 0.
	d2z_des = 0.
	d2theta_des = 0.
	d2psi_des = 0.

	#Calculating errors
	e_x = x-x_des
	e_y = y-y_des
	e_z = z-z_des
	e_theta = theta-theta_des
	e_psi = psi-psi_des	
	
	if e_psi < -pi:
	   e_psi = 2.0*pi + e_psi
	elif e_psi > pi:
           e_psi = -(2*pi - e_psi)
	
	
	tempT = dot(J1_INV,array([[e_x], [e_y], [e_z]]))
        tempR = dot(J2_INV[1:3,1:3],array([[e_theta], [e_psi]]))

	
	"""k_u=200.0
        k_x=150.0
	k_v=200.0
        k_y=150.0
	k_w=220.0
        k_z=200.0
	k_q=200.0
        k_theta=150.0
	k_r=200.0
        k_psi=150.0
	nu=0.
	nv=0.
	nw=0.
	nq=0.
	nr=0."""

        #Dynamic Reconfigure
        k_x = self.k_x
	k_y = self.k_y
	k_z = self.k_z
	k_theta = self.k_theta
	k_psi = self.k_psi
	k_u = self.k_u
	k_v = self.k_v
	k_w = self.k_w
	k_q = self.k_q
	k_r = self.k_r
        ki_x = self.ki_x
        ki_y = self.ki_y
	ki_z = self.ki_z
	ki_theta = self.ki_theta
	ki_psi = self.ki_psi
	nu = self.nu
	nv = self.nv
	nw = self.nw
	nq = self.nq
	nr = self.nr

	

	self.integral_x = self.integral_x + tempT[0]*self.dt
        self.integral_y = self.integral_y + tempT[1]*self.dt
        self.integral_z = self.integral_z + tempT[2]*self.dt
        self.integral_theta = self.integral_theta + tempR[0]*self.dt 
        self.integral_psi = self.integral_psi + tempR[1]*self.dt      

        if self.integral_x > 10.0:
           self.integral_x = 10.0
        if self.integral_x < -10.0:
           self.integral_x = -10.0 

	if self.integral_y > 10.0:
           self.integral_y = 10.0
        if self.integral_y < -10.0:
           self.integral_y = -10.0
  
        if self.integral_z > 10.0:
           self.integral_z = 10.0
        if self.integral_z < -10.0:
           self.integral_z = -10.0 

        if self.integral_theta > 10.0:
           self.integral_theta = 10.0
        if self.integral_theta < -10.0:
           self.integral_theta = -10.0 

        if self.integral_psi > 10.0:
           self.integral_psi = 10.0
        if self.integral_psi < -10.0:
           self.integral_psi = -10.0

	#Model Free Robust Pose Controller
	X = -k_u*u-k_x*tempT[0] - ki_x*self.integral_x
	Y = -k_v*v-k_y*tempT[1] - ki_y*self.integral_y
        Z = -k_w*w-k_z*tempT[2] - ki_z*self.integral_z
        #M = -k_q*q-k_theta*tempR[0]-4.4*sin(theta_des)*100.0  + ki_theta*self.integral_theta
        M = -k_q*q-k_theta*tempR[0] - ki_theta*self.integral_theta
        #M = 0.0
	N = -k_r*r-k_psi*tempR[1]  - ki_psi*self.integral_psi


        #X=0.0
        #Y=0.0
        #Z = 0.0
        #M=0.0
        #N=0.0
	
	print "THE CONTROLLER SEES:",
        print "e_x:", e_x," ", "e_y:", e_y," ",  "e_z:", e_z," ",  "e_psi:", e_psi*180.0/pi	

	#Saturate Control Signals (Body Forces)
	if X > 100.0:
	   X = 100.0
	elif X < -100.0:
             X = -100.0	

	if Y > 100.0:
	   Y = 100.0
	elif Y < -100.0:
             Y = -100.0	

	if Z > 100.0:
	   Z = 100.0
	elif X < -100.0:
             X = -100.0	

	if M > 100.0:
	   M = 100.0
	elif M < -100.0:
             M = -100.0

	if N > 100.0:
	   N = 100.0
	elif N < -100.0:
             N = -100.0		

	
	U = array([X, Y, Z, M, N])

	return U
         
    def iterate(self): 
        
        # Main loop
	t = rospy.Time.now().to_sec() - self.t0  
        
        print "current state:", str(self.x), str(self.y), str(self.z), str(self.pitch), str(self.yaw)
	
	if self.enable == False:
           print "WP Controller is down", "\n"
               
	else:
	   print "WP Controller is up: VEHICLE IN CLOSED LOOP MODE", "\n"
		
	   U = self.controlScheme(t)
            
           data = BodyForceReq()
                
           data.header.stamp = rospy.Time.now()
           data.header.frame_id = "vehicle_frame"
           data.goal.requester = "wp tracking controller"
           data.goal.id = 0
           data.goal.priority = GoalDescriptor.PRIORITY_NORMAL
           data.wrench.force.x = U[0]*self.power_limit
           data.wrench.force.y = U[1]*self.power_limit
           data.wrench.force.z = U[2]*self.power_limit-4.25        
           data.wrench.torque.x = 0.
           data.wrench.torque.y = U[3]*self.power_limit
           data.wrench.torque.z = U[4]*self.power_limit
            
           data.disable_axis.x = False
           data.disable_axis.y = False
           data.disable_axis.z = False
           data.disable_axis.roll = True
           data.disable_axis.pitch = False
           data.disable_axis.yaw = False
            
           self.pub_tau.publish(data)

    def reconfigure(self, config, level):
        # Fill in local variables with values received from dynamic reconfigure clients (typically the GUI).
        self.k_x = config["k_x"]
	self.k_y = config["k_y"]
	self.k_z = config["k_z"]
	self.k_theta = config["k_theta"]
	self.k_psi = config["k_psi"]
	self.k_u = config["k_u"]
	self.k_v = config["k_v"]
	self.k_w = config["k_w"]
	self.k_q = config["k_q"]
	self.k_r = config["k_r"]
        self.ki_x = config["ki_x"]
        self.ki_y  = config["ki_y"]
	self.ki_z = config["ki_z"]
	self.ki_theta = config["ki_theta"]
	self.ki_psi  = config["ki_psi"]
	self.nu = config["nu"]
	self.nv = config["nv"]
	self.nw = config["nw"]
	self.nq = config["nq"]
	self.nr = config["nr"]

        # Return the new variables.
        return config


if __name__ == '__main__':
    try:
        rospy.init_node('ntua_g500_controller')
        controlObject = G500WPTrackingController(rospy.get_name())
	
	while not rospy.is_shutdown():
		controlObject.iterate()
		rospy.sleep(controlObject.dt)
        rospy.spin()

    except rospy.ROSInterruptException: pass
