#!/usr/bin/env python

# ROS imports
import roslib 
roslib.load_manifest('cola2_control')
import rospy

# Msgs imports
from cola2_control.msg import ThrustersData
from auv_msgs.msg import BodyForceReq

# Python imports
from numpy import array, zeros, dot, sqrt
from numpy.linalg import pinv


class ThrusterAllocator :
    
    def __init__(self, name):
        """ Controls the velocity and pose of an AUV  """
        self.name = name
       
        # Create publisher
        self.pub = rospy.Publisher("/cola2_control/thrusters_data", ThrustersData)
        
        # Create Subscriber
        rospy.Subscriber("/cola2_control/merged_body_force_req", BodyForceReq, self.updateBodyForceReq)


    def updateBodyForceReq(self, w) :
        # Input data 
        body_force_req = zeros(6)

        if not w.disable_axis.x : 
            body_force_req[0] = w.wrench.force.x
        if not w.disable_axis.y : 
            body_force_req[1] = w.wrench.force.y
        if not w.disable_axis.z : 
            body_force_req[2] = w.wrench.force.z
        if not w.disable_axis.roll : 
            body_force_req[3] = w.wrench.torque.x
        if not w.disable_axis.pitch : 
            body_force_req[4] = w.wrench.torque.y
        if not w.disable_axis.yaw : 
            body_force_req[5] = w.wrench.torque.z
        
        setpoint  = self.bodyForces2Setpoints(body_force_req)
        
        # Log and send computed data
        thrusters = ThrustersData()
        thrusters.header.stamp = rospy.Time.now()
        thrusters.header.frame_id = "girona500"
        thrusters.setpoints = setpoint

        #publish
        self.pub.publish(thrusters)


    def bodyForces2Setpoints(self, body_force_req):
        setpoint = zeros(5)
        thrusts = zeros(5)

        #Girona500 Thrusters
        dh = 0.2587
        dv = 0.5587
        b = [-1.0, -1.0,  0.0,  0.0, 0.0,
              0.0,  0.0,  0.0,  0.0, 1.0,
              0.0,  0.0, -1.0, -1.0, 0.0,
              0.0,  0.0,  0.0,  0.0, 0.0,
              0.0,  0.0,  -dv,   dv, 0.0,
              -dh,   dh,  0.0,  0.0, 0.0]
        b = array(b).reshape(6, 5)

        # Why diveided by 9.81?
        thrusts = dot(pinv(b), body_force_req)/9.81

        for i in range(5):
            # TODO: What are these values?
            if thrusts[i] >= 0.0030437*53.62**2 - 0.1770504*53.62 + 2.5747336:
                setpoint[i] = (0.1770504 + sqrt(0.1770504**2 - 4.0*0.0030437*(2.5747336-thrusts[i]))) / (2.0*0.0030437)
            elif thrusts[i] >= 0.0:
                setpoint[i] = sqrt(thrusts[i]/0.0006374)
            elif thrusts[i] >= -0.000575*(-52.7)**2:
                setpoint[i] = -sqrt(thrusts[i]/-0.000575)
            else:
                setpoint[i] = (0.1559903+sqrt(0.1559903**2-4.0*(-0.0027196)*(-2.2367814-thrusts[i])))/(2.0*(-0.0027196))
            
            # Normalize
            if setpoint[i] > 100.0:
                setpoint[i] = 1.0
            elif setpoint[i] < -100:
                setpoint[i] = -1.0 
            else:
                setpoint[i] = setpoint[i] / 100.0 
        
        return setpoint


if __name__ == '__main__':
    try:
        rospy.init_node('new_thruster_allocator')
        thruster_allocator = ThrusterAllocator(rospy.get_name())
        rospy.spin()
    except rospy.ROSInterruptException: pass
