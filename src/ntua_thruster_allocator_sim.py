#!/usr/bin/env python

# ROS imports
import roslib 
roslib.load_manifest('cola2_control')
import rospy

# Msgs imports
from cola2_control.msg import ThrustersData
from auv_msgs.msg import BodyForceReq

# Python imports
from numpy import array, nonzero, zeros, dot, copysign, sqrt
from numpy.linalg import pinv

# Custom imports
import cola2_ros_lib

class ThrusterAllocator :
    
    def __init__(self, name):
        """ Controls the velocity and pose of an AUV  """
        self.name = name
        #   Load parameters
        self.getConfig()
       
    #   Create publisher
        self.pub = rospy.Publisher("/cola2_control/thrusters_data", ThrustersData)
      
    #   Create Subscriber
        rospy.Subscriber("/cola2_control/merged_body_force_req", BodyForceReq, self.updateBodyForceReq)
                
    
    def getConfig(self) :
        """ Load parameters from the rosparam server """
        self.vehicle_name = rospy.get_param('vehicle_name')
        param_dict = {'ctf': 'dynamics/' + self.vehicle_name + '/ctf',
                      'ctb': 'dynamics/' + self.vehicle_name + '/ctb',
                      'dv': 'dynamics/' + self.vehicle_name + '/dv',
                      'dh': 'dynamics/' + self.vehicle_name + '/dh'}
        cola2_ros_lib.getRosParams(self, param_dict)
        
#        self.ctf = rospy.get_param("dynamics/" + self.vehicle_name + "/ctf")
#        self.ctb = rospy.get_param("dynamics/" + self.vehicle_name + "/ctb")
#        self.dv = rospy.get_param("dynamics/" + self.vehicle_name + "/dv")
#        self.dh = rospy.get_param("dynamics/" + self.vehicle_name + "/dh")
        
    def updateBodyForceReq(self, w) :
        # Input data 
        body_force_req = zeros(6)
        #setpoint = zeros(5)

        if not w.disable_axis.x : 
            body_force_req[0] = w.wrench.force.x
        if not w.disable_axis.y : 
            body_force_req[1] = w.wrench.force.y
        if not w.disable_axis.z : 
            body_force_req[2] = w.wrench.force.z
        if not w.disable_axis.roll : 
            body_force_req[3] = w.wrench.torque.x
        if not w.disable_axis.pitch : 
            body_force_req[4] = w.wrench.torque.y
        if not w.disable_axis.yaw : 
            body_force_req[5] = w.wrench.torque.z

        setpoint  = self.bodyForces2Setpoints(body_force_req)

        # Log and send computed data
        thrusters = ThrustersData()
        thrusters.header.stamp = rospy.Time.now()
        thrusters.header.frame_id = "girona500"
        thrusters.setpoints = setpoint
        
        #publish
        self.pub.publish(thrusters)


    def bodyForces2Setpoints(self, body_force_req):
        setpoint = zeros(5)
        temp = zeros(5)

        ct = zeros(len(body_force_req))
        i1 = nonzero(body_force_req >= 0.0)
        i2 = nonzero(body_force_req <= 0.0)
        ct[i1] = self.ctf
        ct[i2] = self.ctb
           
        #Garbi/Girona500 Thrusters
        b = [-ct[0],            -ct[1],             .0,             .0,             .0,
             .0,                .0,                 .0,             .0,             ct[4],
             .0,                .0,                 -ct[2],         -ct[3],         .0,
             .0,                .0,                 .0,             .0,             .0,
             .0,                .0,                 -ct[2]*self.dv, ct[3]*self.dv,  .0,
             -ct[0]*self.dh,     ct[1]*self.dh,     .0,             .0,             .0]
        b = array(b).reshape(6,5)
        
        temp = dot(pinv(b), body_force_req)/(1500.00**2)
        setpoint[0] = copysign(1,temp[0])*sqrt(abs(temp[0]))
        setpoint[1] = copysign(1,temp[1])*sqrt(abs(temp[1]))
        setpoint[2] = copysign(1,temp[2])*sqrt(abs(temp[2]))
        setpoint[3] = copysign(1,temp[3])*sqrt(abs(temp[3]))
        setpoint[4] = copysign(1,temp[4])*sqrt(abs(temp[4]))

        return setpoint
	
if __name__ == '__main__':
    try:
        rospy.init_node('new_thruster_allocator')
        thruster_allocator = ThrusterAllocator(rospy.get_name())
        rospy.spin()
    except rospy.ROSInterruptException: pass
