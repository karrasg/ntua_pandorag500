#!/usr/bin/env python

# ROS imports
import roslib 
roslib.load_manifest('cola2_control')
import rospy

# Msgs imports
from auv_msgs.msg import BodyForceReq, NavSts, BodyVelocityReq, GoalDescriptor
# from nav_msgs.msg import Odometry
from std_srvs.srv import Empty, EmptyResponse
# from std_msgs.msg import Float32

# Python imports
from numpy import zeros 


class VelocityController :
    def __init__(self, name):
        """ Controls the velocity and pose of an AUV  """
        self.name = name
        
        # Load parameters
        self.enable = False
        self.goal = GoalDescriptor()
        
        # Input data 
        self.v = zeros(6)
        self.desired_velocity = zeros(6)
        self.resp = BodyVelocityReq()
        
        # Create publisher
        self.pub_tau = rospy.Publisher("/cola2_control/body_force_req", BodyForceReq)
        
        # Create Subscriber
        rospy.Subscriber("/cola2_navigation/nav_sts", NavSts, self.updateNavSts)
        rospy.Subscriber("/cola2_control/merged_body_velocity_req", BodyVelocityReq, self.updateResponse)
        
        #Create services
        self.enable_srv = rospy.Service('/cola2_control/enable_velocity_controller', Empty, self.enableSrv)
        self.disable_srv = rospy.Service('/cola2_control/disable_velocity_controller', Empty, self.disableSrv)


    def enableSrv(self, req):
        self.enable = True
        rospy.loginfo('%s Enabled', self.name)
        return EmptyResponse()
    
        
    def disableSrv(self, req):
        self.enable = False
        rospy.loginfo('%s Disabled', self.name)
        return EmptyResponse()
    
    
    def updateNavSts(self, nav_sts):
        self.v[0] = nav_sts.body_velocity.x
        self.v[1] = nav_sts.body_velocity.y
        self.v[2] = nav_sts.body_velocity.z
        self.v[3] = nav_sts.orientation_rate.roll
        self.v[4] = nav_sts.orientation_rate.pitch
        self.v[5] = nav_sts.orientation_rate.yaw
        
    
    def updateResponse(self, resp):
        self.goal = resp.goal
        self.desired_velocity[0] = resp.twist.linear.x
        self.desired_velocity[1] = resp.twist.linear.y
        self.desired_velocity[2] = resp.twist.linear.z
        self.desired_velocity[3] = resp.twist.angular.x
        self.desired_velocity[4] = resp.twist.angular.y
        self.desired_velocity[5] = resp.twist.angular.z
        self.resp = resp
        self.iterate()
         
         
    def iterate(self): 
        if self.enable:
            # Main loop
            rospy.loginfo("desired_velocity: %s", str(self.desired_velocity))
            rospy.loginfo("current velocity: %s", str(self.v))
            
            data = BodyForceReq()
            data.header.stamp = rospy.Time.now()
            data.header.frame_id = "girona500"
            data.goal = self.goal
            # data.goal.requester = "velocity_controller"
            data.wrench.force.x = self.desired_velocity[0]*(300.0/0.7)*0.5
            data.wrench.force.y = self.desired_velocity[1]*(150.0/0.15)*0.5
            data.wrench.force.z = self.desired_velocity[2]*(300.0/0.5)*0.5        
            data.wrench.torque.x = self.desired_velocity[3]
            data.wrench.torque.y = self.desired_velocity[4]
            data.wrench.torque.z = self.desired_velocity[5]*(105.0/0.3)*0.5
            
            data.disable_axis.x = self.resp.disable_axis.x
            data.disable_axis.y = self.resp.disable_axis.y
            data.disable_axis.z = self.resp.disable_axis.z
            data.disable_axis.roll = self.resp.disable_axis.roll
            data.disable_axis.pitch = self.resp.disable_axis.pitch
            data.disable_axis.yaw = self.resp.disable_axis.yaw
            
            self.pub_tau.publish(data)


if __name__ == '__main__':
    try:
        rospy.init_node('velocity_controller')
        velocity_controller = VelocityController(rospy.get_name())
        rospy.spin()

    except rospy.ROSInterruptException: pass
