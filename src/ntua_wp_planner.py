#!/usr/bin/env python

# ROS imports
import roslib 
import rospy
roslib.load_manifest('auv_msgs')
# Msgs imports
from auv_msgs.msg import *
from geometry_msgs.msg import WrenchStamped
from nav_msgs.msg import Odometry
from std_srvs.srv import Empty, EmptyResponse, EmptyRequest
from std_msgs.msg import Float32

# Python imports
from numpy import *
import sys

class g500Planner:
    def __init__(self, name):
       
        self.name = name
        #Get Initial Time
	self.t0 = rospy.Time.now().to_sec()
	#Initialize WP counter
	self.counter = 0.0 

	#Initilize Desired States
	self.x_des = float (sys.argv[1])
	self.y_des = float (sys.argv[2])
	self.z_des = float (sys.argv[3])
	self.theta_des = (float (sys.argv[4]))*pi/180.0
	self.psi_des = (float (sys.argv[5]))*pi/180.0


        #Initilize States
	self.x = 0.
	self.y = 0.
	self.z = 0.
        self.roll = 0.
	self.pitch = 0.
	self.yaw = 0.
        self.u = 0.
        self.v = 0.
        self.w = 0.
        self.p = 0.
        self.q = 0.
        self.r = 0.
	self.t_nav = 0.
        

	#Create Publisher
	self.pub_msg = rospy.Publisher("/ntua_wp_planner/world_waypoint_req", WorldWaypointReq)

        # Create Subscriber
        rospy.Subscriber("/cola2_navigation/nav_sts", NavSts, self.updateNavSts)

    def updateNavSts(self, nav_sts):
	self.x = nav_sts.position.north
	self.y = nav_sts.position.east
	self.z = nav_sts.position.depth
        self.roll = nav_sts.orientation.roll
	self.pitch = -nav_sts.orientation.pitch
	self.yaw = nav_sts.orientation.yaw
        self.u = nav_sts.body_velocity.x
        self.v = nav_sts.body_velocity.y
        self.w = nav_sts.body_velocity.z
        self.p = nav_sts.orientation_rate.roll
        self.q = -nav_sts.orientation_rate.pitch
        self.r = nav_sts.orientation_rate.yaw
	self.t_nav = nav_sts.header.stamp.to_sec()
	
	
        
    def planning (self,data,t):


	#Define Desired SetPoints (5 WPs in 3D space)
	
	x_des = self.x_des
	y_des = self.y_des
	z_des = self.z_des

	psi_des = self.psi_des
	theta_des = self.theta_des

        data.header.stamp = rospy.Time.now()
    	data.header.frame_id = ""
	data.goal.priority = 0
	data.altitude_mode = False   

    	data.position.north =  x_des
	data.position.east =  y_des
	data.position.depth =  z_des
    	 
	data.altitude =  0.0

	data.orientation.roll = 0.0
	data.orientation.pitch = theta_des
	data.orientation.yaw = psi_des

	data.disable_axis.x = False
	data.disable_axis.y = False
	data.disable_axis.z = False
	data.disable_axis.roll = True
	data.disable_axis.pitch = False
	data.disable_axis.yaw = False

	data.position_tolerance.x = 0.0
	data.position_tolerance.y = 0.0
	data.position_tolerance.z = 0.0
	data.orientation_tolerance.roll = 0.0
	data.orientation_tolerance.pitch = 0.0
	data.orientation_tolerance.yaw = 0.0

	print "current:", " ", "North: %.2f" % self.x, "East: %.2f " % self.y, "Depth: %.2f " % self.z, "Pitch: %.2f" % self.pitch, "Yaw: %.2f " % self.yaw, "\n"
	#print "current:", " ", "North:", self.x, " ",  "East:", " ", self.y, " ", "Depth:", self.z, " ", "Pitch:", self.pitch, " ","Yaw:", self.yaw, "\n"
	#print "desired:", " ", "North:", x_des, " ",  "East:", " ", y_des, " ", "Depth:", z_des, " ", "Pitch:", theta_des, " ","Yaw:", psi_des, "\n"
	print "desired:", " ", "North: %.2f" % x_des, "East: %.2f " % y_des, "Depth: %.2f " % z_des, "Pitch: %.2f" % theta_des, "Yaw: %.2f " % psi_des, "\n"
	self.pub_msg.publish(data)
              
        
	
if __name__ == '__main__':
    try:
        rospy.init_node('g500Planner')
	rospy.loginfo('g500Planner Node is up')
        dt = 0.1
	
        g500PlannerObj = g500Planner(rospy.get_name())
        WP_msg = WorldWaypointReq()
	
	while not rospy.is_shutdown():
              t =  rospy.Time.now().to_sec() - g500PlannerObj.t0 
	      g500PlannerObj.planning (WP_msg, t)
              rospy.sleep(dt)
	rospy.spin()
    except rospy.ROSInterruptException: pass 
