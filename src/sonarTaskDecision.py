#!/usr/bin/env python

# ROS imports
import roslib 
roslib.load_manifest('ntua_pandora')
roslib.load_manifest('udg_pandora')
import rospy

# Msgs imports
from auv_msgs.msg import *
from sensor_msgs.msg import JointState
from sensor_msgs.msg import Joy
from pose_ekf_slam.msg import Map

from std_msgs.msg import Bool
from std_srvs.srv import Empty, EmptyResponse
from geometry_msgs.msg import PoseStamped, Twist, WrenchStamped
from udg_pandora.msg import Point2D, Point2DMatched, BoxCorners

import tf
# Python imports
from numpy import *
from numpy.linalg import *
from math import *
import nlopt

# Custom imports
import cola2_lib
import cola2_ros_lib
import sys


from g500PoseControllerNTUA import *
from g500TeleoperationNTUA import *
from g500SonarPlanner import *
from WPTrackingController import *
from UVMSG500VelocityController import *

class sonarTaskDecision:
    def __init__(self, name):
        self.name = name
	#Get Initial Time
        self.t0 = rospy.Time.now().to_sec()  
   
        self.button1 = 0
        self.button2 = 0
        self.button3 = 0
        self.button4 = 0
        self.buttonR2 = 0

        self.tele_flag = False
        self.scan_flag = False
        self.circle_flag = False

	#self.pub_stop_circle = rospy.Publisher("/ntua_decision_making/stop_cirle", Bool)        

	#rospy.Subscriber("/ntua_decision_making/start_circle", Bool, self.decisionStartCircle)
        rospy.Subscriber("/joy", Joy, self.updateJoystick)

    def updateJoystick(self, msgJOY):
        self.button1 = msgJOY.buttons[0]
        self.button2 = msgJOY.buttons[1]
        self.button3 = msgJOY.buttons[2]
        self.button4 = msgJOY.buttons[3]
        self.buttonR2 = msgJOY.buttons[7]

    #def decisionStartCircle(self, msg):
        #self.circle_start = msg.data

if __name__ == '__main__':
    try:
        rospy.init_node('sonarTaskDecision')
        sonarDesicionObj = sonarTaskDecision(rospy.get_name())
	g500SonarPlannerObj = g500SonarPlanner(rospy.get_name())
        teleObj = g500Teleoperation(rospy.get_name())
        UVMSVelObj = UVMSVelocityController(rospy.get_name())
        poseControlObject = G500WPTrackingController(rospy.get_name())
	
        #UVMS Velocity Controller
	wrench_msg = BodyForceReq()
	#Vehicle Planner
        WP_msg = WorldWaypointReq()

        

	while not rospy.is_shutdown():
                t =  rospy.Time.now().to_sec() - sonarDesicionObj.t0 
                #Check which button is pressed
                if sonarDesicionObj.button1 == 1:
                   sonarDesicionObj.tele_flag = True
                   sonarDesicionObj.scan_flag = False
                   sonarDesicionObj.circle_flag = False
                 
                if sonarDesicionObj.button2 == 1:
                   sonarDesicionObj.tele_flag = False
                   sonarDesicionObj.scan_flag = True

                if sonarDesicionObj.button3 == 1:
                   sonarDesicionObj.tele_flag = False
                   sonarDesicionObj.circle_flag = True

                if sonarDesicionObj.button4 == 1:
                   sonarDesicionObj.tele_flag = False
                   sonarDesicionObj.circle_flag = False
                  
		if sonarDesicionObj.buttonR2 == 1:
                   sonarDesicionObj.tele_flag = False
                   sonarDesicionObj.scan_flag = False
                   sonarDesicionObj.circle_flag = False
                   
                   
	        if sonarDesicionObj.tele_flag == True:
                   rospy.sleep(0.05)
                   print "NOW IN TELEOPERATION MODE..."
                   teleObj.iterate()
                   rospy.sleep(0.05)  

		elif sonarDesicionObj.scan_flag == True:
	           rospy.sleep(0.05)
                   print "NOW IN SCANNING MODE..."
                   g500SonarPlannerObj.calcChain()
                   g500SonarPlannerObj.planning (WP_msg)
                   
                   if sonarDesicionObj.circle_flag == True:
                      UVMSVelObj.vehicleVelCtrl(wrench_msg, array([0,0,0,0,0,0.2])) 
                      print "PERFORM SURVEILLANCE FOR MISSING WPS"
                      #g500poseControllerObj.iterate()
		      rospy.sleep(0.05) 
                   else:
                      poseControlObject.iterate()
                      
                      
                else:
                   print "VEHICLE IS HOLDING POSITION..."
                   rospy.sleep(0.05)
		   UVMSVelObj.vehicleVelCtrl(wrench_msg, array([0,0,0,0,0,0])) 
                   rospy.sleep(0.05)
                #sonarDesicionObj.pub_stop_circle.publish(Bool(sonarDesicionObj.circle_stop))
        rospy.spin()

    except rospy.ROSInterruptException: pass
