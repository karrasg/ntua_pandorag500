#!/usr/bin/env python

# ROS imports
import roslib 
roslib.load_manifest('ntua_pandora')
roslib.load_manifest('udg_pandora')
import rospy

# Msgs imports
from auv_msgs.msg import *
from sensor_msgs.msg import JointState
from sensor_msgs.msg import Joy
from pose_ekf_slam.msg import Map

from std_msgs.msg import Bool
from std_srvs.srv import Empty, EmptyResponse
from geometry_msgs.msg import PoseStamped, Twist, WrenchStamped
from udg_pandora.msg import Point2D, Point2DMatched, BoxCorners

import tf
# Python imports
from numpy import *
from numpy.linalg import *
from math import *
import nlopt

# Custom imports
import cola2_lib
import cola2_ros_lib
import sys

from armJointControllerNTUA import *
from UVMSPanelPlanner import *
from WPTrackingControllerDemoRM import *
from g500TeleoperationNTUA import *

class valveTaskDecision:
    def __init__(self, name):
        self.name = name
	#Get Initial Time
        self.t0 = rospy.Time.now().to_sec()  
   
        
if __name__ == '__main__':
    try:
        rospy.init_node('valveTaskDecision')
        valveDesicionObj = valveTaskDecision(rospy.get_name())
	armJointControlObj = armJointControllerNTUA(rospy.get_name())
	UVMSPlannerObj = UVMSPlanner(rospy.get_name())
        controlObject = G500WPTrackingController(rospy.get_name())
	teleObj = g500Teleoperation(rospy.get_name())

	#For Reaching phase
        joint_msg = JointState() 
        joint_msg_reach = JointState()
        WP_msg_reach = WorldWaypointReq()

        #For IBVS Phase
	velCmds = Twist()    
 	
        #UVMS Velocity Controller
	jointCmd_msg = JointState()
	wrench_msg = BodyForceReq()

	while not rospy.is_shutdown():
                t =  rospy.Time.now().to_sec() - valveDesicionObj.t0 
                #Check which button is pressed
	        rospy.sleep(0.05)
                #print "NOW IN REACHING MODE..."
                UVMSPlannerObj.armPlanningOpt()
		UVMSPlannerObj.armPlanning (joint_msg_reach)
		UVMSPlannerObj.vehiclePlanning (WP_msg_reach, t)
		armJointControlObj.control(joint_msg)
                controlObject.iterate()
                #teleObj.iterate()
                rospy.sleep(0.05)
        rospy.spin()

    except rospy.ROSInterruptException: pass
