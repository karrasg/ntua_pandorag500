#!/usr/bin/env python

# ROS imports
import roslib 
roslib.load_manifest('ntua_pandora')
roslib.load_manifest('udg_pandora')
import rospy

# Msgs imports
from auv_msgs.msg import *
from sensor_msgs.msg import JointState
from sensor_msgs.msg import Joy
from pose_ekf_slam.msg import Map

from std_msgs.msg import Bool
from std_srvs.srv import Empty, EmptyResponse
from geometry_msgs.msg import PoseStamped, Twist, WrenchStamped
from udg_pandora.msg import Point2D, Point2DMatched, BoxCorners

import tf
# Python imports
from numpy import *
from numpy.linalg import *
from math import *
import nlopt

# Custom imports
import cola2_lib
import cola2_ros_lib
import sys

from armJointControllerNTUA import *
from g500PoseControllerNTUA import *
from g500TeleoperationNTUA import *
from IBVSControllerSim import *
from UVMSG500VelocityController import *
from UVMSPanelPlanner import *

class valveTaskDecision:
    def __init__(self, name):
        self.name = name
	#Get Initial Time
        self.t0 = rospy.Time.now().to_sec()  
   
        self.button1 = 0
        self.button2 = 0
        self.button3 = 0
        self.button4 = 0
        self.buttonR2 = 0

        self.tele_flag = False
        self.reach_flag = False
        self.ibvs_flag = False
        self.goturn_flag = False
        
	
	#rospy.Subscriber("/ntua_decision_making/panel_reach", Bool, self.decisionPanelReach)
        rospy.Subscriber("/joy", Joy, self.updateJoystick)

    #def decisionPanelReach(self, msg):
        #self.panelReach_flag = msg.data

    def updateJoystick(self, msgJOY):
        self.button1 = msgJOY.buttons[0]
        self.button2 = msgJOY.buttons[1]
        self.button3 = msgJOY.buttons[2]
        self.button4 = msgJOY.buttons[3]
        self.buttonR2 = msgJOY.buttons[7]

if __name__ == '__main__':
    try:
        rospy.init_node('valveTaskDecision')
        valveDesicionObj = valveTaskDecision(rospy.get_name())
	armJointControlObj = armJointControllerNTUA(rospy.get_name())
        g500poseControllerObj = g500PoseController(rospy.get_name())
        teleObj = g500Teleoperation(rospy.get_name())
        IBVSControllerObj = IBVSController(rospy.get_name())
        UVMSVelObj = UVMSVelocityController(rospy.get_name())
	UVMSPlannerObj = UVMSPlanner(rospy.get_name())

	#For Reaching phase
        joint_msg = JointState() 
        joint_msg_reach = JointState()
        WP_msg_reach = WorldWaypointReq()

        #For IBVS Phase
	velCmds = Twist()    
 	
        #UVMS Velocity Controller
	jointCmd_msg = JointState()
	wrench_msg = BodyForceReq()

	while not rospy.is_shutdown():

                t =  rospy.Time.now().to_sec() - valveDesicionObj.t0 
                #Check which button is pressed
                if valveDesicionObj.button1 == 1:
                   valveDesicionObj.tele_flag = True
                   valveDesicionObj.reach_flag = False
                   valveDesicionObj.ibvs_flag = False
                   valveDesicionObj.goturn_flag = False

                if valveDesicionObj.button2 == 1:
                   valveDesicionObj.tele_flag = False
                   valveDesicionObj.reach_flag = True
                   valveDesicionObj.ibvs_flag = False
                   valveDesicionObj.goturn_flag = False

                if valveDesicionObj.button3 == 1:
                   valveDesicionObj.tele_flag = False
                   valveDesicionObj.reach_flag = False
                   valveDesicionObj.ibvs_flag = True
                   valveDesicionObj.goturn_flag = False

                if valveDesicionObj.button4 == 1:
                   valveDesicionObj.tele_flag = False
                   valveDesicionObj.reach_flag = False
                   valveDesicionObj.ibvs_flag = False
                   valveDesicionObj.goturn_flag = True
                
		if valveDesicionObj.buttonR2 == 1:
                   valveDesicionObj.tele_flag = False
                   valveDesicionObj.reach_flag = False
                   valveDesicionObj.ibvs_flag = False
                   valveDesicionObj.goturn_flag = False
       

	        if valveDesicionObj.tele_flag == True:
                   rospy.sleep(0.05)
                   print "NOW IN TELEOPERATION MODE..."
                   teleObj.iterate()
                   rospy.sleep(0.05)  
		elif valveDesicionObj.reach_flag == True:
	           rospy.sleep(0.05)
                   print "NOW IN REACHING MODE..."
                   UVMSPlannerObj.armPlanningOpt()
		   UVMSPlannerObj.armPlanning (joint_msg_reach )
		   UVMSPlannerObj.vehiclePlanning (WP_msg_reach, t)
		   armJointControlObj.control(joint_msg)
                   g500poseControllerObj.iterate()
		   rospy.sleep(0.05)
                elif valveDesicionObj.ibvs_flag == True:
		   rospy.sleep(0.05)
		   print "NOW IN VISUAL SERVOING MODE..."
		   IBVSControllerObj.getEEtoWorldPose()
		   IBVSControllerObj.control(velCmds)
                   UVMSVelObj.getEEtoWorldPose()
		   Jw = UVMSVelObj.calcUVMSJac()
		   qUVMS = UVMSVelObj.UVMSVelocityScheme(Jw)
		   UVMSVelObj.armVelcontrol(jointCmd_msg,qUVMS[6:10])
	           UVMSVelObj.vehicleVelCtrl(wrench_msg, qUVMS[0:6])
		   rospy.sleep(0.05)  
                elif valveDesicionObj.goturn_flag == True:  
                   print "NOW HEADING TOWARDS THE VALVE..."
                   rospy.sleep(0.05)  
                   UVMSVelObj.armVelcontrol(jointCmd_msg,array([0.00,0.00,0.00,0.00]))
		   UVMSVelObj.vehicleVelCtrl(wrench_msg, array([0.01,0,0,0,0,0]))
		   rospy.sleep(0.05) 
                else:
                   print "UVMS HOLDING POSITION..."
                   rospy.sleep(0.05)
                   UVMSVelObj.armVelcontrol(jointCmd_msg,array([0.00,0.00,0.00,0.00]))
		   UVMSVelObj.vehicleVelCtrl(wrench_msg, array([0,0,0,0,0,0])) 
                   rospy.sleep(0.05)
        rospy.spin()

    except rospy.ROSInterruptException: pass
