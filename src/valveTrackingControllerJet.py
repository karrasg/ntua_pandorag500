#!/usr/bin/env python

# ROS imports
import roslib
roslib.load_manifest('cola2_control')
roslib.load_manifest('pose_ekf_slam')
import rospy

# Msgs imports
from auv_msgs.msg import BodyForceReq, NavSts, GoalDescriptor
from std_srvs.srv import Empty, EmptyResponse
from pose_ekf_slam.msg import Map

# Python imports
from numpy import array, zeros, dot, deg2rad
from numpy.linalg import inv
from math import cos, sin, tan, sqrt, exp, pi
import tf
from std_msgs.msg import Float32

class valveTrackingController:
    def __init__(self, name):
        """ Controls the velocity and pose of an AUV  """
        self.name = name
        self.panel_angle = 0.80

        # TODO: NARCIS Stuff to reduce controllers power
        self.power_reduction = 0.2
        self.x_offset = -1.28
        """-1.45"""
        self.y_offset = -0.93
        """-1.17"""
        self.z_offset = -0.1
        """-0.4"""
        self.desired_theta = deg2rad(0.0) # Useless ???

        self.A = 0.50
        self.T = 80.0
        self.ticTime = 0.0
        self.psi_des_stable = 0.0
        self.x_lan_des = 0.0
	self.y_lan_des = 0.0
        self.z_lan_des = 0.0
        
        self.tic = 0
        # Load parameters
        self.enable = False
        self.counter = 0
        self.dh = 0.35
        self.counter_time = 0
        self.landmark_init = False
        
        #Initilize Time
        self.t0 = rospy.Time.now().to_sec()
        self.t1 = 0.0
        
        # Input data
        self.xp = zeros(12)

        #Initilize Landmark Position and Seq
        self.x_lan   =  0.0
        self.y_lan   =  0.0
        self.z_lan   =  0.0
        self.seq_lan =  -1
        self.seq_lan_prev =  -1

        #Initiliaze Prescribed Performance Controller Values
        self.r0_x=5.
        self.r0_y=5.
        self.r0_z=5.
        self.r0_theta=5.
        self.r0_psi=5.
        self.r0_u=1.
        self.r0_v=1.
        self.r0_w=1.
        self.r0_q=5.
        self.r0_r=1.

        self.k_x=15.0#10
        self.k_y=17.5#10
        self.k_z=15.0
        self.k_theta=1.
        self.k_psi=10.0#20
        self.k_u=200.#180
        self.k_v=200.#180
        self.k_w=220.
        self.k_q=40.
        self.k_r=100.0
        self.k_psi=self.k_psi/self.k_r
        self.k_x=self.k_x/self.k_u
        self.k_y=self.k_y/self.k_v
        self.k_z=self.k_z/self.k_w

        self.rl_x=2.5
        self.rl_y=2.5
        self.rl_z=2.5
        self.rl_theta=2.5
        self.rl_psi=2.5
        self.rf_x=0.1
        self.rf_y=0.1
        self.rf_z=0.1
        self.rf_theta=0.02
        self.rf_psi=0.1

        self.rl_u=1.0
        self.rl_v=1.0
        self.rl_w=1.0
        self.rl_q=5.5
        self.rl_r=5.5
        self.rf_u=1.0
        self.rf_v=1.0
        self.rf_w=1.0
        self.rf_q=0.5
        self.rf_r=1.0

        # Create publisher
        self.pub_tau = rospy.Publisher("/cola2_control/body_force_req", BodyForceReq)
        self.pub_angle = rospy.Publisher("/stereo_data/angle", Float32)
        # Create Subscriber
        rospy.Subscriber("/cola2_navigation/nav_sts", NavSts, self.updateNavSts)
        rospy.Subscriber("/pose_ekf_slam/map", Map, self.updateLandmarkPose)

        # Create timer to update offset
        rospy.timer.Timer(rospy.Duration(0.25), self.update_offset)
        self.panel_offset = lambda : 0
        self.panel_offset.rot = None
        self.panel_offset.trans = None
        # TF listener to compute the transforms
        self.panel_offset.tflistener = tf.TransformListener()

        #Create services
        self.enable_srv = rospy.Service('/ntua_control_g500/enable_valve_controller', Empty, self.enableSrv)
        self.disable_srv = rospy.Service('/ntua_control_g500/disable_valve_controller', Empty, self.disableSrv)


    def enableSrv(self, req):
        self.enable = True
        rospy.loginfo('%s Enabled', self.name)
        return EmptyResponse()


    def disableSrv(self, req):
        self.enable = False
        rospy.loginfo('%s Disabled', self.name)
        return EmptyResponse()

    def controlScheme(self,t):

        U = zeros(5)

       #Filling up states
        x = self.xp[0]
        y = self.xp[1]
        z = self.xp[2]
        phi = self.xp[3]
        theta = self.xp[4]
        psi = self.xp[5]
        u = self.xp[6]
        v = self.xp[7]
        w = self.xp[8]
        # p = self.xp[9]
        q = self.xp[10]
        r = self.xp[11]
    
        #Calculating Jacobians
        J1 = [cos(theta)*cos(psi),  cos(psi)*sin(theta)*sin(phi)-sin(psi)*cos(phi),  sin(psi)*sin(phi)+cos(psi)*cos(phi)*sin(theta),
              cos(theta)*sin(psi),  cos(psi)*cos(phi)+sin(phi)*sin(theta)*sin(psi),  sin(psi)*sin(theta)*cos(phi)-cos(psi)*sin(phi),
              -sin(theta),          cos(theta)*sin(phi),                             cos(theta)*cos(phi)]

        J2 = [1.0,  sin(phi)*tan(theta),  cos(phi)*tan(theta),
              0.0,  cos(phi),             -sin(phi),
              0.0,  sin(phi)/cos(theta),  cos(phi)/cos(theta)]

        J1 = array(J1).reshape(3,3)
        J2 = array(J2).reshape(3,3)
    
        J1_INV = inv(J1)
        J2_INV = inv(J2)
    
        # print "t:", t

        #Define Desired SetPoints
        x_des = self.x_lan
        y_des = self.y_lan
        z_des = self.z_lan
        theta_des = self.desired_theta


        #Meandrus
        #x_des = self.x_lan + self.A*sin(self.panel_angle)
        #y_des = self.y_lan - self.A*cos(self.panel_angle)
        #z_des = self.z_lan + 1.25*self.A

        #if self.tic == 1:
           #x_des = self.x_lan_des + 1.0*self.A*cos(2.0*pi*(rospy.Time.now().to_sec() - self.ticTime)/self.T)*sin(self.panel_angle)
           #y_des = self.y_lan_des - 1.0*self.A*cos(2.0*pi*(rospy.Time.now().to_sec() - self.ticTime)/self.T)*cos(self.panel_angle)
           #z_des = self.z_lan_des + 1.25*self.A + 1.0*0.4*((rospy.Time.now().to_sec() - self.ticTime)/self.T)
           
    
        #Calculating errors

        self.x_offset = -2.0*cos(self.panel_angle)
	self.y_offset = -2.0*sin(self.panel_angle)

        e_x = x-x_des - self.x_offset
        e_y = y-y_des - self.y_offset
        e_z = z-z_des - self.z_offset
        e_theta = theta-theta_des
        e_psi = ((x_des-x)/(sqrt((x_des-x)**2+(y_des-y)**2)))*sin(psi) - ((y_des-y)/(sqrt((x_des-x)**2+(y_des-y)**2)))*cos(psi)
        #e_psi = sin(psi-(self.panel_angle+pi/2.0))
        
        if sqrt(e_x**2+e_y**2)<0.05 and self.tic == 0:
           self.tic = 1
	   #self.ticTime = rospy.Time.now().to_sec()
           self.psi_des_stable = psi
           #self.x_lan_des = self.x_lan
	   #self.y_lan_des = self.y_lan
           #self.z_lan_des = self.z_lan

        if self.tic==1:
           e_psi = psi-self.psi_des_stable 
           #e_psi = sin(psi-0.75)    

        r_x = (self.r0_x-self.rf_x)*exp(-self.rl_x*t)+self.rf_x
        r_y = (self.r0_y-self.rf_y)*exp(-self.rl_y*t)+self.rf_y
        r_z = (self.r0_z-self.rf_z)*exp(-self.rl_z*t)+self.rf_z
        r_theta = (self.r0_theta-self.rf_theta)*exp(-self.rl_theta*t)+self.rf_theta
        r_psi = (self.r0_psi-self.rf_psi)*exp(-self.rl_psi*t)+self.rf_psi
    
        temp = array([[-self.k_x*e_x/r_x], [-self.k_y*e_y/r_y], [-self.k_z*e_z/r_z]])
        temp = array(temp).reshape(3,1)
        temp = dot(J1_INV,temp)
    
        u_des = temp[0]
        v_des = temp[1]
        w_des = temp[2]
    
        temp = array([[-self.k_theta*e_theta/r_theta], [-self.k_psi*e_psi/r_psi]])
        temp = array(temp).reshape(2,1)
        temp = dot(J2_INV[1:3,1:3],temp)
    
        q_des = temp[0]
        r_des = temp[1]
    
        e_u = u-u_des
        e_v = v-v_des
        e_w = w-w_des
        e_q = q-q_des
        e_r = r-r_des
    
        r_u = (self.r0_u-self.rf_u)*exp(-self.rl_u*t)+self.rf_u
        r_v = (self.r0_v-self.rf_v)*exp(-self.rl_v*t)+self.rf_v
        r_w = (self.r0_w-self.rf_w)*exp(-self.rl_w*t)+self.rf_w
        r_q = (self.r0_q-self.rf_q)*exp(-self.rl_q*t)+self.rf_q
        r_r = (self.r0_r-self.rf_r)*exp(-self.rl_r*t)+self.rf_r
    
        X = -self.k_u*e_u/r_u
        Y = -self.k_v*e_v/r_v
        Z = -self.k_w*e_w/r_w
        M = -self.k_q*e_q/r_q
        N = -self.k_r*e_r/r_r
        N = - 150.0*e_psi - 80.0*r
    
        '''X=0.0
        Y=0.0
        N=-self.k_r*r-self.k_psi*self.k_r*psi'''
        U[0] = X
        U[1] = Y
        U[2] = Z
        U[3] = M
        U[4] = N
    
        print "current error:", str(e_x/r_x), str(e_y/r_y), str(e_z/r_z), str(e_theta/r_theta), str(e_psi/r_psi)
        print "tic:", self.tic
        return U


    def updateNavSts(self, nav_sts):
        self.xp[0] = nav_sts.position.north
        self.xp[1] = nav_sts.position.east
        self.xp[2] = nav_sts.position.depth
        self.xp[3] = nav_sts.orientation.roll
        self.xp[4] = nav_sts.orientation.pitch
        self.xp[5] = nav_sts.orientation.yaw
        self.xp[6] = nav_sts.body_velocity.x
        self.xp[7] = nav_sts.body_velocity.y
        self.xp[8] = nav_sts.body_velocity.z
        self.xp[9] = nav_sts.orientation_rate.roll
        self.xp[10] = nav_sts.orientation_rate.pitch
        self.xp[11] = nav_sts.orientation_rate.yaw


    """def updateLandmarkPose(self, landm_msg):
        self.landmark_init = True
        self.x_lan   =  landm_msg.landmark[0].position.x
        self.y_lan   =  landm_msg.landmark[0].position.y
        self.z_lan   =  landm_msg.landmark[0].position.z
        self.seq_lan =  landm_msg.header.seq"""

    #When Multiple Landmarks Detected
    def updateLandmarkPose(self, landm_msg):
        self.landmark_init = True
        num_lan = len(landm_msg.landmark)
        
        for i in range(0,num_lan):
          if landm_msg.landmark[i].landmark_id == '/pose_ekf_slam/landmark_update/panel_centre':
             self.x_lan = landm_msg.landmark[i].pose.pose.position.x
             self.y_lan = landm_msg.landmark[i].pose.pose.position.y
             self.z_lan = landm_msg.landmark[i].pose.pose.position.z
             self.seq_lan = landm_msg.header.seq

    def update_offset(self, event):
        panel_offset = self.panel_offset
        # Check if transform is available
        try:
            #panel_offset.tflistener.waitForTransform("world", "panel_centre", rospy.Time.now(), rospy.Duration(0.2))
            panel_offset.trans, panel_offset.rot = panel_offset.tflistener.lookupTransform("world", "panel_centre", rospy.Time())
        except tf.Exception:
            #self.panel_angle = 0.80
            print "No transform available"
        else:
            print "Rotation (x, y, z, w): ", panel_offset.rot
            angle = tf.transformations.euler_from_quaternion(panel_offset.rot)
            print "Angle RPY: ", angle
            self.panel_angle = angle[2]-pi/2.0

    def iterate(self):
        # Main loop
        t = rospy.Time.now().to_sec() - self.t0
        U = zeros(5)

        if self.enable == False:
            print "Valve Controller is down", "\n"
            self.counter_time = 0
            self.tic = 0
        elif self.landmark_init:
            print "Valve Controller is up: VEHICLE IN CLOSED LOOP MODE", "\n"
            if self.counter_time == 0:    
                self.t1 = rospy.Time.now().to_sec() - self.t0
                self.counter_time = 1

                #Filling up states
                x = self.xp[0]
                y = self.xp[1]
                z = self.xp[2]
                phi = self.xp[3]
                theta = self.xp[4]
                psi = self.xp[5]
                u = self.xp[6]
                v = self.xp[7]
                w = self.xp[8]
                # p = self.xp[9]
                q = self.xp[10]
                r = self.xp[11]

                #Calculating Jacobians
                J1 = [cos(theta)*cos(psi),  cos(psi)*sin(theta)*sin(phi)-sin(psi)*cos(phi),  sin(psi)*sin(phi)+cos(psi)*cos(phi)*sin(theta),
                     cos(theta)*sin(psi),  cos(psi)*cos(phi)+sin(phi)*sin(theta)*sin(psi),  sin(psi)*sin(theta)*cos(phi)-cos(psi)*sin(phi),
                     -sin(theta),          cos(theta)*sin(phi),                             cos(theta)*cos(phi)]

                J2 = [1.0,  sin(phi)*tan(theta),  cos(phi)*tan(theta),
                      0.0,  cos(phi),             -sin(phi),
                      0.0,  sin(phi)/cos(theta),  cos(phi)/cos(theta)]

                J1 = array(J1).reshape(3,3)
                J2 = array(J2).reshape(3,3)

                J1_INV = inv(J1)
                J2_INV = inv(J2)

                #Define Desired SetPoints
                x_des = self.x_lan
                y_des = self.y_lan
                z_des = self.z_lan

                theta_des = self.desired_theta

                #Meandrus
                #x_des = self.x_lan + self.A*sin(self.panel_angle)
                #y_des = self.y_lan - self.A*cos(self.panel_angle)
                #z_des = self.z_lan - self.A

                # Calculating errors
                e_x = x-x_des - self.x_offset
                e_y = y-y_des - self.y_offset
                e_z = z-z_des - self.z_offset
                e_theta = theta-theta_des
                e_psi = ((x_des-x)/(sqrt((x_des-x)**2+(y_des-y)**2)))*sin(psi) - ((y_des-y)/(sqrt((x_des-x)**2+(y_des-y)**2)))*cos(psi)
                #e_psi = sin(psi-(self.panel_angle+pi/2.0))

                self.r0_x=2.0*abs(e_x) + self.rf_x
                self.r0_y=2.0*abs(e_y) + self.rf_y
                self.r0_z=2.0*abs(e_z) + self.rf_z
                self.r0_theta=2.0*abs(e_theta) + 0.5
                self.r0_psi=2.0*abs(e_psi)+self.rf_psi
  
                r_x = (self.r0_x-self.rf_x)*exp(-self.rl_x*t)+self.rf_x
                r_y = (self.r0_y-self.rf_y)*exp(-self.rl_y*t)+self.rf_y
                r_z = (self.r0_z-self.rf_z)*exp(-self.rl_z*t)+self.rf_z
                r_theta = (self.r0_theta-self.rf_theta)*exp(-self.rl_theta*t)+self.rf_theta
                r_psi = (self.r0_psi-self.rf_psi)*exp(-self.rl_psi*t)+self.rf_psi

                temp = array([[-self.k_x*e_x/r_x], [-self.k_y*e_y/r_y], [-self.k_z*e_z/r_z]])
                temp = array(temp).reshape(3,1)
                temp = dot(J1_INV,temp)

                u_des = temp[0]
                v_des = temp[1]
                w_des = temp[2]
  
                temp = array([[-self.k_theta*e_theta/r_theta], [-self.k_psi*e_psi/r_psi]])
                temp = array(temp).reshape(2,1)
                temp = dot(J2_INV[1:3,1:3],temp)

                q_des = temp[0]
                r_des = temp[1]

                e_u = u-u_des
                e_v = v-v_des
                e_w = w-w_des
                e_q = q-q_des
                e_r = r-r_des

                self.r0_u=1.0
                self.r0_v=1.0
                self.r0_w=1.0
                self.r0_q=2.0*abs(e_q) + 1.5
                self.r0_r=1.0

            U = self.controlScheme(t-self.t1)
         
            data = BodyForceReq()
            data.header.stamp = rospy.Time.now()
            data.header.frame_id = "girona500"
            data.goal.requester = "valve_tracking_controller"
            data.goal.id = 0
            data.goal.priority = GoalDescriptor.PRIORITY_NORMAL
            
            data.wrench.force.x = U[0] * self.power_reduction*1.0
            data.wrench.force.y = U[1] * (self.power_reduction*1.0)
            data.wrench.force.z = U[2] * (self.power_reduction*1.0)+ 12.0 #13.4
            data.wrench.torque.x = 0.
            data.wrench.torque.y = U[3] * self.power_reduction
            data.wrench.torque.z = U[4] * (self.power_reduction)
            data.disable_axis.x = False
            data.disable_axis.y = False
            data.disable_axis.z = False
            data.disable_axis.roll = True
            data.disable_axis.pitch = True
            data.disable_axis.yaw = False

            self.pub_tau.publish(data)
            self.pub_angle.publish(self.panel_angle)
        else:
            print 'Landmark not yet initialized'

if __name__ == '__main__':
    try:
        rospy.init_node('valveTrackingController')
        controller = valveTrackingController(rospy.get_name())

        while not rospy.is_shutdown():
            rospy.sleep(0.05)
            controller.iterate()
            rospy.sleep(0.05)
        
        rospy.spin()

    except rospy.ROSInterruptException: pass
