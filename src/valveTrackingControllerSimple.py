#!/usr/bin/env python


# ROS imports
import roslib 
roslib.load_manifest('cola2_control')
import rospy

# Msgs imports
from auv_msgs.msg import BodyForceReq, NavSts, GoalDescriptor
from std_srvs.srv import Empty, EmptyResponse

# Python imports
from numpy import array, zeros, dot
from numpy.linalg import inv
from math import cos, sin, tan, sqrt, exp

class valveTrackingControllerSimple:
    def __init__(self, name):
        """ Controls the velocity and pose of an AUV  """
        self.name = name
 	self.power_reduce = 0.1
 
        # Load parameters
        self.enable = False
        self.counter = 0
        self.dh = 0.35
        self.counter_time = 0
                    
        #Initilize Time
        self.t0 = rospy.Time.now().to_sec()
        self.t1 = 0.0

        # Input data 
        self.xp = zeros(12)
        
        #Distance From Target 
        self.x_offset = -0.5
	self.y_offset = -2.0
        #Initiliaze Prescribed Performance Controller Values
        self.r0_x=5.
        self.r0_y=5.
        self.r0_z=5.
        self.r0_theta=5.
        self.r0_psi=5.
        self.r0_u=5.
        self.r0_v=5.
        self.r0_w=5.
        self.r0_q=5.
        self.r0_r=5.

        self.k_x=0.8/5.0
        self.k_y=0.5/5.0
        self.k_z=0.7/5.0
        self.k_theta=1.
        self.k_psi=0.75/5.0
        self.k_u=40.
        self.k_v=40.
        self.k_w=40.
        self.k_q=40.
        self.k_r=40.

        self.rl_x=2.5
        self.rl_y=2.5
        self.rl_z=2.5
        self.rl_theta=2.5
        self.rl_psi=2.5
        self.rf_x=0.1
        self.rf_y=0.1
        self.rf_z=0.1
        self.rf_theta=0.02
        self.rf_psi=0.01

        self.rl_u=5.5
        self.rl_v=5.5
        self.rl_w=5.5
        self.rl_q=5.5
        self.rl_r=5.5
        self.rf_u=0.5
        self.rf_v=0.5
        self.rf_w=0.5
        self.rf_q=0.5
        self.rf_r=0.5

        # Create publisher
        self.pub_tau = rospy.Publisher("/cola2_control/body_force_req", BodyForceReq)
        
        # Create Subscriber
        rospy.Subscriber("/cola2_navigation/nav_sts", NavSts, self.updateNavSts) 
    
        #Create services
        self.enable_srv = rospy.Service('/ntua_control_g500/enable_valve_controller_simple', Empty, self.enableSrv)
        self.disable_srv = rospy.Service('/ntua_control_g500/disable_valve_controller_simple', Empty, self.disableSrv)     


    def enableSrv(self, req):
        self.enable = True
        rospy.loginfo('%s Enabled', self.name)
        return EmptyResponse()


    def disableSrv(self, req):
        self.enable = False
        rospy.loginfo('%s Disabled', self.name)
        return EmptyResponse()


    def updateNavSts(self, nav_sts):
        self.xp[0] = nav_sts.position.north
        self.xp[1] = nav_sts.position.east
        self.xp[2] = nav_sts.position.depth
        self.xp[3] = nav_sts.orientation.roll
        self.xp[4] = nav_sts.orientation.pitch
        self.xp[5] = nav_sts.orientation.yaw
        self.xp[6] = nav_sts.body_velocity.x
        self.xp[7] = nav_sts.body_velocity.y
        self.xp[8] = nav_sts.body_velocity.z
        self.xp[9] = nav_sts.orientation_rate.roll
        self.xp[10] = nav_sts.orientation_rate.pitch
        self.xp[11] = nav_sts.orientation_rate.yaw


    def controlScheme(self,t):
        U = zeros(5)    

        #Filling up states
        x = self.xp[0]
        y = self.xp[1]
        z = self.xp[2]
        phi = self.xp[3]
        theta = self.xp[4]
        psi = self.xp[5]
        u = self.xp[6]
        v = self.xp[7]
        w = self.xp[8]
        # p = self.xp[9]
        q = self.xp[10]
        r = self.xp[11]
        
        #Calculating Jacobians
        J1 = [cos(theta)*cos(psi),      cos(psi)*sin(theta)*sin(phi)-sin(psi)*cos(phi),  sin(psi)*sin(phi)+cos(psi)*cos(phi)*sin(theta),
              cos(theta)*sin(psi),      cos(psi)*cos(phi)+sin(phi)*sin(theta)*sin(psi),  sin(psi)*sin(theta)*cos(phi)-cos(psi)*sin(phi),
              -sin(theta),              cos(theta)*sin(phi),                             cos(theta)*cos(phi)]

        J2 = [1.0,  sin(phi)*tan(theta),  cos(phi)*tan(theta),
              0.0,  cos(phi),             -sin(phi),
              0.0,  sin(phi)/cos(theta),  cos(phi)/cos(theta)]

        J1 = array(J1).reshape(3,3)
        J2 = array(J2).reshape(3,3)

        J1_INV = inv(J1)
        J2_INV = inv(J2)

        print "t:", t

        #Define Desired SetPoints
        x_des = 0.0
        y_des = 0.0
        z_des = 2.5
        theta_des = 0.

#        dx_des = 0.
#        dy_des = 0.
#        dz_des = 0.
#        dtheta_des = 0.
#        dpsi_des = 0.
#        d2x_des = 0.
#        d2y_des = 0.
#        d2z_des = 0.
#        d2theta_des = 0.
#        d2psi_des = 0.

        #Calculating errors
        e_x = x-x_des - self.x_offset
        e_y = y-y_des - self.y_offset
        e_z = z-z_des
        e_theta = theta-theta_des
        e_psi = ((x_des-x)/(sqrt((x_des-x)**2+(y_des-y)**2)))*sin(psi) - ((y_des-y)/(sqrt((x_des-x)**2+(y_des-y)**2)))*cos(psi)

        r_x = (self.r0_x-self.rf_x)*exp(-self.rl_x*t)+self.rf_x
        r_y = (self.r0_y-self.rf_y)*exp(-self.rl_y*t)+self.rf_y
        r_z = (self.r0_z-self.rf_z)*exp(-self.rl_z*t)+self.rf_z
        r_theta = (self.r0_theta-self.rf_theta)*exp(-self.rl_theta*t)+self.rf_theta
        r_psi = (self.r0_psi-self.rf_psi)*exp(-self.rl_psi*t)+self.rf_psi

        temp = array([[-self.k_x*e_x/r_x], [-self.k_y*e_y/r_y], [-self.k_z*e_z/r_z]])
        temp = array(temp).reshape(3,1)
        temp = dot(J1_INV,temp)

        u_des = temp[0]
        v_des = temp[1]
        w_des = temp[2]

        temp = array([[-self.k_theta*e_theta/r_theta], [-self.k_psi*e_psi/r_psi]])
        temp = array(temp).reshape(2,1)
        temp = dot(J2_INV[1:3,1:3],temp)    

        q_des = temp[0]
        r_des = temp[1]

        e_u = u-u_des
        e_v = v-v_des
        e_w = w-w_des
        e_q = q-q_des
        e_r = r-r_des

        r_u = (self.r0_u-self.rf_u)*exp(-self.rl_u*t)+self.rf_u
        r_v = (self.r0_v-self.rf_v)*exp(-self.rl_v*t)+self.rf_v
        r_w = (self.r0_w-self.rf_w)*exp(-self.rl_w*t)+self.rf_w
        r_q = (self.r0_q-self.rf_q)*exp(-self.rl_q*t)+self.rf_q
        r_r = (self.r0_r-self.rf_r)*exp(-self.rl_r*t)+self.rf_r

        X = -self.k_u*e_u/r_u
        Y = -self.k_v*e_v/r_v
        Z = -self.k_w*e_w/r_w
        M = -self.k_q*e_q/r_q
        N = -self.k_r*e_r/r_r
        
        U[0] = X
        U[1] = Y
        U[2] = Z
        U[3] = M
        U[4] = N

        print "current error:", str(e_x), str(e_y), str(e_z), str(e_theta), str(e_psi)    
        return U


    def iterate(self): 
        # Main loop
        t = rospy.Time.now().to_sec() - self.t0  
        U = zeros(5)
        if not self.enable:
            print "Valve Controller is down", "\n"
            self.counter_time = 0
        else:
            print "Valve Controller is up: VEHICLE IN CLOSED LOOP MODE", "\n"
            if self.counter_time == 0:
                self.t1 = rospy.Time.now().to_sec() - self.t0
                self.counter_time = 1

            #Filling up states
            x = self.xp[0]
            y = self.xp[1]
            z = self.xp[2]
            phi = self.xp[3]
            theta = self.xp[4]
            psi = self.xp[5]
            u = self.xp[6]
            v = self.xp[7]
            w = self.xp[8]
            # p = self.xp[9]
            q = self.xp[10]
            r = self.xp[11]

            #Calculating Jacobians
            J1 = [cos(theta)*cos(psi),   cos(psi)*sin(theta)*sin(phi)-sin(psi)*cos(phi),  sin(psi)*sin(phi)+cos(psi)*cos(phi)*sin(theta),
                 cos(theta)*sin(psi),   cos(psi)*cos(phi)+sin(phi)*sin(theta)*sin(psi),  sin(psi)*sin(theta)*cos(phi)-cos(psi)*sin(phi),
                   -sin(theta),            cos(theta)*sin(phi),                             cos(theta)*cos(phi)]
                
            J2 = [1.0,  sin(phi)*tan(theta),  cos(phi)*tan(theta),
                  0.0,  cos(phi),             -sin(phi),
                  0.0,  sin(phi)/cos(theta),  cos(phi)/cos(theta)]
            
            J1 = array(J1).reshape(3,3)
            J2 = array(J2).reshape(3,3)

            J1_INV = inv(J1)
            J2_INV = inv(J2)

            #Define Desired SetPoints
            x_des = 0.0
            y_des = 0.0
            z_des =  2.5
            theta_des = 0.

            #Calculating errors
            e_x = x - x_des - self.x_offset
            e_y = y - y_des - self.y_offset
            e_z = z - z_des
            e_theta = theta - theta_des
            e_psi = ((x_des-x)/(sqrt((x_des-x)**2+(y_des-y)**2)))*sin(psi) - ((y_des-y)/(sqrt((x_des-x)**2+(y_des-y)**2)))*cos(psi)

            self.r0_x=2.0*abs(e_x) + 0.5
            self.r0_y=2.0*abs(e_y) + 0.5
            self.r0_z=2.0*abs(e_z) + 0.5
            self.r0_theta=2.0*abs(e_theta) + 0.5
            self.r0_psi=1.0*abs(e_psi) + 0.5
            
            r_x = (self.r0_x-self.rf_x)*exp(-self.rl_x*t)+self.rf_x
            r_y = (self.r0_y-self.rf_y)*exp(-self.rl_y*t)+self.rf_y
            r_z = (self.r0_z-self.rf_z)*exp(-self.rl_z*t)+self.rf_z
            r_theta = (self.r0_theta-self.rf_theta)*exp(-self.rl_theta*t)+self.rf_theta
            r_psi = (self.r0_psi-self.rf_psi)*exp(-self.rl_psi*t)+self.rf_psi

            temp = array([[-self.k_x*e_x/r_x], [-self.k_y*e_y/r_y], [-self.k_z*e_z/r_z]])
            temp = array(temp).reshape(3,1)
            temp = dot(J1_INV,temp)

            u_des = temp[0]
            v_des = temp[1]
            w_des = temp[2]

            temp = array([[-self.k_theta*e_theta/r_theta], [-self.k_psi*e_psi/r_psi]])
            temp = array(temp).reshape(2,1)
            temp = dot(J2_INV[1:3,1:3],temp)    

            q_des = temp[0]
            r_des = temp[1]

            e_u = u-u_des
            e_v = v-v_des
            e_w = w-w_des
            e_q = q-q_des
            e_r = r-r_des
            
            self.r0_u=2.0*abs(e_u) + 1.5
            self.r0_v=2.0*abs(e_v) + 1.5
            self.r0_w=2.0*abs(e_w) + 1.5
            self.r0_q=2.0*abs(e_q) + 1.5
            self.r0_r=2.0*abs(e_r) + 1.5
            
            U = self.controlScheme(t-self.t1)
            
            data = BodyForceReq()
            data.header.stamp = rospy.Time.now()
            data.header.frame_id = "girona500"
            data.goal.requester = "valve_tracking_controller"
            data.goal.id = 0
            data.goal.priority = GoalDescriptor.PRIORITY_NORMAL
            data.wrench.force.x = U[0] * self.power_reduce
            data.wrench.force.y = U[1] * self.power_reduce
            data.wrench.force.z = U[2] * self.power_reduce*5.0    
            data.wrench.torque.x = 0.
            data.wrench.torque.y = U[3] * self.power_reduce
            data.wrench.torque.z = U[4] * (self.power_reduce/3.0)
            data.disable_axis.x = False
            data.disable_axis.y = False
            data.disable_axis.z = False
            data.disable_axis.roll = True
            data.disable_axis.pitch = True
            data.disable_axis.yaw = False
            
            self.pub_tau.publish(data)


if __name__ == '__main__':
    try:
        rospy.init_node('valveTrackingControllerSimple')
        controller = valveTrackingControllerSimple(rospy.get_name())
        
        while not rospy.is_shutdown():
            rospy.sleep(0.05)
            controller.iterate()
            rospy.sleep(0.05)
        
        rospy.spin()

    except rospy.ROSInterruptException: pass
